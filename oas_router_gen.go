package api

import (
	"net/http"
	"net/url"
	"strings"

	"github.com/ogen-go/ogen/uri"
)

// ServeHTTP serves http request as defined by OpenAPI v3 specification,
// calling handler that matches the path or returning not found error.
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	elem := r.URL.Path
	elemIsEscaped := false
	if rawPath := r.URL.RawPath; rawPath != "" {
		if normalized, ok := uri.NormalizeEscapedPath(rawPath); ok {
			elem = normalized
			elemIsEscaped = strings.ContainsRune(elem, '%')
		}
	}
	if prefix := s.cfg.Prefix; len(prefix) > 0 {
		if strings.HasPrefix(elem, prefix) {
			// Cut prefix from the path.
			elem = strings.TrimPrefix(elem, prefix)
		} else {
			// Prefix doesn't match.
			s.notFound(w, r)
			return
		}
	}
	if len(elem) == 0 {
		s.notFound(w, r)
		return
	}
	args := [1]string{}

	// Static code generated router with unwrapped path search.
	switch {
	default:
		if len(elem) == 0 {
			break
		}
		switch elem[0] {
		case '/': // Prefix: "/"
			if l := len("/"); len(elem) >= l && elem[0:l] == "/" {
				elem = elem[l:]
			} else {
				break
			}

			if len(elem) == 0 {
				break
			}
			switch elem[0] {
			case 'm': // Prefix: "merchant"
				if l := len("merchant"); len(elem) >= l && elem[0:l] == "merchant" {
					elem = elem[l:]
				} else {
					break
				}

				if len(elem) == 0 {
					break
				}
				switch elem[0] {
				case '/': // Prefix: "/"
					if l := len("/"); len(elem) >= l && elem[0:l] == "/" {
						elem = elem[l:]
					} else {
						break
					}

					if len(elem) == 0 {
						break
					}
					switch elem[0] {
					case 'p': // Prefix: "places"
						if l := len("places"); len(elem) >= l && elem[0:l] == "places" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							switch r.Method {
							case "GET":
								s.handlePlacesListPublicV1MerchantPlacesGetRequest([0]string{}, elemIsEscaped, w, r)
							case "POST":
								s.handlePlacesCreatePublicV1MerchantPlacesPostRequest([0]string{}, elemIsEscaped, w, r)
							default:
								s.notAllowed(w, r, "GET,POST")
							}

							return
						}
						switch elem[0] {
						case '/': // Prefix: "/"
							if l := len("/"); len(elem) >= l && elem[0:l] == "/" {
								elem = elem[l:]
							} else {
								break
							}

							if len(elem) == 0 {
								break
							}
							switch elem[0] {
							case 'b': // Prefix: "block"
								if l := len("block"); len(elem) >= l && elem[0:l] == "block" {
									elem = elem[l:]
								} else {
									break
								}

								if len(elem) == 0 {
									break
								}
								switch elem[0] {
								case '/': // Prefix: "/"
									if l := len("/"); len(elem) >= l && elem[0:l] == "/" {
										elem = elem[l:]
									} else {
										break
									}

									if len(elem) == 0 {
										// Leaf node.
										switch r.Method {
										case "POST":
											s.handlePlacesBlockPublicV1MerchantPlacesBlockPostRequest([0]string{}, elemIsEscaped, w, r)
										default:
											s.notAllowed(w, r, "POST")
										}

										return
									}
								case 's': // Prefix: "s/"
									if l := len("s/"); len(elem) >= l && elem[0:l] == "s/" {
										elem = elem[l:]
									} else {
										break
									}

									if len(elem) == 0 {
										// Leaf node.
										switch r.Method {
										case "GET":
											s.handlePlacesBlocksPublicV1MerchantPlacesBlocksGetRequest([0]string{}, elemIsEscaped, w, r)
										default:
											s.notAllowed(w, r, "GET")
										}

										return
									}
								}
							case 'o': // Prefix: "order-check"
								if l := len("order-check"); len(elem) >= l && elem[0:l] == "order-check" {
									elem = elem[l:]
								} else {
									break
								}

								if len(elem) == 0 {
									// Leaf node.
									switch r.Method {
									case "POST":
										s.handlePlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRequest([0]string{}, elemIsEscaped, w, r)
									default:
										s.notAllowed(w, r, "POST")
									}

									return
								}
							case 'u': // Prefix: "unblock/"
								if l := len("unblock/"); len(elem) >= l && elem[0:l] == "unblock/" {
									elem = elem[l:]
								} else {
									break
								}

								if len(elem) == 0 {
									// Leaf node.
									switch r.Method {
									case "POST":
										s.handlePlacesUnblockPublicV1MerchantPlacesUnblockPostRequest([0]string{}, elemIsEscaped, w, r)
									default:
										s.notAllowed(w, r, "POST")
									}

									return
								}
							}
							// Param: "place_id"
							// Match until "/"
							idx := strings.IndexByte(elem, '/')
							if idx < 0 {
								idx = len(elem)
							}
							args[0] = elem[:idx]
							elem = elem[idx:]

							if len(elem) == 0 {
								switch r.Method {
								case "PUT":
									s.handlePlacesUpdatePublicV1MerchantPlacesPlaceIDPutRequest([1]string{
										args[0],
									}, elemIsEscaped, w, r)
								default:
									s.notAllowed(w, r, "PUT")
								}

								return
							}
							switch elem[0] {
							case '/': // Prefix: "/schedule"
								if l := len("/schedule"); len(elem) >= l && elem[0:l] == "/schedule" {
									elem = elem[l:]
								} else {
									break
								}

								if len(elem) == 0 {
									// Leaf node.
									switch r.Method {
									case "GET":
										s.handlePlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRequest([1]string{
											args[0],
										}, elemIsEscaped, w, r)
									case "PUT":
										s.handlePlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRequest([1]string{
											args[0],
										}, elemIsEscaped, w, r)
									default:
										s.notAllowed(w, r, "GET,PUT")
									}

									return
								}
							}
						}
					}
					// Param: "merchant_id"
					// Match until "/"
					idx := strings.IndexByte(elem, '/')
					if idx < 0 {
						idx = len(elem)
					}
					args[0] = elem[:idx]
					elem = elem[idx:]

					if len(elem) == 0 {
						break
					}
					switch elem[0] {
					case '/': // Prefix: "/schedule"
						if l := len("/schedule"); len(elem) >= l && elem[0:l] == "/schedule" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							// Leaf node.
							switch r.Method {
							case "GET":
								s.handleMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRequest([1]string{
									args[0],
								}, elemIsEscaped, w, r)
							case "PUT":
								s.handleMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRequest([1]string{
									args[0],
								}, elemIsEscaped, w, r)
							default:
								s.notAllowed(w, r, "GET,PUT")
							}

							return
						}
					}
				case '_': // Prefix: "_"
					if l := len("_"); len(elem) >= l && elem[0:l] == "_" {
						elem = elem[l:]
					} else {
						break
					}

					if len(elem) == 0 {
						break
					}
					switch elem[0] {
					case 'c': // Prefix: "courier_near_"
						if l := len("courier_near_"); len(elem) >= l && elem[0:l] == "courier_near_" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							break
						}
						switch elem[0] {
						case 'd': // Prefix: "delivery_address_callback_url/"
							if l := len("delivery_address_callback_url/"); len(elem) >= l && elem[0:l] == "delivery_address_callback_url/" {
								elem = elem[l:]
							} else {
								break
							}

							// Param: "order_id"
							// Leaf parameter
							args[0] = elem
							elem = ""

							if len(elem) == 0 {
								// Leaf node.
								switch r.Method {
								case "POST":
									s.handleMerchantCourierNearDeliveryAddressCallbackURLOrderIDPostRequest([1]string{
										args[0],
									}, elemIsEscaped, w, r)
								default:
									s.notAllowed(w, r, "POST")
								}

								return
							}
						case 'p': // Prefix: "place_callback_url/"
							if l := len("place_callback_url/"); len(elem) >= l && elem[0:l] == "place_callback_url/" {
								elem = elem[l:]
							} else {
								break
							}

							// Param: "order_id"
							// Leaf parameter
							args[0] = elem
							elem = ""

							if len(elem) == 0 {
								// Leaf node.
								switch r.Method {
								case "POST":
									s.handleMerchantCourierNearPlaceCallbackURLOrderIDPostRequest([1]string{
										args[0],
									}, elemIsEscaped, w, r)
								default:
									s.notAllowed(w, r, "POST")
								}

								return
							}
						}
					case 'o': // Prefix: "order_status_callback_url/"
						if l := len("order_status_callback_url/"); len(elem) >= l && elem[0:l] == "order_status_callback_url/" {
							elem = elem[l:]
						} else {
							break
						}

						// Param: "order_id"
						// Leaf parameter
						args[0] = elem
						elem = ""

						if len(elem) == 0 {
							// Leaf node.
							switch r.Method {
							case "POST":
								s.handleMerchantOrderStatusCallbackURLOrderIDPostRequest([1]string{
									args[0],
								}, elemIsEscaped, w, r)
							default:
								s.notAllowed(w, r, "POST")
							}

							return
						}
					}
				}
			case 'o': // Prefix: "orders/"
				if l := len("orders/"); len(elem) >= l && elem[0:l] == "orders/" {
					elem = elem[l:]
				} else {
					break
				}

				if len(elem) == 0 {
					switch r.Method {
					case "POST":
						s.handlePublicCreateOrderPublicV1OrdersPostRequest([0]string{}, elemIsEscaped, w, r)
					default:
						s.notAllowed(w, r, "POST")
					}

					return
				}
				switch elem[0] {
				case 'd': // Prefix: "dry_run/"
					if l := len("dry_run/"); len(elem) >= l && elem[0:l] == "dry_run/" {
						elem = elem[l:]
					} else {
						break
					}

					if len(elem) == 0 {
						// Leaf node.
						switch r.Method {
						case "POST":
							s.handlePublicDryRunPublicV1OrdersDryRunPostRequest([0]string{}, elemIsEscaped, w, r)
						default:
							s.notAllowed(w, r, "POST")
						}

						return
					}
				}
				// Param: "order_id"
				// Match until "/"
				idx := strings.IndexByte(elem, '/')
				if idx < 0 {
					idx = len(elem)
				}
				args[0] = elem[:idx]
				elem = elem[idx:]

				if len(elem) == 0 {
					break
				}
				switch elem[0] {
				case '/': // Prefix: "/"
					if l := len("/"); len(elem) >= l && elem[0:l] == "/" {
						elem = elem[l:]
					} else {
						break
					}

					if len(elem) == 0 {
						switch r.Method {
						case "DELETE":
							s.handlePublicCancelOrderPublicV1OrdersOrderIDDeleteRequest([1]string{
								args[0],
							}, elemIsEscaped, w, r)
						case "GET":
							s.handlePublicGetOrderPublicV1OrdersOrderIDGetRequest([1]string{
								args[0],
							}, elemIsEscaped, w, r)
						case "PATCH":
							s.handlePublicUpdateOrderPublicV1OrdersOrderIDPatchRequest([1]string{
								args[0],
							}, elemIsEscaped, w, r)
						default:
							s.notAllowed(w, r, "DELETE,GET,PATCH")
						}

						return
					}
					switch elem[0] {
					case 'p': // Prefix: "photos/"
						if l := len("photos/"); len(elem) >= l && elem[0:l] == "photos/" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							// Leaf node.
							switch r.Method {
							case "GET":
								s.handlePublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRequest([1]string{
									args[0],
								}, elemIsEscaped, w, r)
							default:
								s.notAllowed(w, r, "GET")
							}

							return
						}
					case 'r': // Prefix: "return/"
						if l := len("return/"); len(elem) >= l && elem[0:l] == "return/" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							// Leaf node.
							switch r.Method {
							case "PATCH":
								s.handlePublicReturnOrderPublicV1OrdersOrderIDReturnPatchRequest([1]string{
									args[0],
								}, elemIsEscaped, w, r)
							default:
								s.notAllowed(w, r, "PATCH")
							}

							return
						}
					case 't': // Prefix: "tracking/"
						if l := len("tracking/"); len(elem) >= l && elem[0:l] == "tracking/" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							// Leaf node.
							switch r.Method {
							case "GET":
								s.handlePublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRequest([1]string{
									args[0],
								}, elemIsEscaped, w, r)
							default:
								s.notAllowed(w, r, "GET")
							}

							return
						}
					}
				}
			case 'v': // Prefix: "v2/orders/"
				if l := len("v2/orders/"); len(elem) >= l && elem[0:l] == "v2/orders/" {
					elem = elem[l:]
				} else {
					break
				}

				// Param: "order_id"
				// Match until "/"
				idx := strings.IndexByte(elem, '/')
				if idx < 0 {
					idx = len(elem)
				}
				args[0] = elem[:idx]
				elem = elem[idx:]

				if len(elem) == 0 {
					break
				}
				switch elem[0] {
				case '/': // Prefix: "/return/"
					if l := len("/return/"); len(elem) >= l && elem[0:l] == "/return/" {
						elem = elem[l:]
					} else {
						break
					}

					if len(elem) == 0 {
						// Leaf node.
						switch r.Method {
						case "PATCH":
							s.handlePublicReturnOrderPublicV2OrdersOrderIDReturnPatchRequest([1]string{
								args[0],
							}, elemIsEscaped, w, r)
						default:
							s.notAllowed(w, r, "PATCH")
						}

						return
					}
				}
			}
		}
	}
	s.notFound(w, r)
}

// Route is route object.
type Route struct {
	name        string
	operationID string
	pathPattern string
	count       int
	args        [1]string
}

// Name returns ogen operation name.
//
// It is guaranteed to be unique and not empty.
func (r Route) Name() string {
	return r.name
}

// OperationID returns OpenAPI operationId.
func (r Route) OperationID() string {
	return r.operationID
}

// PathPattern returns OpenAPI path.
func (r Route) PathPattern() string {
	return r.pathPattern
}

// Args returns parsed arguments.
func (r Route) Args() []string {
	return r.args[:r.count]
}

// FindRoute finds Route for given method and path.
//
// Note: this method does not unescape path or handle reserved characters in path properly. Use FindPath instead.
func (s *Server) FindRoute(method, path string) (Route, bool) {
	return s.FindPath(method, &url.URL{Path: path})
}

// FindPath finds Route for given method and URL.
func (s *Server) FindPath(method string, u *url.URL) (r Route, _ bool) {
	var (
		elem = u.Path
		args = r.args
	)
	if rawPath := u.RawPath; rawPath != "" {
		if normalized, ok := uri.NormalizeEscapedPath(rawPath); ok {
			elem = normalized
		}
		defer func() {
			for i, arg := range r.args[:r.count] {
				if unescaped, err := url.PathUnescape(arg); err == nil {
					r.args[i] = unescaped
				}
			}
		}()
	}

	// Static code generated router with unwrapped path search.
	switch {
	default:
		if len(elem) == 0 {
			break
		}
		switch elem[0] {
		case '/': // Prefix: "/"
			if l := len("/"); len(elem) >= l && elem[0:l] == "/" {
				elem = elem[l:]
			} else {
				break
			}

			if len(elem) == 0 {
				break
			}
			switch elem[0] {
			case 'm': // Prefix: "merchant"
				if l := len("merchant"); len(elem) >= l && elem[0:l] == "merchant" {
					elem = elem[l:]
				} else {
					break
				}

				if len(elem) == 0 {
					break
				}
				switch elem[0] {
				case '/': // Prefix: "/"
					if l := len("/"); len(elem) >= l && elem[0:l] == "/" {
						elem = elem[l:]
					} else {
						break
					}

					if len(elem) == 0 {
						break
					}
					switch elem[0] {
					case 'p': // Prefix: "places"
						if l := len("places"); len(elem) >= l && elem[0:l] == "places" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							switch method {
							case "GET":
								r.name = "PlacesListPublicV1MerchantPlacesGet"
								r.operationID = "places_list_public_v1_merchant_places_get"
								r.pathPattern = "/merchant/places"
								r.args = args
								r.count = 0
								return r, true
							case "POST":
								r.name = "PlacesCreatePublicV1MerchantPlacesPost"
								r.operationID = "places_create_public_v1_merchant_places_post"
								r.pathPattern = "/merchant/places"
								r.args = args
								r.count = 0
								return r, true
							default:
								return
							}
						}
						switch elem[0] {
						case '/': // Prefix: "/"
							if l := len("/"); len(elem) >= l && elem[0:l] == "/" {
								elem = elem[l:]
							} else {
								break
							}

							if len(elem) == 0 {
								break
							}
							switch elem[0] {
							case 'b': // Prefix: "block"
								if l := len("block"); len(elem) >= l && elem[0:l] == "block" {
									elem = elem[l:]
								} else {
									break
								}

								if len(elem) == 0 {
									break
								}
								switch elem[0] {
								case '/': // Prefix: "/"
									if l := len("/"); len(elem) >= l && elem[0:l] == "/" {
										elem = elem[l:]
									} else {
										break
									}

									if len(elem) == 0 {
										switch method {
										case "POST":
											// Leaf: PlacesBlockPublicV1MerchantPlacesBlockPost
											r.name = "PlacesBlockPublicV1MerchantPlacesBlockPost"
											r.operationID = "places_block_public_v1_merchant_places_block__post"
											r.pathPattern = "/merchant/places/block/"
											r.args = args
											r.count = 0
											return r, true
										default:
											return
										}
									}
								case 's': // Prefix: "s/"
									if l := len("s/"); len(elem) >= l && elem[0:l] == "s/" {
										elem = elem[l:]
									} else {
										break
									}

									if len(elem) == 0 {
										switch method {
										case "GET":
											// Leaf: PlacesBlocksPublicV1MerchantPlacesBlocksGet
											r.name = "PlacesBlocksPublicV1MerchantPlacesBlocksGet"
											r.operationID = "places_blocks_public_v1_merchant_places_blocks__get"
											r.pathPattern = "/merchant/places/blocks/"
											r.args = args
											r.count = 0
											return r, true
										default:
											return
										}
									}
								}
							case 'o': // Prefix: "order-check"
								if l := len("order-check"); len(elem) >= l && elem[0:l] == "order-check" {
									elem = elem[l:]
								} else {
									break
								}

								if len(elem) == 0 {
									switch method {
									case "POST":
										// Leaf: PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost
										r.name = "PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost"
										r.operationID = "places_order_check_public_v1_merchant_places_order_check_post"
										r.pathPattern = "/merchant/places/order-check"
										r.args = args
										r.count = 0
										return r, true
									default:
										return
									}
								}
							case 'u': // Prefix: "unblock/"
								if l := len("unblock/"); len(elem) >= l && elem[0:l] == "unblock/" {
									elem = elem[l:]
								} else {
									break
								}

								if len(elem) == 0 {
									switch method {
									case "POST":
										// Leaf: PlacesUnblockPublicV1MerchantPlacesUnblockPost
										r.name = "PlacesUnblockPublicV1MerchantPlacesUnblockPost"
										r.operationID = "places_unblock_public_v1_merchant_places_unblock__post"
										r.pathPattern = "/merchant/places/unblock/"
										r.args = args
										r.count = 0
										return r, true
									default:
										return
									}
								}
							}
							// Param: "place_id"
							// Match until "/"
							idx := strings.IndexByte(elem, '/')
							if idx < 0 {
								idx = len(elem)
							}
							args[0] = elem[:idx]
							elem = elem[idx:]

							if len(elem) == 0 {
								switch method {
								case "PUT":
									r.name = "PlacesUpdatePublicV1MerchantPlacesPlaceIDPut"
									r.operationID = "places_update_public_v1_merchant_places__place_id__put"
									r.pathPattern = "/merchant/places/{place_id}"
									r.args = args
									r.count = 1
									return r, true
								default:
									return
								}
							}
							switch elem[0] {
							case '/': // Prefix: "/schedule"
								if l := len("/schedule"); len(elem) >= l && elem[0:l] == "/schedule" {
									elem = elem[l:]
								} else {
									break
								}

								if len(elem) == 0 {
									switch method {
									case "GET":
										// Leaf: PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet
										r.name = "PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet"
										r.operationID = "place_schedule_get_public_v1_merchant_places__place_id__schedule_get"
										r.pathPattern = "/merchant/places/{place_id}/schedule"
										r.args = args
										r.count = 1
										return r, true
									case "PUT":
										// Leaf: PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut
										r.name = "PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut"
										r.operationID = "place_schedule_update_public_v1_merchant_places__place_id__schedule_put"
										r.pathPattern = "/merchant/places/{place_id}/schedule"
										r.args = args
										r.count = 1
										return r, true
									default:
										return
									}
								}
							}
						}
					}
					// Param: "merchant_id"
					// Match until "/"
					idx := strings.IndexByte(elem, '/')
					if idx < 0 {
						idx = len(elem)
					}
					args[0] = elem[:idx]
					elem = elem[idx:]

					if len(elem) == 0 {
						break
					}
					switch elem[0] {
					case '/': // Prefix: "/schedule"
						if l := len("/schedule"); len(elem) >= l && elem[0:l] == "/schedule" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							switch method {
							case "GET":
								// Leaf: MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet
								r.name = "MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet"
								r.operationID = "merchant_schedule_get_public_v1_merchant__merchant_id__schedule_get"
								r.pathPattern = "/merchant/{merchant_id}/schedule"
								r.args = args
								r.count = 1
								return r, true
							case "PUT":
								// Leaf: MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut
								r.name = "MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut"
								r.operationID = "merchant_schedule_update_public_v1_merchant__merchant_id__schedule_put"
								r.pathPattern = "/merchant/{merchant_id}/schedule"
								r.args = args
								r.count = 1
								return r, true
							default:
								return
							}
						}
					}
				case '_': // Prefix: "_"
					if l := len("_"); len(elem) >= l && elem[0:l] == "_" {
						elem = elem[l:]
					} else {
						break
					}

					if len(elem) == 0 {
						break
					}
					switch elem[0] {
					case 'c': // Prefix: "courier_near_"
						if l := len("courier_near_"); len(elem) >= l && elem[0:l] == "courier_near_" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							break
						}
						switch elem[0] {
						case 'd': // Prefix: "delivery_address_callback_url/"
							if l := len("delivery_address_callback_url/"); len(elem) >= l && elem[0:l] == "delivery_address_callback_url/" {
								elem = elem[l:]
							} else {
								break
							}

							// Param: "order_id"
							// Leaf parameter
							args[0] = elem
							elem = ""

							if len(elem) == 0 {
								switch method {
								case "POST":
									// Leaf: MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost
									r.name = "MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost"
									r.operationID = ""
									r.pathPattern = "/merchant_courier_near_delivery_address_callback_url/{order_id}"
									r.args = args
									r.count = 1
									return r, true
								default:
									return
								}
							}
						case 'p': // Prefix: "place_callback_url/"
							if l := len("place_callback_url/"); len(elem) >= l && elem[0:l] == "place_callback_url/" {
								elem = elem[l:]
							} else {
								break
							}

							// Param: "order_id"
							// Leaf parameter
							args[0] = elem
							elem = ""

							if len(elem) == 0 {
								switch method {
								case "POST":
									// Leaf: MerchantCourierNearPlaceCallbackURLOrderIDPost
									r.name = "MerchantCourierNearPlaceCallbackURLOrderIDPost"
									r.operationID = ""
									r.pathPattern = "/merchant_courier_near_place_callback_url/{order_id}"
									r.args = args
									r.count = 1
									return r, true
								default:
									return
								}
							}
						}
					case 'o': // Prefix: "order_status_callback_url/"
						if l := len("order_status_callback_url/"); len(elem) >= l && elem[0:l] == "order_status_callback_url/" {
							elem = elem[l:]
						} else {
							break
						}

						// Param: "order_id"
						// Leaf parameter
						args[0] = elem
						elem = ""

						if len(elem) == 0 {
							switch method {
							case "POST":
								// Leaf: MerchantOrderStatusCallbackURLOrderIDPost
								r.name = "MerchantOrderStatusCallbackURLOrderIDPost"
								r.operationID = ""
								r.pathPattern = "/merchant_order_status_callback_url/{order_id}"
								r.args = args
								r.count = 1
								return r, true
							default:
								return
							}
						}
					}
				}
			case 'o': // Prefix: "orders/"
				if l := len("orders/"); len(elem) >= l && elem[0:l] == "orders/" {
					elem = elem[l:]
				} else {
					break
				}

				if len(elem) == 0 {
					switch method {
					case "POST":
						r.name = "PublicCreateOrderPublicV1OrdersPost"
						r.operationID = "public_create_order_public_v1_orders__post"
						r.pathPattern = "/orders/"
						r.args = args
						r.count = 0
						return r, true
					default:
						return
					}
				}
				switch elem[0] {
				case 'd': // Prefix: "dry_run/"
					if l := len("dry_run/"); len(elem) >= l && elem[0:l] == "dry_run/" {
						elem = elem[l:]
					} else {
						break
					}

					if len(elem) == 0 {
						switch method {
						case "POST":
							// Leaf: PublicDryRunPublicV1OrdersDryRunPost
							r.name = "PublicDryRunPublicV1OrdersDryRunPost"
							r.operationID = "public_dry_run_public_v1_orders_dry_run__post"
							r.pathPattern = "/orders/dry_run/"
							r.args = args
							r.count = 0
							return r, true
						default:
							return
						}
					}
				}
				// Param: "order_id"
				// Match until "/"
				idx := strings.IndexByte(elem, '/')
				if idx < 0 {
					idx = len(elem)
				}
				args[0] = elem[:idx]
				elem = elem[idx:]

				if len(elem) == 0 {
					break
				}
				switch elem[0] {
				case '/': // Prefix: "/"
					if l := len("/"); len(elem) >= l && elem[0:l] == "/" {
						elem = elem[l:]
					} else {
						break
					}

					if len(elem) == 0 {
						switch method {
						case "DELETE":
							r.name = "PublicCancelOrderPublicV1OrdersOrderIDDelete"
							r.operationID = "public_cancel_order_public_v1_orders__order_id___delete"
							r.pathPattern = "/orders/{order_id}/"
							r.args = args
							r.count = 1
							return r, true
						case "GET":
							r.name = "PublicGetOrderPublicV1OrdersOrderIDGet"
							r.operationID = "public_get_order_public_v1_orders__order_id___get"
							r.pathPattern = "/orders/{order_id}/"
							r.args = args
							r.count = 1
							return r, true
						case "PATCH":
							r.name = "PublicUpdateOrderPublicV1OrdersOrderIDPatch"
							r.operationID = "public_update_order_public_v1_orders__order_id___patch"
							r.pathPattern = "/orders/{order_id}/"
							r.args = args
							r.count = 1
							return r, true
						default:
							return
						}
					}
					switch elem[0] {
					case 'p': // Prefix: "photos/"
						if l := len("photos/"); len(elem) >= l && elem[0:l] == "photos/" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							switch method {
							case "GET":
								// Leaf: PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet
								r.name = "PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet"
								r.operationID = "public_get_order_photos_public_v1_orders__order_id__photos__get"
								r.pathPattern = "/orders/{order_id}/photos/"
								r.args = args
								r.count = 1
								return r, true
							default:
								return
							}
						}
					case 'r': // Prefix: "return/"
						if l := len("return/"); len(elem) >= l && elem[0:l] == "return/" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							switch method {
							case "PATCH":
								// Leaf: PublicReturnOrderPublicV1OrdersOrderIDReturnPatch
								r.name = "PublicReturnOrderPublicV1OrdersOrderIDReturnPatch"
								r.operationID = "public_return_order_public_v1_orders__order_id__return__patch"
								r.pathPattern = "/orders/{order_id}/return/"
								r.args = args
								r.count = 1
								return r, true
							default:
								return
							}
						}
					case 't': // Prefix: "tracking/"
						if l := len("tracking/"); len(elem) >= l && elem[0:l] == "tracking/" {
							elem = elem[l:]
						} else {
							break
						}

						if len(elem) == 0 {
							switch method {
							case "GET":
								// Leaf: PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet
								r.name = "PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet"
								r.operationID = "public_get_courier_position_public_v1_orders__order_id__tracking__get"
								r.pathPattern = "/orders/{order_id}/tracking/"
								r.args = args
								r.count = 1
								return r, true
							default:
								return
							}
						}
					}
				}
			case 'v': // Prefix: "v2/orders/"
				if l := len("v2/orders/"); len(elem) >= l && elem[0:l] == "v2/orders/" {
					elem = elem[l:]
				} else {
					break
				}

				// Param: "order_id"
				// Match until "/"
				idx := strings.IndexByte(elem, '/')
				if idx < 0 {
					idx = len(elem)
				}
				args[0] = elem[:idx]
				elem = elem[idx:]

				if len(elem) == 0 {
					break
				}
				switch elem[0] {
				case '/': // Prefix: "/return/"
					if l := len("/return/"); len(elem) >= l && elem[0:l] == "/return/" {
						elem = elem[l:]
					} else {
						break
					}

					if len(elem) == 0 {
						switch method {
						case "PATCH":
							// Leaf: PublicReturnOrderPublicV2OrdersOrderIDReturnPatch
							r.name = "PublicReturnOrderPublicV2OrdersOrderIDReturnPatch"
							r.operationID = "public_return_order_public_v2_orders__order_id__return__patch"
							r.pathPattern = "/v2/orders/{order_id}/return/"
							r.args = args
							r.count = 1
							return r, true
						default:
							return
						}
					}
				}
			}
		}
	}
	return r, false
}
