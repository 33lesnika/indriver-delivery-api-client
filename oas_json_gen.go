package api

import (
	"math/bits"
	"strconv"
	"time"

	"github.com/go-faster/errors"
	"github.com/go-faster/jx"
	"github.com/google/uuid"

	"github.com/ogen-go/ogen/json"
	"github.com/ogen-go/ogen/validate"
)

// Encode implements json.Marshaler.
func (s *AdditionalServiceCodeNameOnly) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *AdditionalServiceCodeNameOnly) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("code_name")
		s.CodeName.Encode(e)
	}
}

var jsonFieldsNameOfAdditionalServiceCodeNameOnly = [1]string{
	0: "code_name",
}

// Decode decodes AdditionalServiceCodeNameOnly from json.
func (s *AdditionalServiceCodeNameOnly) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode AdditionalServiceCodeNameOnly to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "code_name":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				if err := s.CodeName.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"code_name\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode AdditionalServiceCodeNameOnly")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000001,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfAdditionalServiceCodeNameOnly) {
					name = jsonFieldsNameOfAdditionalServiceCodeNameOnly[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *AdditionalServiceCodeNameOnly) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *AdditionalServiceCodeNameOnly) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes AdditionalServiceCodeNameOnlyCodeName as json.
func (s AdditionalServiceCodeNameOnlyCodeName) Encode(e *jx.Encoder) {
	e.Str(string(s))
}

// Decode decodes AdditionalServiceCodeNameOnlyCodeName from json.
func (s *AdditionalServiceCodeNameOnlyCodeName) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode AdditionalServiceCodeNameOnlyCodeName to nil")
	}
	v, err := d.StrBytes()
	if err != nil {
		return err
	}
	// Try to use constant string.
	switch AdditionalServiceCodeNameOnlyCodeName(v) {
	case AdditionalServiceCodeNameOnlyCodeNameLeaveAtTheDoor:
		*s = AdditionalServiceCodeNameOnlyCodeNameLeaveAtTheDoor
	case AdditionalServiceCodeNameOnlyCodeNameAdult:
		*s = AdditionalServiceCodeNameOnlyCodeNameAdult
	case AdditionalServiceCodeNameOnlyCodeNameNotCallMe:
		*s = AdditionalServiceCodeNameOnlyCodeNameNotCallMe
	default:
		*s = AdditionalServiceCodeNameOnlyCodeName(v)
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s AdditionalServiceCodeNameOnlyCodeName) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *AdditionalServiceCodeNameOnlyCodeName) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes AdditionalServiceInDB as json.
func (s AdditionalServiceInDB) Encode(e *jx.Encoder) {
	unwrapped := jx.Raw(s)

	if len(unwrapped) != 0 {
		e.Raw(unwrapped)
	}
}

// Decode decodes AdditionalServiceInDB from json.
func (s *AdditionalServiceInDB) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode AdditionalServiceInDB to nil")
	}
	var unwrapped jx.Raw
	if err := func() error {
		v, err := d.RawAppend(nil)
		unwrapped = jx.Raw(v)
		if err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return errors.Wrap(err, "alias")
	}
	*s = AdditionalServiceInDB(unwrapped)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s AdditionalServiceInDB) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *AdditionalServiceInDB) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *AreaCreateWithoutPlace) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *AreaCreateWithoutPlace) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("min_delivery_duration")
		e.Int(s.MinDeliveryDuration)
	}
	{

		e.FieldStart("max_delivery_duration")
		e.Int(s.MaxDeliveryDuration)
	}
	{

		e.FieldStart("geometry")
		s.Geometry.Encode(e)
	}
}

var jsonFieldsNameOfAreaCreateWithoutPlace = [3]string{
	0: "min_delivery_duration",
	1: "max_delivery_duration",
	2: "geometry",
}

// Decode decodes AreaCreateWithoutPlace from json.
func (s *AreaCreateWithoutPlace) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode AreaCreateWithoutPlace to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "min_delivery_duration":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := d.Int()
				s.MinDeliveryDuration = int(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"min_delivery_duration\"")
			}
		case "max_delivery_duration":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Int()
				s.MaxDeliveryDuration = int(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"max_delivery_duration\"")
			}
		case "geometry":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				if err := s.Geometry.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"geometry\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode AreaCreateWithoutPlace")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfAreaCreateWithoutPlace) {
					name = jsonFieldsNameOfAreaCreateWithoutPlace[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *AreaCreateWithoutPlace) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *AreaCreateWithoutPlace) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *BaseSchemaResponse) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *BaseSchemaResponse) encodeFields(e *jx.Encoder) {
}

var jsonFieldsNameOfBaseSchemaResponse = [0]string{}

// Decode decodes BaseSchemaResponse from json.
func (s *BaseSchemaResponse) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode BaseSchemaResponse to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode BaseSchemaResponse")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *BaseSchemaResponse) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *BaseSchemaResponse) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *CancelOrderInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *CancelOrderInRequest) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("cancel_code")
		s.CancelCode.Encode(e)
	}
	{
		if s.CancelReason.Set {
			e.FieldStart("cancel_reason")
			s.CancelReason.Encode(e)
		}
	}
}

var jsonFieldsNameOfCancelOrderInRequest = [2]string{
	0: "cancel_code",
	1: "cancel_reason",
}

// Decode decodes CancelOrderInRequest from json.
func (s *CancelOrderInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode CancelOrderInRequest to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "cancel_code":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				if err := s.CancelCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cancel_code\"")
			}
		case "cancel_reason":
			if err := func() error {
				s.CancelReason.Reset()
				if err := s.CancelReason.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cancel_reason\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode CancelOrderInRequest")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000001,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfCancelOrderInRequest) {
					name = jsonFieldsNameOfCancelOrderInRequest[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *CancelOrderInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *CancelOrderInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *CreateDeliveryInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *CreateDeliveryInRequest) encodeFields(e *jx.Encoder) {
	{
		if s.ExpectedTime.Set {
			e.FieldStart("expected_time")
			s.ExpectedTime.Encode(e, json.EncodeDateTime)
		}
	}
	{

		e.FieldStart("payment_method")
		s.PaymentMethod.Encode(e)
	}
	{

		e.FieldStart("consumer_data")
		e.Str(s.ConsumerData)
	}
	{

		e.FieldStart("region")
		e.Str(s.Region)
	}
	{

		e.FieldStart("street")
		e.Str(s.Street)
	}
	{
		if s.HouseNumber.Set {
			e.FieldStart("house_number")
			s.HouseNumber.Encode(e)
		}
	}
	{
		if s.FlatNumber.Set {
			e.FieldStart("flat_number")
			s.FlatNumber.Encode(e)
		}
	}
	{
		if s.Entrance.Set {
			e.FieldStart("entrance")
			s.Entrance.Encode(e)
		}
	}
	{
		if s.Intercom.Set {
			e.FieldStart("intercom")
			s.Intercom.Encode(e)
		}
	}
	{
		if s.Floor.Set {
			e.FieldStart("floor")
			s.Floor.Encode(e)
		}
	}
	{
		if s.Location.Set {
			e.FieldStart("location")
			s.Location.Encode(e)
		}
	}
	{
		if s.Comment.Set {
			e.FieldStart("comment")
			s.Comment.Encode(e)
		}
	}
	{
		if s.Attributes.Set {
			e.FieldStart("attributes")
			s.Attributes.Encode(e)
		}
	}
}

var jsonFieldsNameOfCreateDeliveryInRequest = [13]string{
	0:  "expected_time",
	1:  "payment_method",
	2:  "consumer_data",
	3:  "region",
	4:  "street",
	5:  "house_number",
	6:  "flat_number",
	7:  "entrance",
	8:  "intercom",
	9:  "floor",
	10: "location",
	11: "comment",
	12: "attributes",
}

// Decode decodes CreateDeliveryInRequest from json.
func (s *CreateDeliveryInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode CreateDeliveryInRequest to nil")
	}
	var requiredBitSet [2]uint8
	s.setDefaults()

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "expected_time":
			if err := func() error {
				s.ExpectedTime.Reset()
				if err := s.ExpectedTime.Decode(d, json.DecodeDateTime); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"expected_time\"")
			}
		case "payment_method":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				if err := s.PaymentMethod.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"payment_method\"")
			}
		case "consumer_data":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Str()
				s.ConsumerData = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"consumer_data\"")
			}
		case "region":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Str()
				s.Region = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"region\"")
			}
		case "street":
			requiredBitSet[0] |= 1 << 4
			if err := func() error {
				v, err := d.Str()
				s.Street = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"street\"")
			}
		case "house_number":
			if err := func() error {
				s.HouseNumber.Reset()
				if err := s.HouseNumber.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"house_number\"")
			}
		case "flat_number":
			if err := func() error {
				s.FlatNumber.Reset()
				if err := s.FlatNumber.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"flat_number\"")
			}
		case "entrance":
			if err := func() error {
				s.Entrance.Reset()
				if err := s.Entrance.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"entrance\"")
			}
		case "intercom":
			if err := func() error {
				s.Intercom.Reset()
				if err := s.Intercom.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"intercom\"")
			}
		case "floor":
			if err := func() error {
				s.Floor.Reset()
				if err := s.Floor.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"floor\"")
			}
		case "location":
			if err := func() error {
				s.Location.Reset()
				if err := s.Location.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"location\"")
			}
		case "comment":
			if err := func() error {
				s.Comment.Reset()
				if err := s.Comment.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"comment\"")
			}
		case "attributes":
			if err := func() error {
				s.Attributes.Reset()
				if err := s.Attributes.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"attributes\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode CreateDeliveryInRequest")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [2]uint8{
		0b00011110,
		0b00000000,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfCreateDeliveryInRequest) {
					name = jsonFieldsNameOfCreateDeliveryInRequest[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *CreateDeliveryInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *CreateDeliveryInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *CreateIngredientInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *CreateIngredientInRequest) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("name")
		e.Str(s.Name)
	}
	{

		e.FieldStart("price")
		e.Float64(s.Price)
	}
	{

		e.FieldStart("group_name")
		e.Str(s.GroupName)
	}
	{
		if s.VatCode.Set {
			e.FieldStart("vat_code")
			s.VatCode.Encode(e)
		}
	}
	{
		if s.RkeeperCode.Set {
			e.FieldStart("rkeeper_code")
			s.RkeeperCode.Encode(e)
		}
	}
	{
		if s.ExternalID.Set {
			e.FieldStart("external_id")
			s.ExternalID.Encode(e)
		}
	}
}

var jsonFieldsNameOfCreateIngredientInRequest = [6]string{
	0: "name",
	1: "price",
	2: "group_name",
	3: "vat_code",
	4: "rkeeper_code",
	5: "external_id",
}

// Decode decodes CreateIngredientInRequest from json.
func (s *CreateIngredientInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode CreateIngredientInRequest to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "name":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := d.Str()
				s.Name = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"name\"")
			}
		case "price":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Float64()
				s.Price = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"price\"")
			}
		case "group_name":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Str()
				s.GroupName = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"group_name\"")
			}
		case "vat_code":
			if err := func() error {
				s.VatCode.Reset()
				if err := s.VatCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"vat_code\"")
			}
		case "rkeeper_code":
			if err := func() error {
				s.RkeeperCode.Reset()
				if err := s.RkeeperCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"rkeeper_code\"")
			}
		case "external_id":
			if err := func() error {
				s.ExternalID.Reset()
				if err := s.ExternalID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"external_id\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode CreateIngredientInRequest")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfCreateIngredientInRequest) {
					name = jsonFieldsNameOfCreateIngredientInRequest[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *CreateIngredientInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *CreateIngredientInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *CreateOrderInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *CreateOrderInRequest) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("place_id")
		json.EncodeUUID(e, s.PlaceID)
	}
	{

		e.FieldStart("cost")
		s.Cost.Encode(e)
	}
	{

		e.FieldStart("person_count")
		e.Int(s.PersonCount)
	}
	{
		if s.Comment.Set {
			e.FieldStart("comment")
			s.Comment.Encode(e)
		}
	}
	{

		e.FieldStart("products")
		e.ArrStart()
		for _, elem := range s.Products {
			elem.Encode(e)
		}
		e.ArrEnd()
	}
	{
		if s.AdditionalServices != nil {
			e.FieldStart("additional_services")
			e.ArrStart()
			for _, elem := range s.AdditionalServices {
				elem.Encode(e)
			}
			e.ArrEnd()
		}
	}
	{
		if s.IsForceStart.Set {
			e.FieldStart("is_force_start")
			s.IsForceStart.Encode(e)
		}
	}
	{

		e.FieldStart("order_id")
		e.Str(s.OrderID)
	}
	{

		e.FieldStart("customer")
		s.Customer.Encode(e)
	}
	{

		e.FieldStart("delivery")
		s.Delivery.Encode(e)
	}
}

var jsonFieldsNameOfCreateOrderInRequest = [10]string{
	0: "place_id",
	1: "cost",
	2: "person_count",
	3: "comment",
	4: "products",
	5: "additional_services",
	6: "is_force_start",
	7: "order_id",
	8: "customer",
	9: "delivery",
}

// Decode decodes CreateOrderInRequest from json.
func (s *CreateOrderInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode CreateOrderInRequest to nil")
	}
	var requiredBitSet [2]uint8
	s.setDefaults()

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "place_id":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.PlaceID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"place_id\"")
			}
		case "cost":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				if err := s.Cost.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cost\"")
			}
		case "person_count":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Int()
				s.PersonCount = int(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"person_count\"")
			}
		case "comment":
			if err := func() error {
				s.Comment.Reset()
				if err := s.Comment.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"comment\"")
			}
		case "products":
			requiredBitSet[0] |= 1 << 4
			if err := func() error {
				s.Products = make([]CreateProductInRequest, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem CreateProductInRequest
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Products = append(s.Products, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"products\"")
			}
		case "additional_services":
			if err := func() error {
				s.AdditionalServices = make([]AdditionalServiceCodeNameOnly, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem AdditionalServiceCodeNameOnly
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.AdditionalServices = append(s.AdditionalServices, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"additional_services\"")
			}
		case "is_force_start":
			if err := func() error {
				s.IsForceStart.Reset()
				if err := s.IsForceStart.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"is_force_start\"")
			}
		case "order_id":
			requiredBitSet[0] |= 1 << 7
			if err := func() error {
				v, err := d.Str()
				s.OrderID = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"order_id\"")
			}
		case "customer":
			requiredBitSet[1] |= 1 << 0
			if err := func() error {
				if err := s.Customer.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"customer\"")
			}
		case "delivery":
			requiredBitSet[1] |= 1 << 1
			if err := func() error {
				if err := s.Delivery.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"delivery\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode CreateOrderInRequest")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [2]uint8{
		0b10010111,
		0b00000011,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfCreateOrderInRequest) {
					name = jsonFieldsNameOfCreateOrderInRequest[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *CreateOrderInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *CreateOrderInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *CreateProductInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *CreateProductInRequest) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("name")
		e.Str(s.Name)
	}
	{

		e.FieldStart("quantity")
		e.Int(s.Quantity)
	}
	{

		e.FieldStart("price")
		e.Float64(s.Price)
	}
	{

		e.FieldStart("total_price")
		e.Float64(s.TotalPrice)
	}
	{
		if s.VatCode.Set {
			e.FieldStart("vat_code")
			s.VatCode.Encode(e)
		}
	}
	{
		if s.RkeeperCode.Set {
			e.FieldStart("rkeeper_code")
			s.RkeeperCode.Encode(e)
		}
	}
	{
		if s.ExternalID.Set {
			e.FieldStart("external_id")
			s.ExternalID.Encode(e)
		}
	}
	{
		if s.Ingredients != nil {
			e.FieldStart("ingredients")
			e.ArrStart()
			for _, elem := range s.Ingredients {
				elem.Encode(e)
			}
			e.ArrEnd()
		}
	}
}

var jsonFieldsNameOfCreateProductInRequest = [8]string{
	0: "name",
	1: "quantity",
	2: "price",
	3: "total_price",
	4: "vat_code",
	5: "rkeeper_code",
	6: "external_id",
	7: "ingredients",
}

// Decode decodes CreateProductInRequest from json.
func (s *CreateProductInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode CreateProductInRequest to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "name":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := d.Str()
				s.Name = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"name\"")
			}
		case "quantity":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Int()
				s.Quantity = int(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"quantity\"")
			}
		case "price":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Float64()
				s.Price = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"price\"")
			}
		case "total_price":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Float64()
				s.TotalPrice = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"total_price\"")
			}
		case "vat_code":
			if err := func() error {
				s.VatCode.Reset()
				if err := s.VatCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"vat_code\"")
			}
		case "rkeeper_code":
			if err := func() error {
				s.RkeeperCode.Reset()
				if err := s.RkeeperCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"rkeeper_code\"")
			}
		case "external_id":
			if err := func() error {
				s.ExternalID.Reset()
				if err := s.ExternalID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"external_id\"")
			}
		case "ingredients":
			if err := func() error {
				s.Ingredients = make([]CreateIngredientInRequest, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem CreateIngredientInRequest
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Ingredients = append(s.Ingredients, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"ingredients\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode CreateProductInRequest")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00001111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfCreateProductInRequest) {
					name = jsonFieldsNameOfCreateProductInRequest[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *CreateProductInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *CreateProductInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *DbModelsBasePoint) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *DbModelsBasePoint) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("longitude")
		e.Float64(s.Longitude)
	}
	{

		e.FieldStart("latitude")
		e.Float64(s.Latitude)
	}
}

var jsonFieldsNameOfDbModelsBasePoint = [2]string{
	0: "longitude",
	1: "latitude",
}

// Decode decodes DbModelsBasePoint from json.
func (s *DbModelsBasePoint) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode DbModelsBasePoint to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "longitude":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := d.Float64()
				s.Longitude = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"longitude\"")
			}
		case "latitude":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Float64()
				s.Latitude = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"latitude\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode DbModelsBasePoint")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000011,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfDbModelsBasePoint) {
					name = jsonFieldsNameOfDbModelsBasePoint[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *DbModelsBasePoint) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *DbModelsBasePoint) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *DeliveryAttributes) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *DeliveryAttributes) encodeFields(e *jx.Encoder) {
	{
		if s.ShortCode.Set {
			e.FieldStart("short_code")
			s.ShortCode.Encode(e)
		}
	}
	{
		if s.PinCode.Set {
			e.FieldStart("pin_code")
			s.PinCode.Encode(e)
		}
	}
	{
		if s.Channel.Set {
			e.FieldStart("channel")
			s.Channel.Encode(e)
		}
	}
	{
		if s.BankTransactionID.Set {
			e.FieldStart("bank_transaction_id")
			s.BankTransactionID.Encode(e)
		}
	}
}

var jsonFieldsNameOfDeliveryAttributes = [4]string{
	0: "short_code",
	1: "pin_code",
	2: "channel",
	3: "bank_transaction_id",
}

// Decode decodes DeliveryAttributes from json.
func (s *DeliveryAttributes) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode DeliveryAttributes to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "short_code":
			if err := func() error {
				s.ShortCode.Reset()
				if err := s.ShortCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"short_code\"")
			}
		case "pin_code":
			if err := func() error {
				s.PinCode.Reset()
				if err := s.PinCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"pin_code\"")
			}
		case "channel":
			if err := func() error {
				s.Channel.Reset()
				if err := s.Channel.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"channel\"")
			}
		case "bank_transaction_id":
			if err := func() error {
				s.BankTransactionID.Reset()
				if err := s.BankTransactionID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"bank_transaction_id\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode DeliveryAttributes")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *DeliveryAttributes) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *DeliveryAttributes) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *DeliveryDryRunInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *DeliveryDryRunInRequest) encodeFields(e *jx.Encoder) {
	{
		if s.ExpectedTime.Set {
			e.FieldStart("expected_time")
			s.ExpectedTime.Encode(e, json.EncodeDateTime)
		}
	}
	{

		e.FieldStart("consumer_data")
		e.Str(s.ConsumerData)
	}
	{
		if s.Region.Set {
			e.FieldStart("region")
			s.Region.Encode(e)
		}
	}
	{

		e.FieldStart("street")
		e.Str(s.Street)
	}
	{
		if s.HouseNumber.Set {
			e.FieldStart("house_number")
			s.HouseNumber.Encode(e)
		}
	}
	{
		if s.FlatNumber.Set {
			e.FieldStart("flat_number")
			s.FlatNumber.Encode(e)
		}
	}
	{
		if s.Entrance.Set {
			e.FieldStart("entrance")
			s.Entrance.Encode(e)
		}
	}
	{
		if s.Intercom.Set {
			e.FieldStart("intercom")
			s.Intercom.Encode(e)
		}
	}
	{
		if s.Floor.Set {
			e.FieldStart("floor")
			s.Floor.Encode(e)
		}
	}
	{
		if s.Location.Set {
			e.FieldStart("location")
			s.Location.Encode(e)
		}
	}
	{
		if s.Comment.Set {
			e.FieldStart("comment")
			s.Comment.Encode(e)
		}
	}
	{
		if s.Attributes.Set {
			e.FieldStart("attributes")
			s.Attributes.Encode(e)
		}
	}
}

var jsonFieldsNameOfDeliveryDryRunInRequest = [12]string{
	0:  "expected_time",
	1:  "consumer_data",
	2:  "region",
	3:  "street",
	4:  "house_number",
	5:  "flat_number",
	6:  "entrance",
	7:  "intercom",
	8:  "floor",
	9:  "location",
	10: "comment",
	11: "attributes",
}

// Decode decodes DeliveryDryRunInRequest from json.
func (s *DeliveryDryRunInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode DeliveryDryRunInRequest to nil")
	}
	var requiredBitSet [2]uint8
	s.setDefaults()

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "expected_time":
			if err := func() error {
				s.ExpectedTime.Reset()
				if err := s.ExpectedTime.Decode(d, json.DecodeDateTime); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"expected_time\"")
			}
		case "consumer_data":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Str()
				s.ConsumerData = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"consumer_data\"")
			}
		case "region":
			if err := func() error {
				s.Region.Reset()
				if err := s.Region.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"region\"")
			}
		case "street":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Str()
				s.Street = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"street\"")
			}
		case "house_number":
			if err := func() error {
				s.HouseNumber.Reset()
				if err := s.HouseNumber.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"house_number\"")
			}
		case "flat_number":
			if err := func() error {
				s.FlatNumber.Reset()
				if err := s.FlatNumber.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"flat_number\"")
			}
		case "entrance":
			if err := func() error {
				s.Entrance.Reset()
				if err := s.Entrance.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"entrance\"")
			}
		case "intercom":
			if err := func() error {
				s.Intercom.Reset()
				if err := s.Intercom.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"intercom\"")
			}
		case "floor":
			if err := func() error {
				s.Floor.Reset()
				if err := s.Floor.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"floor\"")
			}
		case "location":
			if err := func() error {
				s.Location.Reset()
				if err := s.Location.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"location\"")
			}
		case "comment":
			if err := func() error {
				s.Comment.Reset()
				if err := s.Comment.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"comment\"")
			}
		case "attributes":
			if err := func() error {
				s.Attributes.Reset()
				if err := s.Attributes.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"attributes\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode DeliveryDryRunInRequest")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [2]uint8{
		0b00001010,
		0b00000000,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfDeliveryDryRunInRequest) {
					name = jsonFieldsNameOfDeliveryDryRunInRequest[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *DeliveryDryRunInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *DeliveryDryRunInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *HTTPValidationError) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *HTTPValidationError) encodeFields(e *jx.Encoder) {
	{
		if s.Errors != nil {
			e.FieldStart("errors")
			e.ArrStart()
			for _, elem := range s.Errors {
				elem.Encode(e)
			}
			e.ArrEnd()
		}
	}
}

var jsonFieldsNameOfHTTPValidationError = [1]string{
	0: "errors",
}

// Decode decodes HTTPValidationError from json.
func (s *HTTPValidationError) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode HTTPValidationError to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "errors":
			if err := func() error {
				s.Errors = make([]ValidationError, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem ValidationError
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Errors = append(s.Errors, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"errors\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode HTTPValidationError")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *HTTPValidationError) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *HTTPValidationError) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *IngredientForResponse) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *IngredientForResponse) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("id")
		json.EncodeUUID(e, s.ID)
	}
	{

		e.FieldStart("name")
		e.Str(s.Name)
	}
	{

		e.FieldStart("price")
		e.Float64(s.Price)
	}
	{

		e.FieldStart("group_name")
		e.Str(s.GroupName)
	}
	{
		if s.VatCode.Set {
			e.FieldStart("vat_code")
			s.VatCode.Encode(e)
		}
	}
	{
		if s.RkeeperCode.Set {
			e.FieldStart("rkeeper_code")
			s.RkeeperCode.Encode(e)
		}
	}
	{
		if s.ExternalID.Set {
			e.FieldStart("external_id")
			s.ExternalID.Encode(e)
		}
	}
}

var jsonFieldsNameOfIngredientForResponse = [7]string{
	0: "id",
	1: "name",
	2: "price",
	3: "group_name",
	4: "vat_code",
	5: "rkeeper_code",
	6: "external_id",
}

// Decode decodes IngredientForResponse from json.
func (s *IngredientForResponse) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode IngredientForResponse to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "id":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.ID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"id\"")
			}
		case "name":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Str()
				s.Name = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"name\"")
			}
		case "price":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Float64()
				s.Price = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"price\"")
			}
		case "group_name":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Str()
				s.GroupName = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"group_name\"")
			}
		case "vat_code":
			if err := func() error {
				s.VatCode.Reset()
				if err := s.VatCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"vat_code\"")
			}
		case "rkeeper_code":
			if err := func() error {
				s.RkeeperCode.Reset()
				if err := s.RkeeperCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"rkeeper_code\"")
			}
		case "external_id":
			if err := func() error {
				s.ExternalID.Reset()
				if err := s.ExternalID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"external_id\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode IngredientForResponse")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00001111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfIngredientForResponse) {
					name = jsonFieldsNameOfIngredientForResponse[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *IngredientForResponse) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *IngredientForResponse) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *JsonGeometry) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *JsonGeometry) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("data")
		s.Data.Encode(e)
	}
}

var jsonFieldsNameOfJsonGeometry = [1]string{
	0: "data",
}

// Decode decodes JsonGeometry from json.
func (s *JsonGeometry) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode JsonGeometry to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "data":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				if err := s.Data.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"data\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode JsonGeometry")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000001,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfJsonGeometry) {
					name = jsonFieldsNameOfJsonGeometry[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *JsonGeometry) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *JsonGeometry) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *JsonGeometryData) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *JsonGeometryData) encodeFields(e *jx.Encoder) {
}

var jsonFieldsNameOfJsonGeometryData = [0]string{}

// Decode decodes JsonGeometryData from json.
func (s *JsonGeometryData) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode JsonGeometryData to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode JsonGeometryData")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *JsonGeometryData) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *JsonGeometryData) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes bool as json.
func (o OptBool) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	e.Bool(bool(o.Value))
}

// Decode decodes bool from json.
func (o *OptBool) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptBool to nil")
	}
	o.Set = true
	v, err := d.Bool()
	if err != nil {
		return err
	}
	o.Value = bool(v)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptBool) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptBool) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes time.Time as json.
func (o OptDateTime) Encode(e *jx.Encoder, format func(*jx.Encoder, time.Time)) {
	if !o.Set {
		return
	}
	format(e, o.Value)
}

// Decode decodes time.Time from json.
func (o *OptDateTime) Decode(d *jx.Decoder, format func(*jx.Decoder) (time.Time, error)) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptDateTime to nil")
	}
	o.Set = true
	v, err := format(d)
	if err != nil {
		return err
	}
	o.Value = v
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptDateTime) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e, json.EncodeDateTime)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptDateTime) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d, json.DecodeDateTime)
}

// Encode encodes DbModelsBasePoint as json.
func (o OptDbModelsBasePoint) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	o.Value.Encode(e)
}

// Decode decodes DbModelsBasePoint from json.
func (o *OptDbModelsBasePoint) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptDbModelsBasePoint to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptDbModelsBasePoint) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptDbModelsBasePoint) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes DeliveryAttributes as json.
func (o OptDeliveryAttributes) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	o.Value.Encode(e)
}

// Decode decodes DeliveryAttributes from json.
func (o *OptDeliveryAttributes) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptDeliveryAttributes to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptDeliveryAttributes) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptDeliveryAttributes) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes float64 as json.
func (o OptFloat64) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	e.Float64(float64(o.Value))
}

// Decode decodes float64 from json.
func (o *OptFloat64) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptFloat64 to nil")
	}
	o.Set = true
	v, err := d.Float64()
	if err != nil {
		return err
	}
	o.Value = float64(v)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptFloat64) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptFloat64) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes int as json.
func (o OptInt) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	e.Int(int(o.Value))
}

// Decode decodes int from json.
func (o *OptInt) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptInt to nil")
	}
	o.Set = true
	v, err := d.Int()
	if err != nil {
		return err
	}
	o.Value = int(v)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptInt) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptInt) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes OrderCancelCodeEnum as json.
func (o OptOrderCancelCodeEnum) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	e.Int(int(o.Value))
}

// Decode decodes OrderCancelCodeEnum from json.
func (o *OptOrderCancelCodeEnum) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptOrderCancelCodeEnum to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptOrderCancelCodeEnum) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptOrderCancelCodeEnum) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes OrderStatusCallback as json.
func (o OptOrderStatusCallback) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	o.Value.Encode(e)
}

// Decode decodes OrderStatusCallback from json.
func (o *OptOrderStatusCallback) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptOrderStatusCallback to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptOrderStatusCallback) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptOrderStatusCallback) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes OrderStatusCallbackPhotosPhotoType as json.
func (o OptOrderStatusCallbackPhotosPhotoType) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	e.Float64(float64(o.Value))
}

// Decode decodes OrderStatusCallbackPhotosPhotoType from json.
func (o *OptOrderStatusCallbackPhotosPhotoType) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptOrderStatusCallbackPhotosPhotoType to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptOrderStatusCallbackPhotosPhotoType) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptOrderStatusCallbackPhotosPhotoType) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes OrderStatusEnum as json.
func (o OptOrderStatusEnum) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	e.Int(int(o.Value))
}

// Decode decodes OrderStatusEnum from json.
func (o *OptOrderStatusEnum) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptOrderStatusEnum to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptOrderStatusEnum) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptOrderStatusEnum) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes Passport as json.
func (o OptPassport) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	o.Value.Encode(e)
}

// Decode decodes Passport from json.
func (o *OptPassport) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptPassport to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptPassport) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptPassport) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes PaymentMethodEnum as json.
func (o OptPaymentMethodEnum) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	e.Int(int(o.Value))
}

// Decode decodes PaymentMethodEnum from json.
func (o *OptPaymentMethodEnum) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptPaymentMethodEnum to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptPaymentMethodEnum) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptPaymentMethodEnum) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes PaymentStatusEnum as json.
func (o OptPaymentStatusEnum) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	e.Int(int(o.Value))
}

// Decode decodes PaymentStatusEnum from json.
func (o *OptPaymentStatusEnum) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptPaymentStatusEnum to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptPaymentStatusEnum) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptPaymentStatusEnum) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes Point as json.
func (o OptPoint) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	o.Value.Encode(e)
}

// Decode decodes Point from json.
func (o *OptPoint) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptPoint to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptPoint) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptPoint) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes string as json.
func (o OptString) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	e.Str(string(o.Value))
}

// Decode decodes string from json.
func (o *OptString) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptString to nil")
	}
	o.Set = true
	v, err := d.Str()
	if err != nil {
		return err
	}
	o.Value = string(v)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptString) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptString) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes uuid.UUID as json.
func (o OptUUID) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	json.EncodeUUID(e, o.Value)
}

// Decode decodes uuid.UUID from json.
func (o *OptUUID) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptUUID to nil")
	}
	o.Set = true
	v, err := json.DecodeUUID(d)
	if err != nil {
		return err
	}
	o.Value = v
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptUUID) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptUUID) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes UpdateDeliveryInRequest as json.
func (o OptUpdateDeliveryInRequest) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	o.Value.Encode(e)
}

// Decode decodes UpdateDeliveryInRequest from json.
func (o *OptUpdateDeliveryInRequest) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptUpdateDeliveryInRequest to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptUpdateDeliveryInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptUpdateDeliveryInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes Vehicle as json.
func (o OptVehicle) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	o.Value.Encode(e)
}

// Decode decodes Vehicle from json.
func (o *OptVehicle) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptVehicle to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptVehicle) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptVehicle) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes WorkTimes as json.
func (o OptWorkTimes) Encode(e *jx.Encoder) {
	if !o.Set {
		return
	}
	o.Value.Encode(e)
}

// Decode decodes WorkTimes from json.
func (o *OptWorkTimes) Decode(d *jx.Decoder) error {
	if o == nil {
		return errors.New("invalid: unable to decode OptWorkTimes to nil")
	}
	o.Set = true
	if err := o.Value.Decode(d); err != nil {
		return err
	}
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OptWorkTimes) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OptWorkTimes) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes OrderCancelCodeEnum as json.
func (s OrderCancelCodeEnum) Encode(e *jx.Encoder) {
	e.Int(int(s))
}

// Decode decodes OrderCancelCodeEnum from json.
func (s *OrderCancelCodeEnum) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderCancelCodeEnum to nil")
	}
	v, err := d.Int()
	if err != nil {
		return err
	}
	*s = OrderCancelCodeEnum(v)

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OrderCancelCodeEnum) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderCancelCodeEnum) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *OrderCheckInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *OrderCheckInRequest) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("longitude")
		e.Float64(s.Longitude)
	}
	{

		e.FieldStart("latitude")
		e.Float64(s.Latitude)
	}
	{
		if s.Region.Set {
			e.FieldStart("region")
			s.Region.Encode(e)
		}
	}
	{
		if s.CityID.Set {
			e.FieldStart("city_id")
			s.CityID.Encode(e)
		}
	}
	{
		if s.Street.Set {
			e.FieldStart("street")
			s.Street.Encode(e)
		}
	}
	{
		if s.HouseNumber.Set {
			e.FieldStart("house_number")
			s.HouseNumber.Encode(e)
		}
	}
	{
		if s.FlatNumber.Set {
			e.FieldStart("flat_number")
			s.FlatNumber.Encode(e)
		}
	}
	{
		if s.Entrance.Set {
			e.FieldStart("entrance")
			s.Entrance.Encode(e)
		}
	}
	{
		if s.Intercom.Set {
			e.FieldStart("intercom")
			s.Intercom.Encode(e)
		}
	}
	{
		if s.Floor.Set {
			e.FieldStart("floor")
			s.Floor.Encode(e)
		}
	}
	{
		if s.PickupTime.Set {
			e.FieldStart("pickup_time")
			s.PickupTime.Encode(e, json.EncodeDateTime)
		}
	}
	{
		if s.Channel.Set {
			e.FieldStart("channel")
			s.Channel.Encode(e)
		}
	}
}

var jsonFieldsNameOfOrderCheckInRequest = [12]string{
	0:  "longitude",
	1:  "latitude",
	2:  "region",
	3:  "city_id",
	4:  "street",
	5:  "house_number",
	6:  "flat_number",
	7:  "entrance",
	8:  "intercom",
	9:  "floor",
	10: "pickup_time",
	11: "channel",
}

// Decode decodes OrderCheckInRequest from json.
func (s *OrderCheckInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderCheckInRequest to nil")
	}
	var requiredBitSet [2]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "longitude":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := d.Float64()
				s.Longitude = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"longitude\"")
			}
		case "latitude":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Float64()
				s.Latitude = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"latitude\"")
			}
		case "region":
			if err := func() error {
				s.Region.Reset()
				if err := s.Region.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"region\"")
			}
		case "city_id":
			if err := func() error {
				s.CityID.Reset()
				if err := s.CityID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"city_id\"")
			}
		case "street":
			if err := func() error {
				s.Street.Reset()
				if err := s.Street.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"street\"")
			}
		case "house_number":
			if err := func() error {
				s.HouseNumber.Reset()
				if err := s.HouseNumber.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"house_number\"")
			}
		case "flat_number":
			if err := func() error {
				s.FlatNumber.Reset()
				if err := s.FlatNumber.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"flat_number\"")
			}
		case "entrance":
			if err := func() error {
				s.Entrance.Reset()
				if err := s.Entrance.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"entrance\"")
			}
		case "intercom":
			if err := func() error {
				s.Intercom.Reset()
				if err := s.Intercom.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"intercom\"")
			}
		case "floor":
			if err := func() error {
				s.Floor.Reset()
				if err := s.Floor.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"floor\"")
			}
		case "pickup_time":
			if err := func() error {
				s.PickupTime.Reset()
				if err := s.PickupTime.Decode(d, json.DecodeDateTime); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"pickup_time\"")
			}
		case "channel":
			if err := func() error {
				s.Channel.Reset()
				if err := s.Channel.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"channel\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode OrderCheckInRequest")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [2]uint8{
		0b00000011,
		0b00000000,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfOrderCheckInRequest) {
					name = jsonFieldsNameOfOrderCheckInRequest[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *OrderCheckInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderCheckInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *OrderCost) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *OrderCost) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("cost")
		e.Float64(s.Cost)
	}
	{

		e.FieldStart("additional_cost")
		e.Float64(s.AdditionalCost)
	}
	{
		if s.ChangeCost.Set {
			e.FieldStart("change_cost")
			s.ChangeCost.Encode(e)
		}
	}
}

var jsonFieldsNameOfOrderCost = [3]string{
	0: "cost",
	1: "additional_cost",
	2: "change_cost",
}

// Decode decodes OrderCost from json.
func (s *OrderCost) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderCost to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "cost":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := d.Float64()
				s.Cost = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cost\"")
			}
		case "additional_cost":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Float64()
				s.AdditionalCost = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"additional_cost\"")
			}
		case "change_cost":
			if err := func() error {
				s.ChangeCost.Reset()
				if err := s.ChangeCost.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"change_cost\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode OrderCost")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000011,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfOrderCost) {
					name = jsonFieldsNameOfOrderCost[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *OrderCost) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderCost) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *OrderCustomer) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *OrderCustomer) encodeFields(e *jx.Encoder) {
	{
		if s.Name.Set {
			e.FieldStart("name")
			s.Name.Encode(e)
		}
	}
	{
		if s.Phone.Set {
			e.FieldStart("phone")
			s.Phone.Encode(e)
		}
	}
	{
		if s.Email.Set {
			e.FieldStart("email")
			s.Email.Encode(e)
		}
	}
}

var jsonFieldsNameOfOrderCustomer = [3]string{
	0: "name",
	1: "phone",
	2: "email",
}

// Decode decodes OrderCustomer from json.
func (s *OrderCustomer) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderCustomer to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "name":
			if err := func() error {
				s.Name.Reset()
				if err := s.Name.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"name\"")
			}
		case "phone":
			if err := func() error {
				s.Phone.Reset()
				if err := s.Phone.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"phone\"")
			}
		case "email":
			if err := func() error {
				s.Email.Reset()
				if err := s.Email.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"email\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode OrderCustomer")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *OrderCustomer) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderCustomer) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *OrderDryRun) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *OrderDryRun) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("place_id")
		json.EncodeUUID(e, s.PlaceID)
	}
	{

		e.FieldStart("cost")
		s.Cost.Encode(e)
	}
	{

		e.FieldStart("person_count")
		e.Int(s.PersonCount)
	}
	{
		if s.Comment.Set {
			e.FieldStart("comment")
			s.Comment.Encode(e)
		}
	}
	{

		e.FieldStart("products")
		e.ArrStart()
		for _, elem := range s.Products {
			elem.Encode(e)
		}
		e.ArrEnd()
	}
	{
		if s.AdditionalServices != nil {
			e.FieldStart("additional_services")
			e.ArrStart()
			for _, elem := range s.AdditionalServices {
				elem.Encode(e)
			}
			e.ArrEnd()
		}
	}
	{
		if s.IsForceStart.Set {
			e.FieldStart("is_force_start")
			s.IsForceStart.Encode(e)
		}
	}
	{

		e.FieldStart("delivery")
		s.Delivery.Encode(e)
	}
}

var jsonFieldsNameOfOrderDryRun = [8]string{
	0: "place_id",
	1: "cost",
	2: "person_count",
	3: "comment",
	4: "products",
	5: "additional_services",
	6: "is_force_start",
	7: "delivery",
}

// Decode decodes OrderDryRun from json.
func (s *OrderDryRun) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderDryRun to nil")
	}
	var requiredBitSet [1]uint8
	s.setDefaults()

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "place_id":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.PlaceID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"place_id\"")
			}
		case "cost":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				if err := s.Cost.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cost\"")
			}
		case "person_count":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Int()
				s.PersonCount = int(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"person_count\"")
			}
		case "comment":
			if err := func() error {
				s.Comment.Reset()
				if err := s.Comment.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"comment\"")
			}
		case "products":
			requiredBitSet[0] |= 1 << 4
			if err := func() error {
				s.Products = make([]CreateProductInRequest, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem CreateProductInRequest
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Products = append(s.Products, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"products\"")
			}
		case "additional_services":
			if err := func() error {
				s.AdditionalServices = make([]AdditionalServiceCodeNameOnly, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem AdditionalServiceCodeNameOnly
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.AdditionalServices = append(s.AdditionalServices, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"additional_services\"")
			}
		case "is_force_start":
			if err := func() error {
				s.IsForceStart.Reset()
				if err := s.IsForceStart.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"is_force_start\"")
			}
		case "delivery":
			requiredBitSet[0] |= 1 << 7
			if err := func() error {
				if err := s.Delivery.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"delivery\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode OrderDryRun")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b10010111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfOrderDryRun) {
					name = jsonFieldsNameOfOrderDryRun[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *OrderDryRun) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderDryRun) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *OrderMerchant) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *OrderMerchant) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("id")
		json.EncodeUUID(e, s.ID)
	}
	{
		if s.Name.Set {
			e.FieldStart("name")
			s.Name.Encode(e)
		}
	}
}

var jsonFieldsNameOfOrderMerchant = [2]string{
	0: "id",
	1: "name",
}

// Decode decodes OrderMerchant from json.
func (s *OrderMerchant) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderMerchant to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "id":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.ID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"id\"")
			}
		case "name":
			if err := func() error {
				s.Name.Reset()
				if err := s.Name.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"name\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode OrderMerchant")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000001,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfOrderMerchant) {
					name = jsonFieldsNameOfOrderMerchant[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *OrderMerchant) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderMerchant) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *OrderPhotoForResponse) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *OrderPhotoForResponse) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("id")
		json.EncodeUUID(e, s.ID)
	}
	{

		e.FieldStart("photo_url")
		e.Str(s.PhotoURL)
	}
	{

		e.FieldStart("photo_type")
		s.PhotoType.Encode(e)
	}
	{

		e.FieldStart("created_at")
		json.EncodeDateTime(e, s.CreatedAt)
	}
}

var jsonFieldsNameOfOrderPhotoForResponse = [4]string{
	0: "id",
	1: "photo_url",
	2: "photo_type",
	3: "created_at",
}

// Decode decodes OrderPhotoForResponse from json.
func (s *OrderPhotoForResponse) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderPhotoForResponse to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "id":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.ID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"id\"")
			}
		case "photo_url":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Str()
				s.PhotoURL = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"photo_url\"")
			}
		case "photo_type":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				if err := s.PhotoType.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"photo_type\"")
			}
		case "created_at":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := json.DecodeDateTime(d)
				s.CreatedAt = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"created_at\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode OrderPhotoForResponse")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00001111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfOrderPhotoForResponse) {
					name = jsonFieldsNameOfOrderPhotoForResponse[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *OrderPhotoForResponse) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderPhotoForResponse) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes OrderPhotoType as json.
func (s OrderPhotoType) Encode(e *jx.Encoder) {
	e.Int(int(s))
}

// Decode decodes OrderPhotoType from json.
func (s *OrderPhotoType) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderPhotoType to nil")
	}
	v, err := d.Int()
	if err != nil {
		return err
	}
	*s = OrderPhotoType(v)

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OrderPhotoType) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderPhotoType) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *OrderStatusCallback) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *OrderStatusCallback) encodeFields(e *jx.Encoder) {
	{
		if s.Status.Set {
			e.FieldStart("status")
			s.Status.Encode(e)
		}
	}
	{
		if s.CancelCode.Set {
			e.FieldStart("cancel_code")
			s.CancelCode.Encode(e)
		}
	}
	{
		if s.CancelReason.Set {
			e.FieldStart("cancel_reason")
			s.CancelReason.Encode(e)
		}
	}
	{
		if s.DeliveryExpectedTime.Set {
			e.FieldStart("delivery_expected_time")
			s.DeliveryExpectedTime.Encode(e, json.EncodeDateTime)
		}
	}
	{
		if s.Photos != nil {
			e.FieldStart("photos")
			e.ArrStart()
			for _, elem := range s.Photos {
				elem.Encode(e)
			}
			e.ArrEnd()
		}
	}
}

var jsonFieldsNameOfOrderStatusCallback = [5]string{
	0: "status",
	1: "cancel_code",
	2: "cancel_reason",
	3: "delivery_expected_time",
	4: "photos",
}

// Decode decodes OrderStatusCallback from json.
func (s *OrderStatusCallback) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderStatusCallback to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "status":
			if err := func() error {
				s.Status.Reset()
				if err := s.Status.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"status\"")
			}
		case "cancel_code":
			if err := func() error {
				s.CancelCode.Reset()
				if err := s.CancelCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cancel_code\"")
			}
		case "cancel_reason":
			if err := func() error {
				s.CancelReason.Reset()
				if err := s.CancelReason.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cancel_reason\"")
			}
		case "delivery_expected_time":
			if err := func() error {
				s.DeliveryExpectedTime.Reset()
				if err := s.DeliveryExpectedTime.Decode(d, json.DecodeDateTime); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"delivery_expected_time\"")
			}
		case "photos":
			if err := func() error {
				s.Photos = make([]OrderStatusCallbackPhotos, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem OrderStatusCallbackPhotos
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Photos = append(s.Photos, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"photos\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode OrderStatusCallback")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *OrderStatusCallback) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderStatusCallback) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *OrderStatusCallbackPhotos) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *OrderStatusCallbackPhotos) encodeFields(e *jx.Encoder) {
	{
		if s.ID.Set {
			e.FieldStart("id")
			s.ID.Encode(e)
		}
	}
	{
		if s.URL.Set {
			e.FieldStart("url")
			s.URL.Encode(e)
		}
	}
	{
		if s.PhotoType.Set {
			e.FieldStart("photo_type")
			s.PhotoType.Encode(e)
		}
	}
}

var jsonFieldsNameOfOrderStatusCallbackPhotos = [3]string{
	0: "id",
	1: "url",
	2: "photo_type",
}

// Decode decodes OrderStatusCallbackPhotos from json.
func (s *OrderStatusCallbackPhotos) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderStatusCallbackPhotos to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "id":
			if err := func() error {
				s.ID.Reset()
				if err := s.ID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"id\"")
			}
		case "url":
			if err := func() error {
				s.URL.Reset()
				if err := s.URL.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"url\"")
			}
		case "photo_type":
			if err := func() error {
				s.PhotoType.Reset()
				if err := s.PhotoType.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"photo_type\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode OrderStatusCallbackPhotos")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *OrderStatusCallbackPhotos) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderStatusCallbackPhotos) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes OrderStatusCallbackPhotosPhotoType as json.
func (s OrderStatusCallbackPhotosPhotoType) Encode(e *jx.Encoder) {
	e.Float64(float64(s))
}

// Decode decodes OrderStatusCallbackPhotosPhotoType from json.
func (s *OrderStatusCallbackPhotosPhotoType) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderStatusCallbackPhotosPhotoType to nil")
	}
	v, err := d.Float64()
	if err != nil {
		return err
	}
	*s = OrderStatusCallbackPhotosPhotoType(v)

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OrderStatusCallbackPhotosPhotoType) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderStatusCallbackPhotosPhotoType) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes OrderStatusEnum as json.
func (s OrderStatusEnum) Encode(e *jx.Encoder) {
	e.Int(int(s))
}

// Decode decodes OrderStatusEnum from json.
func (s *OrderStatusEnum) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode OrderStatusEnum to nil")
	}
	v, err := d.Int()
	if err != nil {
		return err
	}
	*s = OrderStatusEnum(v)

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s OrderStatusEnum) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *OrderStatusEnum) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *Passport) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *Passport) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("series")
		e.Str(s.Series)
	}
	{

		e.FieldStart("number")
		e.Str(s.Number)
	}
}

var jsonFieldsNameOfPassport = [2]string{
	0: "series",
	1: "number",
}

// Decode decodes Passport from json.
func (s *Passport) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode Passport to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "series":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := d.Str()
				s.Series = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"series\"")
			}
		case "number":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Str()
				s.Number = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"number\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode Passport")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000011,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfPassport) {
					name = jsonFieldsNameOfPassport[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *Passport) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *Passport) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes PaymentMethodEnum as json.
func (s PaymentMethodEnum) Encode(e *jx.Encoder) {
	e.Int(int(s))
}

// Decode decodes PaymentMethodEnum from json.
func (s *PaymentMethodEnum) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PaymentMethodEnum to nil")
	}
	v, err := d.Int()
	if err != nil {
		return err
	}
	*s = PaymentMethodEnum(v)

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s PaymentMethodEnum) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PaymentMethodEnum) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes PaymentStatusEnum as json.
func (s PaymentStatusEnum) Encode(e *jx.Encoder) {
	e.Int(int(s))
}

// Decode decodes PaymentStatusEnum from json.
func (s *PaymentStatusEnum) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PaymentStatusEnum to nil")
	}
	v, err := d.Int()
	if err != nil {
		return err
	}
	*s = PaymentStatusEnum(v)

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s PaymentStatusEnum) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PaymentStatusEnum) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PlaceAccessForResponse) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PlaceAccessForResponse) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("id")
		json.EncodeUUID(e, s.ID)
	}
	{

		e.FieldStart("merchant_place_id")
		e.Str(s.MerchantPlaceID)
	}
	{
		if s.PickupTime.Set {
			e.FieldStart("pickup_time")
			s.PickupTime.Encode(e, json.EncodeDateTime)
		}
	}
	{
		if s.IntervalStart.Set {
			e.FieldStart("interval_start")
			s.IntervalStart.Encode(e, json.EncodeDateTime)
		}
	}
	{
		if s.IntervalEnd.Set {
			e.FieldStart("interval_end")
			s.IntervalEnd.Encode(e, json.EncodeDateTime)
		}
	}
	{

		e.FieldStart("status")
		s.Status.Encode(e)
	}
	{
		if s.DeliveryFee.Set {
			e.FieldStart("delivery_fee")
			s.DeliveryFee.Encode(e)
		}
	}
}

var jsonFieldsNameOfPlaceAccessForResponse = [7]string{
	0: "id",
	1: "merchant_place_id",
	2: "pickup_time",
	3: "interval_start",
	4: "interval_end",
	5: "status",
	6: "delivery_fee",
}

// Decode decodes PlaceAccessForResponse from json.
func (s *PlaceAccessForResponse) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlaceAccessForResponse to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "id":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.ID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"id\"")
			}
		case "merchant_place_id":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Str()
				s.MerchantPlaceID = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"merchant_place_id\"")
			}
		case "pickup_time":
			if err := func() error {
				s.PickupTime.Reset()
				if err := s.PickupTime.Decode(d, json.DecodeDateTime); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"pickup_time\"")
			}
		case "interval_start":
			if err := func() error {
				s.IntervalStart.Reset()
				if err := s.IntervalStart.Decode(d, json.DecodeDateTime); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"interval_start\"")
			}
		case "interval_end":
			if err := func() error {
				s.IntervalEnd.Reset()
				if err := s.IntervalEnd.Decode(d, json.DecodeDateTime); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"interval_end\"")
			}
		case "status":
			requiredBitSet[0] |= 1 << 5
			if err := func() error {
				if err := s.Status.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"status\"")
			}
		case "delivery_fee":
			if err := func() error {
				s.DeliveryFee.Reset()
				if err := s.DeliveryFee.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"delivery_fee\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PlaceAccessForResponse")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00100011,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfPlaceAccessForResponse) {
					name = jsonFieldsNameOfPlaceAccessForResponse[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PlaceAccessForResponse) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlaceAccessForResponse) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PlaceAccessForResponseStatus) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PlaceAccessForResponseStatus) encodeFields(e *jx.Encoder) {
}

var jsonFieldsNameOfPlaceAccessForResponseStatus = [0]string{}

// Decode decodes PlaceAccessForResponseStatus from json.
func (s *PlaceAccessForResponseStatus) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlaceAccessForResponseStatus to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PlaceAccessForResponseStatus")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PlaceAccessForResponseStatus) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlaceAccessForResponseStatus) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PlaceAccessList) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PlaceAccessList) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("is_access")
		e.Bool(s.IsAccess)
	}
	{

		e.FieldStart("places")
		e.ArrStart()
		for _, elem := range s.Places {
			elem.Encode(e)
		}
		e.ArrEnd()
	}
}

var jsonFieldsNameOfPlaceAccessList = [2]string{
	0: "is_access",
	1: "places",
}

// Decode decodes PlaceAccessList from json.
func (s *PlaceAccessList) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlaceAccessList to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "is_access":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := d.Bool()
				s.IsAccess = bool(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"is_access\"")
			}
		case "places":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				s.Places = make([]PlaceAccessForResponse, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem PlaceAccessForResponse
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Places = append(s.Places, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"places\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PlaceAccessList")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000011,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfPlaceAccessList) {
					name = jsonFieldsNameOfPlaceAccessList[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PlaceAccessList) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlaceAccessList) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PlaceBlockInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PlaceBlockInRequest) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("place_ids")
		e.ArrStart()
		for _, elem := range s.PlaceIds {
			json.EncodeUUID(e, elem)
		}
		e.ArrEnd()
	}
	{

		e.FieldStart("datetime_from")
		json.EncodeDateTime(e, s.DatetimeFrom)
	}
	{
		if s.DatetimeTo.Set {
			e.FieldStart("datetime_to")
			s.DatetimeTo.Encode(e, json.EncodeDateTime)
		}
	}
	{
		if s.BlockReason.Set {
			e.FieldStart("block_reason")
			s.BlockReason.Encode(e)
		}
	}
	{
		if s.ExtraData != nil {
			e.FieldStart("extra_data")
			s.ExtraData.Encode(e)
		}
	}
}

var jsonFieldsNameOfPlaceBlockInRequest = [5]string{
	0: "place_ids",
	1: "datetime_from",
	2: "datetime_to",
	3: "block_reason",
	4: "extra_data",
}

// Decode decodes PlaceBlockInRequest from json.
func (s *PlaceBlockInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlaceBlockInRequest to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "place_ids":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				s.PlaceIds = make([]uuid.UUID, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem uuid.UUID
					v, err := json.DecodeUUID(d)
					elem = v
					if err != nil {
						return err
					}
					s.PlaceIds = append(s.PlaceIds, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"place_ids\"")
			}
		case "datetime_from":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := json.DecodeDateTime(d)
				s.DatetimeFrom = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"datetime_from\"")
			}
		case "datetime_to":
			if err := func() error {
				s.DatetimeTo.Reset()
				if err := s.DatetimeTo.Decode(d, json.DecodeDateTime); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"datetime_to\"")
			}
		case "block_reason":
			if err := func() error {
				s.BlockReason.Reset()
				if err := s.BlockReason.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"block_reason\"")
			}
		case "extra_data":
			if err := func() error {
				s.ExtraData = nil
				var elem PlaceBlockInRequestExtraData
				if err := elem.Decode(d); err != nil {
					return err
				}
				s.ExtraData = &elem
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"extra_data\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PlaceBlockInRequest")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000011,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfPlaceBlockInRequest) {
					name = jsonFieldsNameOfPlaceBlockInRequest[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PlaceBlockInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlaceBlockInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PlaceBlockInRequestExtraData) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PlaceBlockInRequestExtraData) encodeFields(e *jx.Encoder) {
}

var jsonFieldsNameOfPlaceBlockInRequestExtraData = [0]string{}

// Decode decodes PlaceBlockInRequestExtraData from json.
func (s *PlaceBlockInRequestExtraData) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlaceBlockInRequestExtraData to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PlaceBlockInRequestExtraData")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PlaceBlockInRequestExtraData) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlaceBlockInRequestExtraData) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PlaceBlockInResponse) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PlaceBlockInResponse) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("id")
		json.EncodeUUID(e, s.ID)
	}
	{

		e.FieldStart("place_id")
		json.EncodeUUID(e, s.PlaceID)
	}
	{

		e.FieldStart("datetime_from")
		json.EncodeDateTime(e, s.DatetimeFrom)
	}
	{
		if s.DatetimeTo.Set {
			e.FieldStart("datetime_to")
			s.DatetimeTo.Encode(e, json.EncodeDateTime)
		}
	}
	{
		if s.BlockReason.Set {
			e.FieldStart("block_reason")
			s.BlockReason.Encode(e)
		}
	}
	{

		e.FieldStart("is_blocked")
		e.Bool(s.IsBlocked)
	}
}

var jsonFieldsNameOfPlaceBlockInResponse = [6]string{
	0: "id",
	1: "place_id",
	2: "datetime_from",
	3: "datetime_to",
	4: "block_reason",
	5: "is_blocked",
}

// Decode decodes PlaceBlockInResponse from json.
func (s *PlaceBlockInResponse) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlaceBlockInResponse to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "id":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.ID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"id\"")
			}
		case "place_id":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.PlaceID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"place_id\"")
			}
		case "datetime_from":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := json.DecodeDateTime(d)
				s.DatetimeFrom = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"datetime_from\"")
			}
		case "datetime_to":
			if err := func() error {
				s.DatetimeTo.Reset()
				if err := s.DatetimeTo.Decode(d, json.DecodeDateTime); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"datetime_to\"")
			}
		case "block_reason":
			if err := func() error {
				s.BlockReason.Reset()
				if err := s.BlockReason.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"block_reason\"")
			}
		case "is_blocked":
			requiredBitSet[0] |= 1 << 5
			if err := func() error {
				v, err := d.Bool()
				s.IsBlocked = bool(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"is_blocked\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PlaceBlockInResponse")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00100111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfPlaceBlockInResponse) {
					name = jsonFieldsNameOfPlaceBlockInResponse[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PlaceBlockInResponse) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlaceBlockInResponse) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PlaceInDB) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PlaceInDB) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("location")
		s.Location.Encode(e)
	}
	{

		e.FieldStart("merchant_id")
		json.EncodeUUID(e, s.MerchantID)
	}
	{
		if s.MerchantPlaceID.Set {
			e.FieldStart("merchant_place_id")
			s.MerchantPlaceID.Encode(e)
		}
	}
	{

		e.FieldStart("address")
		e.Str(s.Address)
	}
	{

		e.FieldStart("name")
		e.Str(s.Name)
	}
	{

		e.FieldStart("city_id")
		e.Int(s.CityID)
	}
	{
		if s.Email.Set {
			e.FieldStart("email")
			s.Email.Encode(e)
		}
	}
	{

		e.FieldStart("id")
		json.EncodeUUID(e, s.ID)
	}
	{
		if s.WorkTimes.Set {
			e.FieldStart("work_times")
			s.WorkTimes.Encode(e)
		}
	}
	{
		if s.IsBlocked.Set {
			e.FieldStart("is_blocked")
			s.IsBlocked.Encode(e)
		}
	}
}

var jsonFieldsNameOfPlaceInDB = [10]string{
	0: "location",
	1: "merchant_id",
	2: "merchant_place_id",
	3: "address",
	4: "name",
	5: "city_id",
	6: "email",
	7: "id",
	8: "work_times",
	9: "is_blocked",
}

// Decode decodes PlaceInDB from json.
func (s *PlaceInDB) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlaceInDB to nil")
	}
	var requiredBitSet [2]uint8
	s.setDefaults()

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "location":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				if err := s.Location.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"location\"")
			}
		case "merchant_id":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.MerchantID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"merchant_id\"")
			}
		case "merchant_place_id":
			if err := func() error {
				s.MerchantPlaceID.Reset()
				if err := s.MerchantPlaceID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"merchant_place_id\"")
			}
		case "address":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Str()
				s.Address = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"address\"")
			}
		case "name":
			requiredBitSet[0] |= 1 << 4
			if err := func() error {
				v, err := d.Str()
				s.Name = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"name\"")
			}
		case "city_id":
			requiredBitSet[0] |= 1 << 5
			if err := func() error {
				v, err := d.Int()
				s.CityID = int(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"city_id\"")
			}
		case "email":
			if err := func() error {
				s.Email.Reset()
				if err := s.Email.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"email\"")
			}
		case "id":
			requiredBitSet[0] |= 1 << 7
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.ID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"id\"")
			}
		case "work_times":
			if err := func() error {
				s.WorkTimes.Reset()
				if err := s.WorkTimes.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"work_times\"")
			}
		case "is_blocked":
			if err := func() error {
				s.IsBlocked.Reset()
				if err := s.IsBlocked.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"is_blocked\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PlaceInDB")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [2]uint8{
		0b10111011,
		0b00000000,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfPlaceInDB) {
					name = jsonFieldsNameOfPlaceInDB[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PlaceInDB) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlaceInDB) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PlaceUnBlockInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PlaceUnBlockInRequest) encodeFields(e *jx.Encoder) {
	{
		if s.PlaceIds != nil {
			e.FieldStart("place_ids")
			e.ArrStart()
			for _, elem := range s.PlaceIds {
				json.EncodeUUID(e, elem)
			}
			e.ArrEnd()
		}
	}
	{
		if s.BlockIds != nil {
			e.FieldStart("block_ids")
			e.ArrStart()
			for _, elem := range s.BlockIds {
				json.EncodeUUID(e, elem)
			}
			e.ArrEnd()
		}
	}
}

var jsonFieldsNameOfPlaceUnBlockInRequest = [2]string{
	0: "place_ids",
	1: "block_ids",
}

// Decode decodes PlaceUnBlockInRequest from json.
func (s *PlaceUnBlockInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlaceUnBlockInRequest to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "place_ids":
			if err := func() error {
				s.PlaceIds = make([]uuid.UUID, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem uuid.UUID
					v, err := json.DecodeUUID(d)
					elem = v
					if err != nil {
						return err
					}
					s.PlaceIds = append(s.PlaceIds, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"place_ids\"")
			}
		case "block_ids":
			if err := func() error {
				s.BlockIds = make([]uuid.UUID, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem uuid.UUID
					v, err := json.DecodeUUID(d)
					elem = v
					if err != nil {
						return err
					}
					s.BlockIds = append(s.BlockIds, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"block_ids\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PlaceUnBlockInRequest")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PlaceUnBlockInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlaceUnBlockInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PlaceUpdate) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PlaceUpdate) encodeFields(e *jx.Encoder) {
	{
		if s.Location.Set {
			e.FieldStart("location")
			s.Location.Encode(e)
		}
	}
	{
		if s.Address.Set {
			e.FieldStart("address")
			s.Address.Encode(e)
		}
	}
	{
		if s.Name.Set {
			e.FieldStart("name")
			s.Name.Encode(e)
		}
	}
	{
		if s.CityID.Set {
			e.FieldStart("city_id")
			s.CityID.Encode(e)
		}
	}
	{
		if s.Email.Set {
			e.FieldStart("email")
			s.Email.Encode(e)
		}
	}
	{
		if s.MerchantPlaceID.Set {
			e.FieldStart("merchant_place_id")
			s.MerchantPlaceID.Encode(e)
		}
	}
}

var jsonFieldsNameOfPlaceUpdate = [6]string{
	0: "location",
	1: "address",
	2: "name",
	3: "city_id",
	4: "email",
	5: "merchant_place_id",
}

// Decode decodes PlaceUpdate from json.
func (s *PlaceUpdate) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlaceUpdate to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "location":
			if err := func() error {
				s.Location.Reset()
				if err := s.Location.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"location\"")
			}
		case "address":
			if err := func() error {
				s.Address.Reset()
				if err := s.Address.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"address\"")
			}
		case "name":
			if err := func() error {
				s.Name.Reset()
				if err := s.Name.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"name\"")
			}
		case "city_id":
			if err := func() error {
				s.CityID.Reset()
				if err := s.CityID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"city_id\"")
			}
		case "email":
			if err := func() error {
				s.Email.Reset()
				if err := s.Email.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"email\"")
			}
		case "merchant_place_id":
			if err := func() error {
				s.MerchantPlaceID.Reset()
				if err := s.MerchantPlaceID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"merchant_place_id\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PlaceUpdate")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PlaceUpdate) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlaceUpdate) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PlaceWithArea) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PlaceWithArea) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("location")
		s.Location.Encode(e)
	}
	{

		e.FieldStart("merchant_place_id")
		e.Str(s.MerchantPlaceID)
	}
	{

		e.FieldStart("address")
		e.Str(s.Address)
	}
	{

		e.FieldStart("name")
		e.Str(s.Name)
	}
	{

		e.FieldStart("city_id")
		e.Int(s.CityID)
	}
	{

		e.FieldStart("area")
		s.Area.Encode(e)
	}
}

var jsonFieldsNameOfPlaceWithArea = [6]string{
	0: "location",
	1: "merchant_place_id",
	2: "address",
	3: "name",
	4: "city_id",
	5: "area",
}

// Decode decodes PlaceWithArea from json.
func (s *PlaceWithArea) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlaceWithArea to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "location":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				if err := s.Location.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"location\"")
			}
		case "merchant_place_id":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Str()
				s.MerchantPlaceID = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"merchant_place_id\"")
			}
		case "address":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Str()
				s.Address = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"address\"")
			}
		case "name":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Str()
				s.Name = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"name\"")
			}
		case "city_id":
			requiredBitSet[0] |= 1 << 4
			if err := func() error {
				v, err := d.Int()
				s.CityID = int(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"city_id\"")
			}
		case "area":
			requiredBitSet[0] |= 1 << 5
			if err := func() error {
				if err := s.Area.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"area\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PlaceWithArea")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00111111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfPlaceWithArea) {
					name = jsonFieldsNameOfPlaceWithArea[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PlaceWithArea) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlaceWithArea) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes PlacesBlockPublicV1MerchantPlacesBlockPostOKApplicationJSON as json.
func (s PlacesBlockPublicV1MerchantPlacesBlockPostOKApplicationJSON) Encode(e *jx.Encoder) {
	unwrapped := []PlaceBlockInResponse(s)

	e.ArrStart()
	for _, elem := range unwrapped {
		elem.Encode(e)
	}
	e.ArrEnd()
}

// Decode decodes PlacesBlockPublicV1MerchantPlacesBlockPostOKApplicationJSON from json.
func (s *PlacesBlockPublicV1MerchantPlacesBlockPostOKApplicationJSON) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlacesBlockPublicV1MerchantPlacesBlockPostOKApplicationJSON to nil")
	}
	var unwrapped []PlaceBlockInResponse
	if err := func() error {
		unwrapped = make([]PlaceBlockInResponse, 0)
		if err := d.Arr(func(d *jx.Decoder) error {
			var elem PlaceBlockInResponse
			if err := elem.Decode(d); err != nil {
				return err
			}
			unwrapped = append(unwrapped, elem)
			return nil
		}); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return errors.Wrap(err, "alias")
	}
	*s = PlacesBlockPublicV1MerchantPlacesBlockPostOKApplicationJSON(unwrapped)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s PlacesBlockPublicV1MerchantPlacesBlockPostOKApplicationJSON) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlacesBlockPublicV1MerchantPlacesBlockPostOKApplicationJSON) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes PlacesBlocksPublicV1MerchantPlacesBlocksGetOKApplicationJSON as json.
func (s PlacesBlocksPublicV1MerchantPlacesBlocksGetOKApplicationJSON) Encode(e *jx.Encoder) {
	unwrapped := []PlaceBlockInResponse(s)

	e.ArrStart()
	for _, elem := range unwrapped {
		elem.Encode(e)
	}
	e.ArrEnd()
}

// Decode decodes PlacesBlocksPublicV1MerchantPlacesBlocksGetOKApplicationJSON from json.
func (s *PlacesBlocksPublicV1MerchantPlacesBlocksGetOKApplicationJSON) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlacesBlocksPublicV1MerchantPlacesBlocksGetOKApplicationJSON to nil")
	}
	var unwrapped []PlaceBlockInResponse
	if err := func() error {
		unwrapped = make([]PlaceBlockInResponse, 0)
		if err := d.Arr(func(d *jx.Decoder) error {
			var elem PlaceBlockInResponse
			if err := elem.Decode(d); err != nil {
				return err
			}
			unwrapped = append(unwrapped, elem)
			return nil
		}); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return errors.Wrap(err, "alias")
	}
	*s = PlacesBlocksPublicV1MerchantPlacesBlocksGetOKApplicationJSON(unwrapped)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s PlacesBlocksPublicV1MerchantPlacesBlocksGetOKApplicationJSON) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlacesBlocksPublicV1MerchantPlacesBlocksGetOKApplicationJSON) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes PlacesListPublicV1MerchantPlacesGetOKApplicationJSON as json.
func (s PlacesListPublicV1MerchantPlacesGetOKApplicationJSON) Encode(e *jx.Encoder) {
	unwrapped := []PlaceInDB(s)

	e.ArrStart()
	for _, elem := range unwrapped {
		elem.Encode(e)
	}
	e.ArrEnd()
}

// Decode decodes PlacesListPublicV1MerchantPlacesGetOKApplicationJSON from json.
func (s *PlacesListPublicV1MerchantPlacesGetOKApplicationJSON) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlacesListPublicV1MerchantPlacesGetOKApplicationJSON to nil")
	}
	var unwrapped []PlaceInDB
	if err := func() error {
		unwrapped = make([]PlaceInDB, 0)
		if err := d.Arr(func(d *jx.Decoder) error {
			var elem PlaceInDB
			if err := elem.Decode(d); err != nil {
				return err
			}
			unwrapped = append(unwrapped, elem)
			return nil
		}); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return errors.Wrap(err, "alias")
	}
	*s = PlacesListPublicV1MerchantPlacesGetOKApplicationJSON(unwrapped)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s PlacesListPublicV1MerchantPlacesGetOKApplicationJSON) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlacesListPublicV1MerchantPlacesGetOKApplicationJSON) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes PlacesUnblockPublicV1MerchantPlacesUnblockPostOKApplicationJSON as json.
func (s PlacesUnblockPublicV1MerchantPlacesUnblockPostOKApplicationJSON) Encode(e *jx.Encoder) {
	unwrapped := []PlaceBlockInResponse(s)

	e.ArrStart()
	for _, elem := range unwrapped {
		elem.Encode(e)
	}
	e.ArrEnd()
}

// Decode decodes PlacesUnblockPublicV1MerchantPlacesUnblockPostOKApplicationJSON from json.
func (s *PlacesUnblockPublicV1MerchantPlacesUnblockPostOKApplicationJSON) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PlacesUnblockPublicV1MerchantPlacesUnblockPostOKApplicationJSON to nil")
	}
	var unwrapped []PlaceBlockInResponse
	if err := func() error {
		unwrapped = make([]PlaceBlockInResponse, 0)
		if err := d.Arr(func(d *jx.Decoder) error {
			var elem PlaceBlockInResponse
			if err := elem.Decode(d); err != nil {
				return err
			}
			unwrapped = append(unwrapped, elem)
			return nil
		}); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return errors.Wrap(err, "alias")
	}
	*s = PlacesUnblockPublicV1MerchantPlacesUnblockPostOKApplicationJSON(unwrapped)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s PlacesUnblockPublicV1MerchantPlacesUnblockPostOKApplicationJSON) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PlacesUnblockPublicV1MerchantPlacesUnblockPostOKApplicationJSON) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *Point) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *Point) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("latitude")
		e.Float64(s.Latitude)
	}
	{

		e.FieldStart("longitude")
		e.Float64(s.Longitude)
	}
}

var jsonFieldsNameOfPoint = [2]string{
	0: "latitude",
	1: "longitude",
}

// Decode decodes Point from json.
func (s *Point) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode Point to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "latitude":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := d.Float64()
				s.Latitude = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"latitude\"")
			}
		case "longitude":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Float64()
				s.Longitude = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"longitude\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode Point")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000011,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfPoint) {
					name = jsonFieldsNameOfPoint[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *Point) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *Point) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *ProductForResponse) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *ProductForResponse) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("id")
		json.EncodeUUID(e, s.ID)
	}
	{

		e.FieldStart("name")
		e.Str(s.Name)
	}
	{

		e.FieldStart("quantity")
		e.Int(s.Quantity)
	}
	{

		e.FieldStart("price")
		e.Float64(s.Price)
	}
	{

		e.FieldStart("total_price")
		e.Float64(s.TotalPrice)
	}
	{
		if s.VatCode.Set {
			e.FieldStart("vat_code")
			s.VatCode.Encode(e)
		}
	}
	{
		if s.ExternalID.Set {
			e.FieldStart("external_id")
			s.ExternalID.Encode(e)
		}
	}
	{
		if s.RkeeperCode.Set {
			e.FieldStart("rkeeper_code")
			s.RkeeperCode.Encode(e)
		}
	}
	{

		e.FieldStart("refund_quantity")
		e.Int(s.RefundQuantity)
	}
	{
		if s.Ingredients != nil {
			e.FieldStart("ingredients")
			e.ArrStart()
			for _, elem := range s.Ingredients {
				elem.Encode(e)
			}
			e.ArrEnd()
		}
	}
}

var jsonFieldsNameOfProductForResponse = [10]string{
	0: "id",
	1: "name",
	2: "quantity",
	3: "price",
	4: "total_price",
	5: "vat_code",
	6: "external_id",
	7: "rkeeper_code",
	8: "refund_quantity",
	9: "ingredients",
}

// Decode decodes ProductForResponse from json.
func (s *ProductForResponse) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode ProductForResponse to nil")
	}
	var requiredBitSet [2]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "id":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.ID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"id\"")
			}
		case "name":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Str()
				s.Name = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"name\"")
			}
		case "quantity":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Int()
				s.Quantity = int(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"quantity\"")
			}
		case "price":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Float64()
				s.Price = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"price\"")
			}
		case "total_price":
			requiredBitSet[0] |= 1 << 4
			if err := func() error {
				v, err := d.Float64()
				s.TotalPrice = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"total_price\"")
			}
		case "vat_code":
			if err := func() error {
				s.VatCode.Reset()
				if err := s.VatCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"vat_code\"")
			}
		case "external_id":
			if err := func() error {
				s.ExternalID.Reset()
				if err := s.ExternalID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"external_id\"")
			}
		case "rkeeper_code":
			if err := func() error {
				s.RkeeperCode.Reset()
				if err := s.RkeeperCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"rkeeper_code\"")
			}
		case "refund_quantity":
			requiredBitSet[1] |= 1 << 0
			if err := func() error {
				v, err := d.Int()
				s.RefundQuantity = int(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"refund_quantity\"")
			}
		case "ingredients":
			if err := func() error {
				s.Ingredients = make([]IngredientForResponse, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem IngredientForResponse
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Ingredients = append(s.Ingredients, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"ingredients\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode ProductForResponse")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [2]uint8{
		0b00011111,
		0b00000001,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfProductForResponse) {
					name = jsonFieldsNameOfProductForResponse[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *ProductForResponse) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *ProductForResponse) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PublicCourierPositionForResponse) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PublicCourierPositionForResponse) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("order_id")
		json.EncodeUUID(e, s.OrderID)
	}
	{

		e.FieldStart("courier_id")
		json.EncodeUUID(e, s.CourierID)
	}
	{

		e.FieldStart("order_status")
		e.Str(s.OrderStatus)
	}
	{

		e.FieldStart("courier_name")
		e.Str(s.CourierName)
	}
	{
		if s.PhoneNumber.Set {
			e.FieldStart("phone_number")
			s.PhoneNumber.Encode(e)
		}
	}
	{

		e.FieldStart("location")
		s.Location.Encode(e)
	}
	{
		if s.Passport.Set {
			e.FieldStart("passport")
			s.Passport.Encode(e)
		}
	}
	{
		if s.Vehicle.Set {
			e.FieldStart("vehicle")
			s.Vehicle.Encode(e)
		}
	}
}

var jsonFieldsNameOfPublicCourierPositionForResponse = [8]string{
	0: "order_id",
	1: "courier_id",
	2: "order_status",
	3: "courier_name",
	4: "phone_number",
	5: "location",
	6: "passport",
	7: "vehicle",
}

// Decode decodes PublicCourierPositionForResponse from json.
func (s *PublicCourierPositionForResponse) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PublicCourierPositionForResponse to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "order_id":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.OrderID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"order_id\"")
			}
		case "courier_id":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.CourierID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"courier_id\"")
			}
		case "order_status":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Str()
				s.OrderStatus = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"order_status\"")
			}
		case "courier_name":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Str()
				s.CourierName = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"courier_name\"")
			}
		case "phone_number":
			if err := func() error {
				s.PhoneNumber.Reset()
				if err := s.PhoneNumber.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"phone_number\"")
			}
		case "location":
			requiredBitSet[0] |= 1 << 5
			if err := func() error {
				if err := s.Location.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"location\"")
			}
		case "passport":
			if err := func() error {
				s.Passport.Reset()
				if err := s.Passport.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"passport\"")
			}
		case "vehicle":
			if err := func() error {
				s.Vehicle.Reset()
				if err := s.Vehicle.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"vehicle\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PublicCourierPositionForResponse")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00101111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfPublicCourierPositionForResponse) {
					name = jsonFieldsNameOfPublicCourierPositionForResponse[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PublicCourierPositionForResponse) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PublicCourierPositionForResponse) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PublicDeliveryForResponse) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PublicDeliveryForResponse) encodeFields(e *jx.Encoder) {
	{
		if s.ExpectedTime.Set {
			e.FieldStart("expected_time")
			s.ExpectedTime.Encode(e, json.EncodeDateTime)
		}
	}
	{
		if s.PaymentMethod.Set {
			e.FieldStart("payment_method")
			s.PaymentMethod.Encode(e)
		}
	}
	{
		if s.ConsumerData.Set {
			e.FieldStart("consumer_data")
			s.ConsumerData.Encode(e)
		}
	}
	{
		if s.Region.Set {
			e.FieldStart("region")
			s.Region.Encode(e)
		}
	}
	{
		if s.City.Set {
			e.FieldStart("city")
			s.City.Encode(e)
		}
	}
	{
		if s.Street.Set {
			e.FieldStart("street")
			s.Street.Encode(e)
		}
	}
	{
		if s.HouseNumber.Set {
			e.FieldStart("house_number")
			s.HouseNumber.Encode(e)
		}
	}
	{
		if s.FlatNumber.Set {
			e.FieldStart("flat_number")
			s.FlatNumber.Encode(e)
		}
	}
	{
		if s.Entrance.Set {
			e.FieldStart("entrance")
			s.Entrance.Encode(e)
		}
	}
	{
		if s.Intercom.Set {
			e.FieldStart("intercom")
			s.Intercom.Encode(e)
		}
	}
	{
		if s.Floor.Set {
			e.FieldStart("floor")
			s.Floor.Encode(e)
		}
	}
	{
		if s.Location.Set {
			e.FieldStart("location")
			s.Location.Encode(e)
		}
	}
	{
		if s.Comment.Set {
			e.FieldStart("comment")
			s.Comment.Encode(e)
		}
	}
	{
		if s.Attributes.Set {
			e.FieldStart("attributes")
			s.Attributes.Encode(e)
		}
	}
}

var jsonFieldsNameOfPublicDeliveryForResponse = [14]string{
	0:  "expected_time",
	1:  "payment_method",
	2:  "consumer_data",
	3:  "region",
	4:  "city",
	5:  "street",
	6:  "house_number",
	7:  "flat_number",
	8:  "entrance",
	9:  "intercom",
	10: "floor",
	11: "location",
	12: "comment",
	13: "attributes",
}

// Decode decodes PublicDeliveryForResponse from json.
func (s *PublicDeliveryForResponse) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PublicDeliveryForResponse to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "expected_time":
			if err := func() error {
				s.ExpectedTime.Reset()
				if err := s.ExpectedTime.Decode(d, json.DecodeDateTime); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"expected_time\"")
			}
		case "payment_method":
			if err := func() error {
				s.PaymentMethod.Reset()
				if err := s.PaymentMethod.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"payment_method\"")
			}
		case "consumer_data":
			if err := func() error {
				s.ConsumerData.Reset()
				if err := s.ConsumerData.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"consumer_data\"")
			}
		case "region":
			if err := func() error {
				s.Region.Reset()
				if err := s.Region.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"region\"")
			}
		case "city":
			if err := func() error {
				s.City.Reset()
				if err := s.City.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"city\"")
			}
		case "street":
			if err := func() error {
				s.Street.Reset()
				if err := s.Street.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"street\"")
			}
		case "house_number":
			if err := func() error {
				s.HouseNumber.Reset()
				if err := s.HouseNumber.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"house_number\"")
			}
		case "flat_number":
			if err := func() error {
				s.FlatNumber.Reset()
				if err := s.FlatNumber.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"flat_number\"")
			}
		case "entrance":
			if err := func() error {
				s.Entrance.Reset()
				if err := s.Entrance.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"entrance\"")
			}
		case "intercom":
			if err := func() error {
				s.Intercom.Reset()
				if err := s.Intercom.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"intercom\"")
			}
		case "floor":
			if err := func() error {
				s.Floor.Reset()
				if err := s.Floor.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"floor\"")
			}
		case "location":
			if err := func() error {
				s.Location.Reset()
				if err := s.Location.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"location\"")
			}
		case "comment":
			if err := func() error {
				s.Comment.Reset()
				if err := s.Comment.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"comment\"")
			}
		case "attributes":
			if err := func() error {
				s.Attributes.Reset()
				if err := s.Attributes.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"attributes\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PublicDeliveryForResponse")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PublicDeliveryForResponse) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PublicDeliveryForResponse) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetOKApplicationJSON as json.
func (s PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetOKApplicationJSON) Encode(e *jx.Encoder) {
	unwrapped := []OrderPhotoForResponse(s)

	e.ArrStart()
	for _, elem := range unwrapped {
		elem.Encode(e)
	}
	e.ArrEnd()
}

// Decode decodes PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetOKApplicationJSON from json.
func (s *PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetOKApplicationJSON) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetOKApplicationJSON to nil")
	}
	var unwrapped []OrderPhotoForResponse
	if err := func() error {
		unwrapped = make([]OrderPhotoForResponse, 0)
		if err := d.Arr(func(d *jx.Decoder) error {
			var elem OrderPhotoForResponse
			if err := elem.Decode(d); err != nil {
				return err
			}
			unwrapped = append(unwrapped, elem)
			return nil
		}); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return errors.Wrap(err, "alias")
	}
	*s = PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetOKApplicationJSON(unwrapped)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetOKApplicationJSON) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetOKApplicationJSON) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *PublicOrderDetailForResponse) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *PublicOrderDetailForResponse) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("id")
		json.EncodeUUID(e, s.ID)
	}
	{

		e.FieldStart("order_id")
		e.Str(s.OrderID)
	}
	{
		if s.RouteID.Set {
			e.FieldStart("route_id")
			s.RouteID.Encode(e)
		}
	}
	{

		e.FieldStart("merchant")
		s.Merchant.Encode(e)
	}
	{
		if s.PlaceID.Set {
			e.FieldStart("place_id")
			s.PlaceID.Encode(e)
		}
	}
	{

		e.FieldStart("status")
		s.Status.Encode(e)
	}
	{

		e.FieldStart("payment_status")
		s.PaymentStatus.Encode(e)
	}
	{

		e.FieldStart("cost")
		s.Cost.Encode(e)
	}
	{

		e.FieldStart("customer")
		s.Customer.Encode(e)
	}
	{

		e.FieldStart("person_count")
		e.Int(s.PersonCount)
	}
	{
		if s.Comment.Set {
			e.FieldStart("comment")
			s.Comment.Encode(e)
		}
	}
	{
		if s.CancelCode.Set {
			e.FieldStart("cancel_code")
			s.CancelCode.Encode(e)
		}
	}
	{
		if s.CancelReason.Set {
			e.FieldStart("cancel_reason")
			s.CancelReason.Encode(e)
		}
	}
	{

		e.FieldStart("is_force_start")
		e.Bool(s.IsForceStart)
	}
	{

		e.FieldStart("delivery")
		s.Delivery.Encode(e)
	}
	{

		e.FieldStart("products")
		e.ArrStart()
		for _, elem := range s.Products {
			elem.Encode(e)
		}
		e.ArrEnd()
	}
	{

		e.FieldStart("created_at")
		json.EncodeDateTime(e, s.CreatedAt)
	}
	{

		e.FieldStart("updated_at")
		json.EncodeDateTime(e, s.UpdatedAt)
	}
	{
		if s.AdditionalServices != nil {
			e.FieldStart("additional_services")
			e.ArrStart()
			for _, elem := range s.AdditionalServices {
				elem.Encode(e)
			}
			e.ArrEnd()
		}
	}
}

var jsonFieldsNameOfPublicOrderDetailForResponse = [19]string{
	0:  "id",
	1:  "order_id",
	2:  "route_id",
	3:  "merchant",
	4:  "place_id",
	5:  "status",
	6:  "payment_status",
	7:  "cost",
	8:  "customer",
	9:  "person_count",
	10: "comment",
	11: "cancel_code",
	12: "cancel_reason",
	13: "is_force_start",
	14: "delivery",
	15: "products",
	16: "created_at",
	17: "updated_at",
	18: "additional_services",
}

// Decode decodes PublicOrderDetailForResponse from json.
func (s *PublicOrderDetailForResponse) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PublicOrderDetailForResponse to nil")
	}
	var requiredBitSet [3]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "id":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeUUID(d)
				s.ID = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"id\"")
			}
		case "order_id":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Str()
				s.OrderID = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"order_id\"")
			}
		case "route_id":
			if err := func() error {
				s.RouteID.Reset()
				if err := s.RouteID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"route_id\"")
			}
		case "merchant":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				if err := s.Merchant.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"merchant\"")
			}
		case "place_id":
			if err := func() error {
				s.PlaceID.Reset()
				if err := s.PlaceID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"place_id\"")
			}
		case "status":
			requiredBitSet[0] |= 1 << 5
			if err := func() error {
				if err := s.Status.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"status\"")
			}
		case "payment_status":
			requiredBitSet[0] |= 1 << 6
			if err := func() error {
				if err := s.PaymentStatus.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"payment_status\"")
			}
		case "cost":
			requiredBitSet[0] |= 1 << 7
			if err := func() error {
				if err := s.Cost.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cost\"")
			}
		case "customer":
			requiredBitSet[1] |= 1 << 0
			if err := func() error {
				if err := s.Customer.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"customer\"")
			}
		case "person_count":
			requiredBitSet[1] |= 1 << 1
			if err := func() error {
				v, err := d.Int()
				s.PersonCount = int(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"person_count\"")
			}
		case "comment":
			if err := func() error {
				s.Comment.Reset()
				if err := s.Comment.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"comment\"")
			}
		case "cancel_code":
			if err := func() error {
				s.CancelCode.Reset()
				if err := s.CancelCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cancel_code\"")
			}
		case "cancel_reason":
			if err := func() error {
				s.CancelReason.Reset()
				if err := s.CancelReason.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cancel_reason\"")
			}
		case "is_force_start":
			requiredBitSet[1] |= 1 << 5
			if err := func() error {
				v, err := d.Bool()
				s.IsForceStart = bool(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"is_force_start\"")
			}
		case "delivery":
			requiredBitSet[1] |= 1 << 6
			if err := func() error {
				if err := s.Delivery.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"delivery\"")
			}
		case "products":
			requiredBitSet[1] |= 1 << 7
			if err := func() error {
				s.Products = make([]ProductForResponse, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem ProductForResponse
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Products = append(s.Products, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"products\"")
			}
		case "created_at":
			requiredBitSet[2] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeDateTime(d)
				s.CreatedAt = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"created_at\"")
			}
		case "updated_at":
			requiredBitSet[2] |= 1 << 1
			if err := func() error {
				v, err := json.DecodeDateTime(d)
				s.UpdatedAt = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"updated_at\"")
			}
		case "additional_services":
			if err := func() error {
				s.AdditionalServices = make([]AdditionalServiceInDB, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem AdditionalServiceInDB
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.AdditionalServices = append(s.AdditionalServices, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"additional_services\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode PublicOrderDetailForResponse")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [3]uint8{
		0b11101011,
		0b11100011,
		0b00000011,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfPublicOrderDetailForResponse) {
					name = jsonFieldsNameOfPublicOrderDetailForResponse[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *PublicOrderDetailForResponse) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PublicOrderDetailForResponse) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes PublicReturnOrderPublicV1OrdersOrderIDReturnPatchOKApplicationJSON as json.
func (s PublicReturnOrderPublicV1OrdersOrderIDReturnPatchOKApplicationJSON) Encode(e *jx.Encoder) {
	unwrapped := jx.Raw(s)

	if len(unwrapped) != 0 {
		e.Raw(unwrapped)
	}
}

// Decode decodes PublicReturnOrderPublicV1OrdersOrderIDReturnPatchOKApplicationJSON from json.
func (s *PublicReturnOrderPublicV1OrdersOrderIDReturnPatchOKApplicationJSON) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PublicReturnOrderPublicV1OrdersOrderIDReturnPatchOKApplicationJSON to nil")
	}
	var unwrapped jx.Raw
	if err := func() error {
		v, err := d.RawAppend(nil)
		unwrapped = jx.Raw(v)
		if err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return errors.Wrap(err, "alias")
	}
	*s = PublicReturnOrderPublicV1OrdersOrderIDReturnPatchOKApplicationJSON(unwrapped)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s PublicReturnOrderPublicV1OrdersOrderIDReturnPatchOKApplicationJSON) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PublicReturnOrderPublicV1OrdersOrderIDReturnPatchOKApplicationJSON) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode encodes PublicReturnOrderPublicV2OrdersOrderIDReturnPatchOKApplicationJSON as json.
func (s PublicReturnOrderPublicV2OrdersOrderIDReturnPatchOKApplicationJSON) Encode(e *jx.Encoder) {
	unwrapped := jx.Raw(s)

	if len(unwrapped) != 0 {
		e.Raw(unwrapped)
	}
}

// Decode decodes PublicReturnOrderPublicV2OrdersOrderIDReturnPatchOKApplicationJSON from json.
func (s *PublicReturnOrderPublicV2OrdersOrderIDReturnPatchOKApplicationJSON) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode PublicReturnOrderPublicV2OrdersOrderIDReturnPatchOKApplicationJSON to nil")
	}
	var unwrapped jx.Raw
	if err := func() error {
		v, err := d.RawAppend(nil)
		unwrapped = jx.Raw(v)
		if err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return errors.Wrap(err, "alias")
	}
	*s = PublicReturnOrderPublicV2OrdersOrderIDReturnPatchOKApplicationJSON(unwrapped)
	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s PublicReturnOrderPublicV2OrdersOrderIDReturnPatchOKApplicationJSON) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *PublicReturnOrderPublicV2OrdersOrderIDReturnPatchOKApplicationJSON) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *RegularSchedule) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *RegularSchedule) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("week_day")
		e.Int(s.WeekDay)
	}
	{

		e.FieldStart("time_start")
		json.EncodeTime(e, s.TimeStart)
	}
	{

		e.FieldStart("time_end")
		json.EncodeTime(e, s.TimeEnd)
	}
	{

		e.FieldStart("is_work")
		e.Bool(s.IsWork)
	}
}

var jsonFieldsNameOfRegularSchedule = [4]string{
	0: "week_day",
	1: "time_start",
	2: "time_end",
	3: "is_work",
}

// Decode decodes RegularSchedule from json.
func (s *RegularSchedule) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode RegularSchedule to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "week_day":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := d.Int()
				s.WeekDay = int(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"week_day\"")
			}
		case "time_start":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := json.DecodeTime(d)
				s.TimeStart = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"time_start\"")
			}
		case "time_end":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := json.DecodeTime(d)
				s.TimeEnd = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"time_end\"")
			}
		case "is_work":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Bool()
				s.IsWork = bool(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"is_work\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode RegularSchedule")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00001111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfRegularSchedule) {
					name = jsonFieldsNameOfRegularSchedule[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *RegularSchedule) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *RegularSchedule) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *ReturnProductsInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *ReturnProductsInRequest) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("products")
		e.ArrStart()
		for _, elem := range s.Products {
			e.Str(elem)
		}
		e.ArrEnd()
	}
	{

		e.FieldStart("return_amount")
		e.Float64(s.ReturnAmount)
	}
	{

		e.FieldStart("new_total_amount")
		e.Float64(s.NewTotalAmount)
	}
	{

		e.FieldStart("reason")
		e.Str(s.Reason)
	}
}

var jsonFieldsNameOfReturnProductsInRequest = [4]string{
	0: "products",
	1: "return_amount",
	2: "new_total_amount",
	3: "reason",
}

// Decode decodes ReturnProductsInRequest from json.
func (s *ReturnProductsInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode ReturnProductsInRequest to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "products":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				s.Products = make([]string, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem string
					v, err := d.Str()
					elem = string(v)
					if err != nil {
						return err
					}
					s.Products = append(s.Products, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"products\"")
			}
		case "return_amount":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Float64()
				s.ReturnAmount = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"return_amount\"")
			}
		case "new_total_amount":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Float64()
				s.NewTotalAmount = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"new_total_amount\"")
			}
		case "reason":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Str()
				s.Reason = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"reason\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode ReturnProductsInRequest")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00001111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfReturnProductsInRequest) {
					name = jsonFieldsNameOfReturnProductsInRequest[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *ReturnProductsInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *ReturnProductsInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *ReturnProductsInRequestV2) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *ReturnProductsInRequestV2) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("products")
		e.ArrStart()
		for _, elem := range s.Products {
			elem.Encode(e)
		}
		e.ArrEnd()
	}
	{

		e.FieldStart("return_amount")
		e.Float64(s.ReturnAmount)
	}
	{

		e.FieldStart("new_total_amount")
		e.Float64(s.NewTotalAmount)
	}
	{

		e.FieldStart("reason")
		e.Str(s.Reason)
	}
	{
		if s.CancelCode.Set {
			e.FieldStart("cancel_code")
			s.CancelCode.Encode(e)
		}
	}
}

var jsonFieldsNameOfReturnProductsInRequestV2 = [5]string{
	0: "products",
	1: "return_amount",
	2: "new_total_amount",
	3: "reason",
	4: "cancel_code",
}

// Decode decodes ReturnProductsInRequestV2 from json.
func (s *ReturnProductsInRequestV2) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode ReturnProductsInRequestV2 to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "products":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				s.Products = make([]ReturnProductsInRequestV2Products, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem ReturnProductsInRequestV2Products
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Products = append(s.Products, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"products\"")
			}
		case "return_amount":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Float64()
				s.ReturnAmount = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"return_amount\"")
			}
		case "new_total_amount":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Float64()
				s.NewTotalAmount = float64(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"new_total_amount\"")
			}
		case "reason":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Str()
				s.Reason = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"reason\"")
			}
		case "cancel_code":
			if err := func() error {
				s.CancelCode.Reset()
				if err := s.CancelCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cancel_code\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode ReturnProductsInRequestV2")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00001111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfReturnProductsInRequestV2) {
					name = jsonFieldsNameOfReturnProductsInRequestV2[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *ReturnProductsInRequestV2) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *ReturnProductsInRequestV2) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *ReturnProductsInRequestV2Products) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *ReturnProductsInRequestV2Products) encodeFields(e *jx.Encoder) {
	{
		if s.ID.Set {
			e.FieldStart("id")
			s.ID.Encode(e)
		}
	}
	{
		if s.Price.Set {
			e.FieldStart("price")
			s.Price.Encode(e)
		}
	}
	{
		if s.RefundAmount.Set {
			e.FieldStart("refund_amount")
			s.RefundAmount.Encode(e)
		}
	}
	{
		if s.RefundQuantity.Set {
			e.FieldStart("refund_quantity")
			s.RefundQuantity.Encode(e)
		}
	}
}

var jsonFieldsNameOfReturnProductsInRequestV2Products = [4]string{
	0: "id",
	1: "price",
	2: "refund_amount",
	3: "refund_quantity",
}

// Decode decodes ReturnProductsInRequestV2Products from json.
func (s *ReturnProductsInRequestV2Products) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode ReturnProductsInRequestV2Products to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "id":
			if err := func() error {
				s.ID.Reset()
				if err := s.ID.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"id\"")
			}
		case "price":
			if err := func() error {
				s.Price.Reset()
				if err := s.Price.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"price\"")
			}
		case "refund_amount":
			if err := func() error {
				s.RefundAmount.Reset()
				if err := s.RefundAmount.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"refund_amount\"")
			}
		case "refund_quantity":
			if err := func() error {
				s.RefundQuantity.Reset()
				if err := s.RefundQuantity.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"refund_quantity\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode ReturnProductsInRequestV2Products")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *ReturnProductsInRequestV2Products) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *ReturnProductsInRequestV2Products) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *SpecialSchedule) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *SpecialSchedule) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("day")
		json.EncodeDate(e, s.Day)
	}
	{

		e.FieldStart("time_start")
		json.EncodeTime(e, s.TimeStart)
	}
	{

		e.FieldStart("time_end")
		json.EncodeTime(e, s.TimeEnd)
	}
	{

		e.FieldStart("is_work")
		e.Bool(s.IsWork)
	}
}

var jsonFieldsNameOfSpecialSchedule = [4]string{
	0: "day",
	1: "time_start",
	2: "time_end",
	3: "is_work",
}

// Decode decodes SpecialSchedule from json.
func (s *SpecialSchedule) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode SpecialSchedule to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "day":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := json.DecodeDate(d)
				s.Day = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"day\"")
			}
		case "time_start":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := json.DecodeTime(d)
				s.TimeStart = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"time_start\"")
			}
		case "time_end":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := json.DecodeTime(d)
				s.TimeEnd = v
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"time_end\"")
			}
		case "is_work":
			requiredBitSet[0] |= 1 << 3
			if err := func() error {
				v, err := d.Bool()
				s.IsWork = bool(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"is_work\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode SpecialSchedule")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00001111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfSpecialSchedule) {
					name = jsonFieldsNameOfSpecialSchedule[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *SpecialSchedule) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *SpecialSchedule) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *UpdateDeliveryInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *UpdateDeliveryInRequest) encodeFields(e *jx.Encoder) {
	{
		if s.ExpectedTime.Set {
			e.FieldStart("expected_time")
			s.ExpectedTime.Encode(e, json.EncodeDateTime)
		}
	}
	{
		if s.PaymentMethod.Set {
			e.FieldStart("payment_method")
			s.PaymentMethod.Encode(e)
		}
	}
	{
		if s.ConsumerData.Set {
			e.FieldStart("consumer_data")
			s.ConsumerData.Encode(e)
		}
	}
	{
		if s.Region.Set {
			e.FieldStart("region")
			s.Region.Encode(e)
		}
	}
	{
		if s.Street.Set {
			e.FieldStart("street")
			s.Street.Encode(e)
		}
	}
	{
		if s.HouseNumber.Set {
			e.FieldStart("house_number")
			s.HouseNumber.Encode(e)
		}
	}
	{
		if s.FlatNumber.Set {
			e.FieldStart("flat_number")
			s.FlatNumber.Encode(e)
		}
	}
	{
		if s.Entrance.Set {
			e.FieldStart("entrance")
			s.Entrance.Encode(e)
		}
	}
	{
		if s.Intercom.Set {
			e.FieldStart("intercom")
			s.Intercom.Encode(e)
		}
	}
	{
		if s.Floor.Set {
			e.FieldStart("floor")
			s.Floor.Encode(e)
		}
	}
	{
		if s.Location.Set {
			e.FieldStart("location")
			s.Location.Encode(e)
		}
	}
	{
		if s.Comment.Set {
			e.FieldStart("comment")
			s.Comment.Encode(e)
		}
	}
	{
		if s.Attributes.Set {
			e.FieldStart("attributes")
			s.Attributes.Encode(e)
		}
	}
}

var jsonFieldsNameOfUpdateDeliveryInRequest = [13]string{
	0:  "expected_time",
	1:  "payment_method",
	2:  "consumer_data",
	3:  "region",
	4:  "street",
	5:  "house_number",
	6:  "flat_number",
	7:  "entrance",
	8:  "intercom",
	9:  "floor",
	10: "location",
	11: "comment",
	12: "attributes",
}

// Decode decodes UpdateDeliveryInRequest from json.
func (s *UpdateDeliveryInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode UpdateDeliveryInRequest to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "expected_time":
			if err := func() error {
				s.ExpectedTime.Reset()
				if err := s.ExpectedTime.Decode(d, json.DecodeDateTime); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"expected_time\"")
			}
		case "payment_method":
			if err := func() error {
				s.PaymentMethod.Reset()
				if err := s.PaymentMethod.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"payment_method\"")
			}
		case "consumer_data":
			if err := func() error {
				s.ConsumerData.Reset()
				if err := s.ConsumerData.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"consumer_data\"")
			}
		case "region":
			if err := func() error {
				s.Region.Reset()
				if err := s.Region.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"region\"")
			}
		case "street":
			if err := func() error {
				s.Street.Reset()
				if err := s.Street.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"street\"")
			}
		case "house_number":
			if err := func() error {
				s.HouseNumber.Reset()
				if err := s.HouseNumber.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"house_number\"")
			}
		case "flat_number":
			if err := func() error {
				s.FlatNumber.Reset()
				if err := s.FlatNumber.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"flat_number\"")
			}
		case "entrance":
			if err := func() error {
				s.Entrance.Reset()
				if err := s.Entrance.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"entrance\"")
			}
		case "intercom":
			if err := func() error {
				s.Intercom.Reset()
				if err := s.Intercom.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"intercom\"")
			}
		case "floor":
			if err := func() error {
				s.Floor.Reset()
				if err := s.Floor.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"floor\"")
			}
		case "location":
			if err := func() error {
				s.Location.Reset()
				if err := s.Location.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"location\"")
			}
		case "comment":
			if err := func() error {
				s.Comment.Reset()
				if err := s.Comment.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"comment\"")
			}
		case "attributes":
			if err := func() error {
				s.Attributes.Reset()
				if err := s.Attributes.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"attributes\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode UpdateDeliveryInRequest")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *UpdateDeliveryInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *UpdateDeliveryInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *UpdateOrderInRequest) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *UpdateOrderInRequest) encodeFields(e *jx.Encoder) {
	{
		if s.Status.Set {
			e.FieldStart("status")
			s.Status.Encode(e)
		}
	}
	{
		if s.PaymentStatus.Set {
			e.FieldStart("payment_status")
			s.PaymentStatus.Encode(e)
		}
	}
	{
		if s.Cost.Set {
			e.FieldStart("cost")
			s.Cost.Encode(e)
		}
	}
	{
		if s.AdditionalCost.Set {
			e.FieldStart("additional_cost")
			s.AdditionalCost.Encode(e)
		}
	}
	{
		if s.ChangeCost.Set {
			e.FieldStart("change_cost")
			s.ChangeCost.Encode(e)
		}
	}
	{
		if s.CustomerName.Set {
			e.FieldStart("customer_name")
			s.CustomerName.Encode(e)
		}
	}
	{
		if s.CustomerPhone.Set {
			e.FieldStart("customer_phone")
			s.CustomerPhone.Encode(e)
		}
	}
	{
		if s.CustomerEmail.Set {
			e.FieldStart("customer_email")
			s.CustomerEmail.Encode(e)
		}
	}
	{
		if s.PersonCount.Set {
			e.FieldStart("person_count")
			s.PersonCount.Encode(e)
		}
	}
	{
		if s.Comment.Set {
			e.FieldStart("comment")
			s.Comment.Encode(e)
		}
	}
	{
		if s.CancelCode.Set {
			e.FieldStart("cancel_code")
			s.CancelCode.Encode(e)
		}
	}
	{
		if s.CancelReason.Set {
			e.FieldStart("cancel_reason")
			s.CancelReason.Encode(e)
		}
	}
	{
		if s.Delivery.Set {
			e.FieldStart("delivery")
			s.Delivery.Encode(e)
		}
	}
	{
		if s.Products != nil {
			e.FieldStart("products")
			e.ArrStart()
			for _, elem := range s.Products {
				elem.Encode(e)
			}
			e.ArrEnd()
		}
	}
}

var jsonFieldsNameOfUpdateOrderInRequest = [14]string{
	0:  "status",
	1:  "payment_status",
	2:  "cost",
	3:  "additional_cost",
	4:  "change_cost",
	5:  "customer_name",
	6:  "customer_phone",
	7:  "customer_email",
	8:  "person_count",
	9:  "comment",
	10: "cancel_code",
	11: "cancel_reason",
	12: "delivery",
	13: "products",
}

// Decode decodes UpdateOrderInRequest from json.
func (s *UpdateOrderInRequest) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode UpdateOrderInRequest to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "status":
			if err := func() error {
				s.Status.Reset()
				if err := s.Status.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"status\"")
			}
		case "payment_status":
			if err := func() error {
				s.PaymentStatus.Reset()
				if err := s.PaymentStatus.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"payment_status\"")
			}
		case "cost":
			if err := func() error {
				s.Cost.Reset()
				if err := s.Cost.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cost\"")
			}
		case "additional_cost":
			if err := func() error {
				s.AdditionalCost.Reset()
				if err := s.AdditionalCost.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"additional_cost\"")
			}
		case "change_cost":
			if err := func() error {
				s.ChangeCost.Reset()
				if err := s.ChangeCost.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"change_cost\"")
			}
		case "customer_name":
			if err := func() error {
				s.CustomerName.Reset()
				if err := s.CustomerName.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"customer_name\"")
			}
		case "customer_phone":
			if err := func() error {
				s.CustomerPhone.Reset()
				if err := s.CustomerPhone.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"customer_phone\"")
			}
		case "customer_email":
			if err := func() error {
				s.CustomerEmail.Reset()
				if err := s.CustomerEmail.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"customer_email\"")
			}
		case "person_count":
			if err := func() error {
				s.PersonCount.Reset()
				if err := s.PersonCount.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"person_count\"")
			}
		case "comment":
			if err := func() error {
				s.Comment.Reset()
				if err := s.Comment.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"comment\"")
			}
		case "cancel_code":
			if err := func() error {
				s.CancelCode.Reset()
				if err := s.CancelCode.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cancel_code\"")
			}
		case "cancel_reason":
			if err := func() error {
				s.CancelReason.Reset()
				if err := s.CancelReason.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"cancel_reason\"")
			}
		case "delivery":
			if err := func() error {
				s.Delivery.Reset()
				if err := s.Delivery.Decode(d); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"delivery\"")
			}
		case "products":
			if err := func() error {
				s.Products = make([]CreateProductInRequest, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem CreateProductInRequest
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Products = append(s.Products, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"products\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode UpdateOrderInRequest")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *UpdateOrderInRequest) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *UpdateOrderInRequest) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *ValidationError) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *ValidationError) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("loc")
		e.ArrStart()
		for _, elem := range s.Loc {
			e.Str(elem)
		}
		e.ArrEnd()
	}
	{

		e.FieldStart("msg")
		e.Str(s.Msg)
	}
	{

		e.FieldStart("type")
		e.Str(s.Type)
	}
}

var jsonFieldsNameOfValidationError = [3]string{
	0: "loc",
	1: "msg",
	2: "type",
}

// Decode decodes ValidationError from json.
func (s *ValidationError) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode ValidationError to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "loc":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				s.Loc = make([]string, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem string
					v, err := d.Str()
					elem = string(v)
					if err != nil {
						return err
					}
					s.Loc = append(s.Loc, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"loc\"")
			}
		case "msg":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Str()
				s.Msg = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"msg\"")
			}
		case "type":
			requiredBitSet[0] |= 1 << 2
			if err := func() error {
				v, err := d.Str()
				s.Type = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"type\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode ValidationError")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000111,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfValidationError) {
					name = jsonFieldsNameOfValidationError[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *ValidationError) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *ValidationError) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *Vehicle) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *Vehicle) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("license_plate")
		e.Str(s.LicensePlate)
	}
	{

		e.FieldStart("model")
		e.Str(s.Model)
	}
}

var jsonFieldsNameOfVehicle = [2]string{
	0: "license_plate",
	1: "model",
}

// Decode decodes Vehicle from json.
func (s *Vehicle) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode Vehicle to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "license_plate":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				v, err := d.Str()
				s.LicensePlate = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"license_plate\"")
			}
		case "model":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				v, err := d.Str()
				s.Model = string(v)
				if err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"model\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode Vehicle")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000011,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfVehicle) {
					name = jsonFieldsNameOfVehicle[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *Vehicle) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *Vehicle) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *WorkTimes) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *WorkTimes) encodeFields(e *jx.Encoder) {
	{

		e.FieldStart("regular")
		e.ArrStart()
		for _, elem := range s.Regular {
			elem.Encode(e)
		}
		e.ArrEnd()
	}
	{

		e.FieldStart("special")
		e.ArrStart()
		for _, elem := range s.Special {
			elem.Encode(e)
		}
		e.ArrEnd()
	}
}

var jsonFieldsNameOfWorkTimes = [2]string{
	0: "regular",
	1: "special",
}

// Decode decodes WorkTimes from json.
func (s *WorkTimes) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode WorkTimes to nil")
	}
	var requiredBitSet [1]uint8

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "regular":
			requiredBitSet[0] |= 1 << 0
			if err := func() error {
				s.Regular = make([]RegularSchedule, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem RegularSchedule
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Regular = append(s.Regular, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"regular\"")
			}
		case "special":
			requiredBitSet[0] |= 1 << 1
			if err := func() error {
				s.Special = make([]SpecialSchedule, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem SpecialSchedule
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Special = append(s.Special, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"special\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode WorkTimes")
	}
	// Validate required fields.
	var failures []validate.FieldError
	for i, mask := range [1]uint8{
		0b00000011,
	} {
		if result := (requiredBitSet[i] & mask) ^ mask; result != 0 {
			// Mask only required fields and check equality to mask using XOR.
			//
			// If XOR result is not zero, result is not equal to expected, so some fields are missed.
			// Bits of fields which would be set are actually bits of missed fields.
			missed := bits.OnesCount8(result)
			for bitN := 0; bitN < missed; bitN++ {
				bitIdx := bits.TrailingZeros8(result)
				fieldIdx := i*8 + bitIdx
				var name string
				if fieldIdx < len(jsonFieldsNameOfWorkTimes) {
					name = jsonFieldsNameOfWorkTimes[fieldIdx]
				} else {
					name = strconv.Itoa(fieldIdx)
				}
				failures = append(failures, validate.FieldError{
					Name:  name,
					Error: validate.ErrFieldRequired,
				})
				// Reset bit.
				result &^= 1 << bitIdx
			}
		}
	}
	if len(failures) > 0 {
		return &validate.Error{Fields: failures}
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *WorkTimes) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *WorkTimes) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}

// Encode implements json.Marshaler.
func (s *WorkTimesUpdate) Encode(e *jx.Encoder) {
	e.ObjStart()
	s.encodeFields(e)
	e.ObjEnd()
}

// encodeFields encodes fields.
func (s *WorkTimesUpdate) encodeFields(e *jx.Encoder) {
	{
		if s.Regular != nil {
			e.FieldStart("regular")
			e.ArrStart()
			for _, elem := range s.Regular {
				elem.Encode(e)
			}
			e.ArrEnd()
		}
	}
	{
		if s.Special != nil {
			e.FieldStart("special")
			e.ArrStart()
			for _, elem := range s.Special {
				elem.Encode(e)
			}
			e.ArrEnd()
		}
	}
}

var jsonFieldsNameOfWorkTimesUpdate = [2]string{
	0: "regular",
	1: "special",
}

// Decode decodes WorkTimesUpdate from json.
func (s *WorkTimesUpdate) Decode(d *jx.Decoder) error {
	if s == nil {
		return errors.New("invalid: unable to decode WorkTimesUpdate to nil")
	}

	if err := d.ObjBytes(func(d *jx.Decoder, k []byte) error {
		switch string(k) {
		case "regular":
			if err := func() error {
				s.Regular = make([]RegularSchedule, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem RegularSchedule
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Regular = append(s.Regular, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"regular\"")
			}
		case "special":
			if err := func() error {
				s.Special = make([]SpecialSchedule, 0)
				if err := d.Arr(func(d *jx.Decoder) error {
					var elem SpecialSchedule
					if err := elem.Decode(d); err != nil {
						return err
					}
					s.Special = append(s.Special, elem)
					return nil
				}); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return errors.Wrap(err, "decode field \"special\"")
			}
		default:
			return d.Skip()
		}
		return nil
	}); err != nil {
		return errors.Wrap(err, "decode WorkTimesUpdate")
	}

	return nil
}

// MarshalJSON implements stdjson.Marshaler.
func (s *WorkTimesUpdate) MarshalJSON() ([]byte, error) {
	e := jx.Encoder{}
	s.Encode(&e)
	return e.Bytes(), nil
}

// UnmarshalJSON implements stdjson.Unmarshaler.
func (s *WorkTimesUpdate) UnmarshalJSON(data []byte) error {
	d := jx.DecodeBytes(data)
	return s.Decode(d)
}
