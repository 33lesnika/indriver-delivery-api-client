package api

import (
	"github.com/ogen-go/ogen/middleware"
)

// Middleware is middleware type.
type Middleware = middleware.Middleware
