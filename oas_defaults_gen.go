package api

// setDefaults set default value of fields.
func (s *CreateDeliveryInRequest) setDefaults() {
	{
		val := string("")
		s.HouseNumber.SetTo(val)
	}
}

// setDefaults set default value of fields.
func (s *CreateOrderInRequest) setDefaults() {
	{
		val := bool(false)
		s.IsForceStart.SetTo(val)
	}
}

// setDefaults set default value of fields.
func (s *DeliveryDryRunInRequest) setDefaults() {
	{
		val := string("")
		s.Region.SetTo(val)
	}
	{
		val := string("")
		s.HouseNumber.SetTo(val)
	}
}

// setDefaults set default value of fields.
func (s *OrderDryRun) setDefaults() {
	{
		val := bool(false)
		s.IsForceStart.SetTo(val)
	}
}

// setDefaults set default value of fields.
func (s *PlaceInDB) setDefaults() {
	{
		val := bool(false)
		s.IsBlocked.SetTo(val)
	}
}
