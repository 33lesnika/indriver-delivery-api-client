package api

import (
	"context"
	"net/http"
	"time"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	semconv "go.opentelemetry.io/otel/semconv/v1.17.0"
	"go.opentelemetry.io/otel/trace"

	"github.com/ogen-go/ogen/middleware"
	"github.com/ogen-go/ogen/ogenerrors"
	"github.com/ogen-go/ogen/otelogen"
)

// handleMerchantCourierNearDeliveryAddressCallbackURLOrderIDPostRequest handles POST /merchant_courier_near_delivery_address_callback_url/{order_id} operation.
//
// Notifies that the courier is located near the delivery address.
//
// POST /merchant_courier_near_delivery_address_callback_url/{order_id}
func (s *Server) handleMerchantCourierNearDeliveryAddressCallbackURLOrderIDPostRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	var otelAttrs []attribute.KeyValue

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost",
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost",
			ID:   "",
		}
	)
	params, err := decodeMerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	var response *MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostOK
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost",
			OperationID:   "",
			Body:          nil,
			Params: middleware.Parameters{
				{
					Name: "order_id",
					In:   "path",
				}: params.OrderID,
			},
			Raw: r,
		}

		type (
			Request  = struct{}
			Params   = MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams
			Response = *MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostOK
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackMerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				err = s.h.MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost(ctx, params)
				return response, err
			},
		)
	} else {
		err = s.h.MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost(ctx, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodeMerchantCourierNearDeliveryAddressCallbackURLOrderIDPostResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handleMerchantCourierNearPlaceCallbackURLOrderIDPostRequest handles POST /merchant_courier_near_place_callback_url/{order_id} operation.
//
// Notifies that the courier is located near the place.
//
// POST /merchant_courier_near_place_callback_url/{order_id}
func (s *Server) handleMerchantCourierNearPlaceCallbackURLOrderIDPostRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	var otelAttrs []attribute.KeyValue

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "MerchantCourierNearPlaceCallbackURLOrderIDPost",
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "MerchantCourierNearPlaceCallbackURLOrderIDPost",
			ID:   "",
		}
	)
	params, err := decodeMerchantCourierNearPlaceCallbackURLOrderIDPostParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	var response *MerchantCourierNearPlaceCallbackURLOrderIDPostOK
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "MerchantCourierNearPlaceCallbackURLOrderIDPost",
			OperationID:   "",
			Body:          nil,
			Params: middleware.Parameters{
				{
					Name: "order_id",
					In:   "path",
				}: params.OrderID,
			},
			Raw: r,
		}

		type (
			Request  = struct{}
			Params   = MerchantCourierNearPlaceCallbackURLOrderIDPostParams
			Response = *MerchantCourierNearPlaceCallbackURLOrderIDPostOK
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackMerchantCourierNearPlaceCallbackURLOrderIDPostParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				err = s.h.MerchantCourierNearPlaceCallbackURLOrderIDPost(ctx, params)
				return response, err
			},
		)
	} else {
		err = s.h.MerchantCourierNearPlaceCallbackURLOrderIDPost(ctx, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodeMerchantCourierNearPlaceCallbackURLOrderIDPostResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handleMerchantOrderStatusCallbackURLOrderIDPostRequest handles POST /merchant_order_status_callback_url/{order_id} operation.
//
// Notifies about order's status update or about delivery delaying.
//
// POST /merchant_order_status_callback_url/{order_id}
func (s *Server) handleMerchantOrderStatusCallbackURLOrderIDPostRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	var otelAttrs []attribute.KeyValue

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "MerchantOrderStatusCallbackURLOrderIDPost",
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "MerchantOrderStatusCallbackURLOrderIDPost",
			ID:   "",
		}
	)
	params, err := decodeMerchantOrderStatusCallbackURLOrderIDPostParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	request, close, err := s.decodeMerchantOrderStatusCallbackURLOrderIDPostRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response *MerchantOrderStatusCallbackURLOrderIDPostOK
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "MerchantOrderStatusCallbackURLOrderIDPost",
			OperationID:   "",
			Body:          request,
			Params: middleware.Parameters{
				{
					Name: "order_id",
					In:   "path",
				}: params.OrderID,
			},
			Raw: r,
		}

		type (
			Request  = OptOrderStatusCallback
			Params   = MerchantOrderStatusCallbackURLOrderIDPostParams
			Response = *MerchantOrderStatusCallbackURLOrderIDPostOK
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackMerchantOrderStatusCallbackURLOrderIDPostParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				err = s.h.MerchantOrderStatusCallbackURLOrderIDPost(ctx, request, params)
				return response, err
			},
		)
	} else {
		err = s.h.MerchantOrderStatusCallbackURLOrderIDPost(ctx, request, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodeMerchantOrderStatusCallbackURLOrderIDPostResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handleMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRequest handles merchant_schedule_get_public_v1_merchant__merchant_id__schedule_get operation.
//
// Merchant Schedule:Get.
//
// GET /merchant/{merchant_id}/schedule
func (s *Server) handleMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("merchant_schedule_get_public_v1_merchant__merchant_id__schedule_get"),
		semconv.HTTPMethodKey.String("GET"),
		semconv.HTTPRouteKey.String("/merchant/{merchant_id}/schedule"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet",
			ID:   "merchant_schedule_get_public_v1_merchant__merchant_id__schedule_get",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	params, err := decodeMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	var response MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet",
			OperationID:   "merchant_schedule_get_public_v1_merchant__merchant_id__schedule_get",
			Body:          nil,
			Params: middleware.Parameters{
				{
					Name: "merchant_id",
					In:   "path",
				}: params.MerchantID,
			},
			Raw: r,
		}

		type (
			Request  = struct{}
			Params   = MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams
			Response = MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet(ctx, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet(ctx, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodeMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handleMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRequest handles merchant_schedule_update_public_v1_merchant__merchant_id__schedule_put operation.
//
// Merchant Schedule:Update.
//
// PUT /merchant/{merchant_id}/schedule
func (s *Server) handleMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("merchant_schedule_update_public_v1_merchant__merchant_id__schedule_put"),
		semconv.HTTPMethodKey.String("PUT"),
		semconv.HTTPRouteKey.String("/merchant/{merchant_id}/schedule"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut",
			ID:   "merchant_schedule_update_public_v1_merchant__merchant_id__schedule_put",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	params, err := decodeMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	request, close, err := s.decodeMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut",
			OperationID:   "merchant_schedule_update_public_v1_merchant__merchant_id__schedule_put",
			Body:          request,
			Params: middleware.Parameters{
				{
					Name: "merchant_id",
					In:   "path",
				}: params.MerchantID,
			},
			Raw: r,
		}

		type (
			Request  = *WorkTimesUpdate
			Params   = MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams
			Response = MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut(ctx, request, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut(ctx, request, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodeMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRequest handles place_schedule_get_public_v1_merchant_places__place_id__schedule_get operation.
//
// Place Schedule:Get.
//
// GET /merchant/places/{place_id}/schedule
func (s *Server) handlePlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("place_schedule_get_public_v1_merchant_places__place_id__schedule_get"),
		semconv.HTTPMethodKey.String("GET"),
		semconv.HTTPRouteKey.String("/merchant/places/{place_id}/schedule"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet",
			ID:   "place_schedule_get_public_v1_merchant_places__place_id__schedule_get",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	params, err := decodePlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	var response PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet",
			OperationID:   "place_schedule_get_public_v1_merchant_places__place_id__schedule_get",
			Body:          nil,
			Params: middleware.Parameters{
				{
					Name: "place_id",
					In:   "path",
				}: params.PlaceID,
			},
			Raw: r,
		}

		type (
			Request  = struct{}
			Params   = PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams
			Response = PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackPlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet(ctx, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet(ctx, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRequest handles place_schedule_update_public_v1_merchant_places__place_id__schedule_put operation.
//
// Place Schedule:Update.
//
// PUT /merchant/places/{place_id}/schedule
func (s *Server) handlePlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("place_schedule_update_public_v1_merchant_places__place_id__schedule_put"),
		semconv.HTTPMethodKey.String("PUT"),
		semconv.HTTPRouteKey.String("/merchant/places/{place_id}/schedule"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut",
			ID:   "place_schedule_update_public_v1_merchant_places__place_id__schedule_put",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	params, err := decodePlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	request, close, err := s.decodePlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut",
			OperationID:   "place_schedule_update_public_v1_merchant_places__place_id__schedule_put",
			Body:          request,
			Params: middleware.Parameters{
				{
					Name: "place_id",
					In:   "path",
				}: params.PlaceID,
			},
			Raw: r,
		}

		type (
			Request  = *WorkTimesUpdate
			Params   = PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams
			Response = PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackPlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut(ctx, request, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut(ctx, request, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePlacesBlockPublicV1MerchantPlacesBlockPostRequest handles places_block_public_v1_merchant_places_block__post operation.
//
// Blocks places for the specified period or endless. Use if you want to temporally stop orders
// creating for one or list of places.
//
// POST /merchant/places/block/
func (s *Server) handlePlacesBlockPublicV1MerchantPlacesBlockPostRequest(args [0]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_block_public_v1_merchant_places_block__post"),
		semconv.HTTPMethodKey.String("POST"),
		semconv.HTTPRouteKey.String("/merchant/places/block/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PlacesBlockPublicV1MerchantPlacesBlockPost",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PlacesBlockPublicV1MerchantPlacesBlockPost",
			ID:   "places_block_public_v1_merchant_places_block__post",
		}
	)
	request, close, err := s.decodePlacesBlockPublicV1MerchantPlacesBlockPostRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PlacesBlockPublicV1MerchantPlacesBlockPostRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PlacesBlockPublicV1MerchantPlacesBlockPost",
			OperationID:   "places_block_public_v1_merchant_places_block__post",
			Body:          request,
			Params:        middleware.Parameters{},
			Raw:           r,
		}

		type (
			Request  = *PlaceBlockInRequest
			Params   = struct{}
			Response = PlacesBlockPublicV1MerchantPlacesBlockPostRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			nil,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PlacesBlockPublicV1MerchantPlacesBlockPost(ctx, request)
				return response, err
			},
		)
	} else {
		response, err = s.h.PlacesBlockPublicV1MerchantPlacesBlockPost(ctx, request)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePlacesBlockPublicV1MerchantPlacesBlockPostResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePlacesBlocksPublicV1MerchantPlacesBlocksGetRequest handles places_blocks_public_v1_merchant_places_blocks__get operation.
//
// Returns list of active places blocks.
//
// GET /merchant/places/blocks/
func (s *Server) handlePlacesBlocksPublicV1MerchantPlacesBlocksGetRequest(args [0]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_blocks_public_v1_merchant_places_blocks__get"),
		semconv.HTTPMethodKey.String("GET"),
		semconv.HTTPRouteKey.String("/merchant/places/blocks/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PlacesBlocksPublicV1MerchantPlacesBlocksGet",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PlacesBlocksPublicV1MerchantPlacesBlocksGet",
			ID:   "places_blocks_public_v1_merchant_places_blocks__get",
		}
	)
	params, err := decodePlacesBlocksPublicV1MerchantPlacesBlocksGetParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	var response PlacesBlocksPublicV1MerchantPlacesBlocksGetRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PlacesBlocksPublicV1MerchantPlacesBlocksGet",
			OperationID:   "places_blocks_public_v1_merchant_places_blocks__get",
			Body:          nil,
			Params: middleware.Parameters{
				{
					Name: "place_ids",
					In:   "query",
				}: params.PlaceIds,
				{
					Name: "limit",
					In:   "query",
				}: params.Limit,
				{
					Name: "offset",
					In:   "query",
				}: params.Offset,
			},
			Raw: r,
		}

		type (
			Request  = struct{}
			Params   = PlacesBlocksPublicV1MerchantPlacesBlocksGetParams
			Response = PlacesBlocksPublicV1MerchantPlacesBlocksGetRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackPlacesBlocksPublicV1MerchantPlacesBlocksGetParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PlacesBlocksPublicV1MerchantPlacesBlocksGet(ctx, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.PlacesBlocksPublicV1MerchantPlacesBlocksGet(ctx, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePlacesBlocksPublicV1MerchantPlacesBlocksGetResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePlacesCreatePublicV1MerchantPlacesPostRequest handles places_create_public_v1_merchant_places_post operation.
//
// Places:Create.
//
// POST /merchant/places
func (s *Server) handlePlacesCreatePublicV1MerchantPlacesPostRequest(args [0]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_create_public_v1_merchant_places_post"),
		semconv.HTTPMethodKey.String("POST"),
		semconv.HTTPRouteKey.String("/merchant/places"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PlacesCreatePublicV1MerchantPlacesPost",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PlacesCreatePublicV1MerchantPlacesPost",
			ID:   "places_create_public_v1_merchant_places_post",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PlacesCreatePublicV1MerchantPlacesPost", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	request, close, err := s.decodePlacesCreatePublicV1MerchantPlacesPostRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PlacesCreatePublicV1MerchantPlacesPostRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PlacesCreatePublicV1MerchantPlacesPost",
			OperationID:   "places_create_public_v1_merchant_places_post",
			Body:          request,
			Params:        middleware.Parameters{},
			Raw:           r,
		}

		type (
			Request  = *PlaceWithArea
			Params   = struct{}
			Response = PlacesCreatePublicV1MerchantPlacesPostRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			nil,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PlacesCreatePublicV1MerchantPlacesPost(ctx, request)
				return response, err
			},
		)
	} else {
		response, err = s.h.PlacesCreatePublicV1MerchantPlacesPost(ctx, request)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePlacesCreatePublicV1MerchantPlacesPostResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePlacesListPublicV1MerchantPlacesGetRequest handles places_list_public_v1_merchant_places_get operation.
//
// Places:List.
//
// GET /merchant/places
func (s *Server) handlePlacesListPublicV1MerchantPlacesGetRequest(args [0]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_list_public_v1_merchant_places_get"),
		semconv.HTTPMethodKey.String("GET"),
		semconv.HTTPRouteKey.String("/merchant/places"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PlacesListPublicV1MerchantPlacesGet",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PlacesListPublicV1MerchantPlacesGet",
			ID:   "places_list_public_v1_merchant_places_get",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PlacesListPublicV1MerchantPlacesGet", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}

	var response PlacesListPublicV1MerchantPlacesGetRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PlacesListPublicV1MerchantPlacesGet",
			OperationID:   "places_list_public_v1_merchant_places_get",
			Body:          nil,
			Params:        middleware.Parameters{},
			Raw:           r,
		}

		type (
			Request  = struct{}
			Params   = struct{}
			Response = PlacesListPublicV1MerchantPlacesGetRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			nil,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PlacesListPublicV1MerchantPlacesGet(ctx)
				return response, err
			},
		)
	} else {
		response, err = s.h.PlacesListPublicV1MerchantPlacesGet(ctx)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePlacesListPublicV1MerchantPlacesGetResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRequest handles places_order_check_public_v1_merchant_places_order_check_post operation.
//
// Check whether delivery from place is possible to adress.
//
// POST /merchant/places/order-check
func (s *Server) handlePlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRequest(args [0]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_order_check_public_v1_merchant_places_order_check_post"),
		semconv.HTTPMethodKey.String("POST"),
		semconv.HTTPRouteKey.String("/merchant/places/order-check"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost",
			ID:   "places_order_check_public_v1_merchant_places_order_check_post",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	request, close, err := s.decodePlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost",
			OperationID:   "places_order_check_public_v1_merchant_places_order_check_post",
			Body:          request,
			Params:        middleware.Parameters{},
			Raw:           r,
		}

		type (
			Request  = *OrderCheckInRequest
			Params   = struct{}
			Response = PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			nil,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost(ctx, request)
				return response, err
			},
		)
	} else {
		response, err = s.h.PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost(ctx, request)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePlacesUnblockPublicV1MerchantPlacesUnblockPostRequest handles places_unblock_public_v1_merchant_places_unblock__post operation.
//
// Unblocks places.
//
// POST /merchant/places/unblock/
func (s *Server) handlePlacesUnblockPublicV1MerchantPlacesUnblockPostRequest(args [0]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_unblock_public_v1_merchant_places_unblock__post"),
		semconv.HTTPMethodKey.String("POST"),
		semconv.HTTPRouteKey.String("/merchant/places/unblock/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PlacesUnblockPublicV1MerchantPlacesUnblockPost",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PlacesUnblockPublicV1MerchantPlacesUnblockPost",
			ID:   "places_unblock_public_v1_merchant_places_unblock__post",
		}
	)
	request, close, err := s.decodePlacesUnblockPublicV1MerchantPlacesUnblockPostRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PlacesUnblockPublicV1MerchantPlacesUnblockPostRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PlacesUnblockPublicV1MerchantPlacesUnblockPost",
			OperationID:   "places_unblock_public_v1_merchant_places_unblock__post",
			Body:          request,
			Params:        middleware.Parameters{},
			Raw:           r,
		}

		type (
			Request  = *PlaceUnBlockInRequest
			Params   = struct{}
			Response = PlacesUnblockPublicV1MerchantPlacesUnblockPostRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			nil,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PlacesUnblockPublicV1MerchantPlacesUnblockPost(ctx, request)
				return response, err
			},
		)
	} else {
		response, err = s.h.PlacesUnblockPublicV1MerchantPlacesUnblockPost(ctx, request)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePlacesUnblockPublicV1MerchantPlacesUnblockPostResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePlacesUpdatePublicV1MerchantPlacesPlaceIDPutRequest handles places_update_public_v1_merchant_places__place_id__put operation.
//
// Places:Update.
//
// PUT /merchant/places/{place_id}
func (s *Server) handlePlacesUpdatePublicV1MerchantPlacesPlaceIDPutRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_update_public_v1_merchant_places__place_id__put"),
		semconv.HTTPMethodKey.String("PUT"),
		semconv.HTTPRouteKey.String("/merchant/places/{place_id}"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PlacesUpdatePublicV1MerchantPlacesPlaceIDPut",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PlacesUpdatePublicV1MerchantPlacesPlaceIDPut",
			ID:   "places_update_public_v1_merchant_places__place_id__put",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PlacesUpdatePublicV1MerchantPlacesPlaceIDPut", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	params, err := decodePlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	request, close, err := s.decodePlacesUpdatePublicV1MerchantPlacesPlaceIDPutRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PlacesUpdatePublicV1MerchantPlacesPlaceIDPutRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PlacesUpdatePublicV1MerchantPlacesPlaceIDPut",
			OperationID:   "places_update_public_v1_merchant_places__place_id__put",
			Body:          request,
			Params: middleware.Parameters{
				{
					Name: "place_id",
					In:   "path",
				}: params.PlaceID,
			},
			Raw: r,
		}

		type (
			Request  = *PlaceUpdate
			Params   = PlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams
			Response = PlacesUpdatePublicV1MerchantPlacesPlaceIDPutRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackPlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PlacesUpdatePublicV1MerchantPlacesPlaceIDPut(ctx, request, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.PlacesUpdatePublicV1MerchantPlacesPlaceIDPut(ctx, request, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePlacesUpdatePublicV1MerchantPlacesPlaceIDPutResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePublicCancelOrderPublicV1OrdersOrderIDDeleteRequest handles public_cancel_order_public_v1_orders__order_id___delete operation.
//
// Cancel order.
//
// DELETE /orders/{order_id}/
func (s *Server) handlePublicCancelOrderPublicV1OrdersOrderIDDeleteRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_cancel_order_public_v1_orders__order_id___delete"),
		semconv.HTTPMethodKey.String("DELETE"),
		semconv.HTTPRouteKey.String("/orders/{order_id}/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PublicCancelOrderPublicV1OrdersOrderIDDelete",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PublicCancelOrderPublicV1OrdersOrderIDDelete",
			ID:   "public_cancel_order_public_v1_orders__order_id___delete",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PublicCancelOrderPublicV1OrdersOrderIDDelete", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	params, err := decodePublicCancelOrderPublicV1OrdersOrderIDDeleteParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	request, close, err := s.decodePublicCancelOrderPublicV1OrdersOrderIDDeleteRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PublicCancelOrderPublicV1OrdersOrderIDDeleteRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PublicCancelOrderPublicV1OrdersOrderIDDelete",
			OperationID:   "public_cancel_order_public_v1_orders__order_id___delete",
			Body:          request,
			Params: middleware.Parameters{
				{
					Name: "order_id",
					In:   "path",
				}: params.OrderID,
			},
			Raw: r,
		}

		type (
			Request  = *CancelOrderInRequest
			Params   = PublicCancelOrderPublicV1OrdersOrderIDDeleteParams
			Response = PublicCancelOrderPublicV1OrdersOrderIDDeleteRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackPublicCancelOrderPublicV1OrdersOrderIDDeleteParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PublicCancelOrderPublicV1OrdersOrderIDDelete(ctx, request, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.PublicCancelOrderPublicV1OrdersOrderIDDelete(ctx, request, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePublicCancelOrderPublicV1OrdersOrderIDDeleteResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePublicCreateOrderPublicV1OrdersPostRequest handles public_create_order_public_v1_orders__post operation.
//
// Create order to delivery.
//
// POST /orders/
func (s *Server) handlePublicCreateOrderPublicV1OrdersPostRequest(args [0]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_create_order_public_v1_orders__post"),
		semconv.HTTPMethodKey.String("POST"),
		semconv.HTTPRouteKey.String("/orders/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PublicCreateOrderPublicV1OrdersPost",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PublicCreateOrderPublicV1OrdersPost",
			ID:   "public_create_order_public_v1_orders__post",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PublicCreateOrderPublicV1OrdersPost", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	request, close, err := s.decodePublicCreateOrderPublicV1OrdersPostRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PublicCreateOrderPublicV1OrdersPostRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PublicCreateOrderPublicV1OrdersPost",
			OperationID:   "public_create_order_public_v1_orders__post",
			Body:          request,
			Params:        middleware.Parameters{},
			Raw:           r,
		}

		type (
			Request  = *CreateOrderInRequest
			Params   = struct{}
			Response = PublicCreateOrderPublicV1OrdersPostRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			nil,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PublicCreateOrderPublicV1OrdersPost(ctx, request)
				return response, err
			},
		)
	} else {
		response, err = s.h.PublicCreateOrderPublicV1OrdersPost(ctx, request)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePublicCreateOrderPublicV1OrdersPostResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePublicDryRunPublicV1OrdersDryRunPostRequest handles public_dry_run_public_v1_orders_dry_run__post operation.
//
// Check whether order can be created.
//
// POST /orders/dry_run/
func (s *Server) handlePublicDryRunPublicV1OrdersDryRunPostRequest(args [0]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_dry_run_public_v1_orders_dry_run__post"),
		semconv.HTTPMethodKey.String("POST"),
		semconv.HTTPRouteKey.String("/orders/dry_run/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PublicDryRunPublicV1OrdersDryRunPost",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PublicDryRunPublicV1OrdersDryRunPost",
			ID:   "public_dry_run_public_v1_orders_dry_run__post",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PublicDryRunPublicV1OrdersDryRunPost", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	request, close, err := s.decodePublicDryRunPublicV1OrdersDryRunPostRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PublicDryRunPublicV1OrdersDryRunPostRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PublicDryRunPublicV1OrdersDryRunPost",
			OperationID:   "public_dry_run_public_v1_orders_dry_run__post",
			Body:          request,
			Params:        middleware.Parameters{},
			Raw:           r,
		}

		type (
			Request  = *OrderDryRun
			Params   = struct{}
			Response = PublicDryRunPublicV1OrdersDryRunPostRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			nil,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PublicDryRunPublicV1OrdersDryRunPost(ctx, request)
				return response, err
			},
		)
	} else {
		response, err = s.h.PublicDryRunPublicV1OrdersDryRunPost(ctx, request)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePublicDryRunPublicV1OrdersDryRunPostResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRequest handles public_get_courier_position_public_v1_orders__order_id__tracking__get operation.
//
// Public:Get Courier Position.
//
// GET /orders/{order_id}/tracking/
func (s *Server) handlePublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_get_courier_position_public_v1_orders__order_id__tracking__get"),
		semconv.HTTPMethodKey.String("GET"),
		semconv.HTTPRouteKey.String("/orders/{order_id}/tracking/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet",
			ID:   "public_get_courier_position_public_v1_orders__order_id__tracking__get",
		}
	)
	params, err := decodePublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	var response PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet",
			OperationID:   "public_get_courier_position_public_v1_orders__order_id__tracking__get",
			Body:          nil,
			Params: middleware.Parameters{
				{
					Name: "order_id",
					In:   "path",
				}: params.OrderID,
			},
			Raw: r,
		}

		type (
			Request  = struct{}
			Params   = PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams
			Response = PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackPublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet(ctx, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet(ctx, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRequest handles public_get_order_photos_public_v1_orders__order_id__photos__get operation.
//
// Getting photos of the order.
//
// GET /orders/{order_id}/photos/
func (s *Server) handlePublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_get_order_photos_public_v1_orders__order_id__photos__get"),
		semconv.HTTPMethodKey.String("GET"),
		semconv.HTTPRouteKey.String("/orders/{order_id}/photos/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet",
			ID:   "public_get_order_photos_public_v1_orders__order_id__photos__get",
		}
	)
	params, err := decodePublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	var response PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet",
			OperationID:   "public_get_order_photos_public_v1_orders__order_id__photos__get",
			Body:          nil,
			Params: middleware.Parameters{
				{
					Name: "order_id",
					In:   "path",
				}: params.OrderID,
				{
					Name: "photo_type",
					In:   "query",
				}: params.PhotoType,
			},
			Raw: r,
		}

		type (
			Request  = struct{}
			Params   = PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams
			Response = PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackPublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet(ctx, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet(ctx, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePublicGetOrderPublicV1OrdersOrderIDGetRequest handles public_get_order_public_v1_orders__order_id___get operation.
//
// Get order info.
//
// GET /orders/{order_id}/
func (s *Server) handlePublicGetOrderPublicV1OrdersOrderIDGetRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_get_order_public_v1_orders__order_id___get"),
		semconv.HTTPMethodKey.String("GET"),
		semconv.HTTPRouteKey.String("/orders/{order_id}/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PublicGetOrderPublicV1OrdersOrderIDGet",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PublicGetOrderPublicV1OrdersOrderIDGet",
			ID:   "public_get_order_public_v1_orders__order_id___get",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PublicGetOrderPublicV1OrdersOrderIDGet", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	params, err := decodePublicGetOrderPublicV1OrdersOrderIDGetParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	var response PublicGetOrderPublicV1OrdersOrderIDGetRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PublicGetOrderPublicV1OrdersOrderIDGet",
			OperationID:   "public_get_order_public_v1_orders__order_id___get",
			Body:          nil,
			Params: middleware.Parameters{
				{
					Name: "order_id",
					In:   "path",
				}: params.OrderID,
			},
			Raw: r,
		}

		type (
			Request  = struct{}
			Params   = PublicGetOrderPublicV1OrdersOrderIDGetParams
			Response = PublicGetOrderPublicV1OrdersOrderIDGetRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackPublicGetOrderPublicV1OrdersOrderIDGetParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PublicGetOrderPublicV1OrdersOrderIDGet(ctx, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.PublicGetOrderPublicV1OrdersOrderIDGet(ctx, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePublicGetOrderPublicV1OrdersOrderIDGetResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePublicReturnOrderPublicV1OrdersOrderIDReturnPatchRequest handles public_return_order_public_v1_orders__order_id__return__patch operation.
//
// Return products of order (deprecated).
//
// PATCH /orders/{order_id}/return/
func (s *Server) handlePublicReturnOrderPublicV1OrdersOrderIDReturnPatchRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_return_order_public_v1_orders__order_id__return__patch"),
		semconv.HTTPMethodKey.String("PATCH"),
		semconv.HTTPRouteKey.String("/orders/{order_id}/return/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PublicReturnOrderPublicV1OrdersOrderIDReturnPatch",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PublicReturnOrderPublicV1OrdersOrderIDReturnPatch",
			ID:   "public_return_order_public_v1_orders__order_id__return__patch",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PublicReturnOrderPublicV1OrdersOrderIDReturnPatch", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	params, err := decodePublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	request, close, err := s.decodePublicReturnOrderPublicV1OrdersOrderIDReturnPatchRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PublicReturnOrderPublicV1OrdersOrderIDReturnPatchRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PublicReturnOrderPublicV1OrdersOrderIDReturnPatch",
			OperationID:   "public_return_order_public_v1_orders__order_id__return__patch",
			Body:          request,
			Params: middleware.Parameters{
				{
					Name: "order_id",
					In:   "path",
				}: params.OrderID,
			},
			Raw: r,
		}

		type (
			Request  = *ReturnProductsInRequest
			Params   = PublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams
			Response = PublicReturnOrderPublicV1OrdersOrderIDReturnPatchRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackPublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PublicReturnOrderPublicV1OrdersOrderIDReturnPatch(ctx, request, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.PublicReturnOrderPublicV1OrdersOrderIDReturnPatch(ctx, request, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePublicReturnOrderPublicV1OrdersOrderIDReturnPatchResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePublicReturnOrderPublicV2OrdersOrderIDReturnPatchRequest handles public_return_order_public_v2_orders__order_id__return__patch operation.
//
// Return products of order.
//
// PATCH /v2/orders/{order_id}/return/
func (s *Server) handlePublicReturnOrderPublicV2OrdersOrderIDReturnPatchRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_return_order_public_v2_orders__order_id__return__patch"),
		semconv.HTTPMethodKey.String("PATCH"),
		semconv.HTTPRouteKey.String("/v2/orders/{order_id}/return/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PublicReturnOrderPublicV2OrdersOrderIDReturnPatch",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PublicReturnOrderPublicV2OrdersOrderIDReturnPatch",
			ID:   "public_return_order_public_v2_orders__order_id__return__patch",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PublicReturnOrderPublicV2OrdersOrderIDReturnPatch", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	params, err := decodePublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	request, close, err := s.decodePublicReturnOrderPublicV2OrdersOrderIDReturnPatchRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PublicReturnOrderPublicV2OrdersOrderIDReturnPatchRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PublicReturnOrderPublicV2OrdersOrderIDReturnPatch",
			OperationID:   "public_return_order_public_v2_orders__order_id__return__patch",
			Body:          request,
			Params: middleware.Parameters{
				{
					Name: "order_id",
					In:   "path",
				}: params.OrderID,
			},
			Raw: r,
		}

		type (
			Request  = *ReturnProductsInRequestV2
			Params   = PublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams
			Response = PublicReturnOrderPublicV2OrdersOrderIDReturnPatchRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackPublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PublicReturnOrderPublicV2OrdersOrderIDReturnPatch(ctx, request, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.PublicReturnOrderPublicV2OrdersOrderIDReturnPatch(ctx, request, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePublicReturnOrderPublicV2OrdersOrderIDReturnPatchResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}

// handlePublicUpdateOrderPublicV1OrdersOrderIDPatchRequest handles public_update_order_public_v1_orders__order_id___patch operation.
//
// Update order. Undesirable to use to cancel an order.
//
// PATCH /orders/{order_id}/
func (s *Server) handlePublicUpdateOrderPublicV1OrdersOrderIDPatchRequest(args [1]string, argsEscaped bool, w http.ResponseWriter, r *http.Request) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_update_order_public_v1_orders__order_id___patch"),
		semconv.HTTPMethodKey.String("PATCH"),
		semconv.HTTPRouteKey.String("/orders/{order_id}/"),
	}

	// Start a span for this request.
	ctx, span := s.cfg.Tracer.Start(r.Context(), "PublicUpdateOrderPublicV1OrdersOrderIDPatch",
		trace.WithAttributes(otelAttrs...),
		serverSpanKind,
	)
	defer span.End()

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		s.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	s.requests.Add(ctx, 1, otelAttrs...)

	var (
		recordError = func(stage string, err error) {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			s.errors.Add(ctx, 1, otelAttrs...)
		}
		err          error
		opErrContext = ogenerrors.OperationContext{
			Name: "PublicUpdateOrderPublicV1OrdersOrderIDPatch",
			ID:   "public_update_order_public_v1_orders__order_id___patch",
		}
	)
	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			sctx, ok, err := s.securityAPIKeyHeader(ctx, "PublicUpdateOrderPublicV1OrdersOrderIDPatch", r)
			if err != nil {
				err = &ogenerrors.SecurityError{
					OperationContext: opErrContext,
					Security:         "APIKeyHeader",
					Err:              err,
				}
				recordError("Security:APIKeyHeader", err)
				s.cfg.ErrorHandler(ctx, w, r, err)
				return
			}
			if ok {
				satisfied[0] |= 1 << 0
				ctx = sctx
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			err = &ogenerrors.SecurityError{
				OperationContext: opErrContext,
				Err:              ogenerrors.ErrSecurityRequirementIsNotSatisfied,
			}
			recordError("Security", err)
			s.cfg.ErrorHandler(ctx, w, r, err)
			return
		}
	}
	params, err := decodePublicUpdateOrderPublicV1OrdersOrderIDPatchParams(args, argsEscaped, r)
	if err != nil {
		err = &ogenerrors.DecodeParamsError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeParams", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	request, close, err := s.decodePublicUpdateOrderPublicV1OrdersOrderIDPatchRequest(r)
	if err != nil {
		err = &ogenerrors.DecodeRequestError{
			OperationContext: opErrContext,
			Err:              err,
		}
		recordError("DecodeRequest", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
	defer func() {
		if err := close(); err != nil {
			recordError("CloseRequest", err)
		}
	}()

	var response PublicUpdateOrderPublicV1OrdersOrderIDPatchRes
	if m := s.cfg.Middleware; m != nil {
		mreq := middleware.Request{
			Context:       ctx,
			OperationName: "PublicUpdateOrderPublicV1OrdersOrderIDPatch",
			OperationID:   "public_update_order_public_v1_orders__order_id___patch",
			Body:          request,
			Params: middleware.Parameters{
				{
					Name: "order_id",
					In:   "path",
				}: params.OrderID,
			},
			Raw: r,
		}

		type (
			Request  = *UpdateOrderInRequest
			Params   = PublicUpdateOrderPublicV1OrdersOrderIDPatchParams
			Response = PublicUpdateOrderPublicV1OrdersOrderIDPatchRes
		)
		response, err = middleware.HookMiddleware[
			Request,
			Params,
			Response,
		](
			m,
			mreq,
			unpackPublicUpdateOrderPublicV1OrdersOrderIDPatchParams,
			func(ctx context.Context, request Request, params Params) (response Response, err error) {
				response, err = s.h.PublicUpdateOrderPublicV1OrdersOrderIDPatch(ctx, request, params)
				return response, err
			},
		)
	} else {
		response, err = s.h.PublicUpdateOrderPublicV1OrdersOrderIDPatch(ctx, request, params)
	}
	if err != nil {
		recordError("Internal", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}

	if err := encodePublicUpdateOrderPublicV1OrdersOrderIDPatchResponse(response, w, span); err != nil {
		recordError("EncodeResponse", err)
		s.cfg.ErrorHandler(ctx, w, r, err)
		return
	}
}
