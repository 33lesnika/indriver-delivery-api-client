package api

import (
	"net/http"

	"github.com/go-faster/errors"
	"github.com/go-faster/jx"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func encodeMerchantCourierNearDeliveryAddressCallbackURLOrderIDPostResponse(response *MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostOK, w http.ResponseWriter, span trace.Span) error {
	w.WriteHeader(200)
	span.SetStatus(codes.Ok, http.StatusText(200))

	return nil
}

func encodeMerchantCourierNearPlaceCallbackURLOrderIDPostResponse(response *MerchantCourierNearPlaceCallbackURLOrderIDPostOK, w http.ResponseWriter, span trace.Span) error {
	w.WriteHeader(200)
	span.SetStatus(codes.Ok, http.StatusText(200))

	return nil
}

func encodeMerchantOrderStatusCallbackURLOrderIDPostResponse(response *MerchantOrderStatusCallbackURLOrderIDPostOK, w http.ResponseWriter, span trace.Span) error {
	w.WriteHeader(200)
	span.SetStatus(codes.Ok, http.StatusText(200))

	return nil
}

func encodeMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetResponse(response MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *WorkTimes:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodeMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutResponse(response MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *WorkTimes:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetResponse(response PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *WorkTimes:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutResponse(response PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *WorkTimes:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePlacesBlockPublicV1MerchantPlacesBlockPostResponse(response PlacesBlockPublicV1MerchantPlacesBlockPostRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PlacesBlockPublicV1MerchantPlacesBlockPostOKApplicationJSON:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePlacesBlocksPublicV1MerchantPlacesBlocksGetResponse(response PlacesBlocksPublicV1MerchantPlacesBlocksGetRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PlacesBlocksPublicV1MerchantPlacesBlocksGetOKApplicationJSON:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePlacesCreatePublicV1MerchantPlacesPostResponse(response PlacesCreatePublicV1MerchantPlacesPostRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PlaceInDB:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePlacesListPublicV1MerchantPlacesGetResponse(response PlacesListPublicV1MerchantPlacesGetRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PlacesListPublicV1MerchantPlacesGetOKApplicationJSON:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostResponse(response PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PlaceAccessList:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePlacesUnblockPublicV1MerchantPlacesUnblockPostResponse(response PlacesUnblockPublicV1MerchantPlacesUnblockPostRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PlacesUnblockPublicV1MerchantPlacesUnblockPostOKApplicationJSON:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePlacesUpdatePublicV1MerchantPlacesPlaceIDPutResponse(response PlacesUpdatePublicV1MerchantPlacesPlaceIDPutRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PlaceInDB:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePublicCancelOrderPublicV1OrdersOrderIDDeleteResponse(response PublicCancelOrderPublicV1OrdersOrderIDDeleteRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PublicCancelOrderPublicV1OrdersOrderIDDeleteNoContent:
		w.WriteHeader(204)
		span.SetStatus(codes.Ok, http.StatusText(204))

		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePublicCreateOrderPublicV1OrdersPostResponse(response PublicCreateOrderPublicV1OrdersPostRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PublicOrderDetailForResponse:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *PublicCreateOrderPublicV1OrdersPostForbidden:
		w.WriteHeader(403)
		span.SetStatus(codes.Error, http.StatusText(403))

		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePublicDryRunPublicV1OrdersDryRunPostResponse(response PublicDryRunPublicV1OrdersDryRunPostRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *BaseSchemaResponse:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *PublicDryRunPublicV1OrdersDryRunPostForbidden:
		w.WriteHeader(403)
		span.SetStatus(codes.Error, http.StatusText(403))

		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetResponse(response PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PublicCourierPositionForResponse:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetResponse(response PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetOKApplicationJSON:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePublicGetOrderPublicV1OrdersOrderIDGetResponse(response PublicGetOrderPublicV1OrdersOrderIDGetRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PublicOrderDetailForResponse:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *PublicGetOrderPublicV1OrdersOrderIDGetNotFound:
		w.WriteHeader(404)
		span.SetStatus(codes.Error, http.StatusText(404))

		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePublicReturnOrderPublicV1OrdersOrderIDReturnPatchResponse(response PublicReturnOrderPublicV1OrdersOrderIDReturnPatchRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PublicReturnOrderPublicV1OrdersOrderIDReturnPatchOKApplicationJSON:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePublicReturnOrderPublicV2OrdersOrderIDReturnPatchResponse(response PublicReturnOrderPublicV2OrdersOrderIDReturnPatchRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PublicReturnOrderPublicV2OrdersOrderIDReturnPatchOKApplicationJSON:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}

func encodePublicUpdateOrderPublicV1OrdersOrderIDPatchResponse(response PublicUpdateOrderPublicV1OrdersOrderIDPatchRes, w http.ResponseWriter, span trace.Span) error {
	switch response := response.(type) {
	case *PublicOrderDetailForResponse:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		span.SetStatus(codes.Ok, http.StatusText(200))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	case *PublicUpdateOrderPublicV1OrdersOrderIDPatchNotFound:
		w.WriteHeader(404)
		span.SetStatus(codes.Error, http.StatusText(404))

		return nil

	case *HTTPValidationError:
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(422)
		span.SetStatus(codes.Error, http.StatusText(422))

		e := jx.GetEncoder()
		response.Encode(e)
		if _, err := e.WriteTo(w); err != nil {
			return errors.Wrap(err, "write")
		}
		return nil

	default:
		return errors.Errorf("unexpected response type: %T", response)
	}
}
