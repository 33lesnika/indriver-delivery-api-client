package api

import (
	"context"
)

// Handler handles operations described by OpenAPI v3 specification.
type Handler interface {
	// MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost implements POST /merchant_courier_near_delivery_address_callback_url/{order_id} operation.
	//
	// Notifies that the courier is located near the delivery address.
	//
	// POST /merchant_courier_near_delivery_address_callback_url/{order_id}
	MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost(ctx context.Context, params MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams) error
	// MerchantCourierNearPlaceCallbackURLOrderIDPost implements POST /merchant_courier_near_place_callback_url/{order_id} operation.
	//
	// Notifies that the courier is located near the place.
	//
	// POST /merchant_courier_near_place_callback_url/{order_id}
	MerchantCourierNearPlaceCallbackURLOrderIDPost(ctx context.Context, params MerchantCourierNearPlaceCallbackURLOrderIDPostParams) error
	// MerchantOrderStatusCallbackURLOrderIDPost implements POST /merchant_order_status_callback_url/{order_id} operation.
	//
	// Notifies about order's status update or about delivery delaying.
	//
	// POST /merchant_order_status_callback_url/{order_id}
	MerchantOrderStatusCallbackURLOrderIDPost(ctx context.Context, req OptOrderStatusCallback, params MerchantOrderStatusCallbackURLOrderIDPostParams) error
	// MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet implements merchant_schedule_get_public_v1_merchant__merchant_id__schedule_get operation.
	//
	// Merchant Schedule:Get.
	//
	// GET /merchant/{merchant_id}/schedule
	MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet(ctx context.Context, params MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams) (MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRes, error)
	// MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut implements merchant_schedule_update_public_v1_merchant__merchant_id__schedule_put operation.
	//
	// Merchant Schedule:Update.
	//
	// PUT /merchant/{merchant_id}/schedule
	MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut(ctx context.Context, req *WorkTimesUpdate, params MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams) (MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRes, error)
	// PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet implements place_schedule_get_public_v1_merchant_places__place_id__schedule_get operation.
	//
	// Place Schedule:Get.
	//
	// GET /merchant/places/{place_id}/schedule
	PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet(ctx context.Context, params PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams) (PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRes, error)
	// PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut implements place_schedule_update_public_v1_merchant_places__place_id__schedule_put operation.
	//
	// Place Schedule:Update.
	//
	// PUT /merchant/places/{place_id}/schedule
	PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut(ctx context.Context, req *WorkTimesUpdate, params PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams) (PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRes, error)
	// PlacesBlockPublicV1MerchantPlacesBlockPost implements places_block_public_v1_merchant_places_block__post operation.
	//
	// Blocks places for the specified period or endless. Use if you want to temporally stop orders
	// creating for one or list of places.
	//
	// POST /merchant/places/block/
	PlacesBlockPublicV1MerchantPlacesBlockPost(ctx context.Context, req *PlaceBlockInRequest) (PlacesBlockPublicV1MerchantPlacesBlockPostRes, error)
	// PlacesBlocksPublicV1MerchantPlacesBlocksGet implements places_blocks_public_v1_merchant_places_blocks__get operation.
	//
	// Returns list of active places blocks.
	//
	// GET /merchant/places/blocks/
	PlacesBlocksPublicV1MerchantPlacesBlocksGet(ctx context.Context, params PlacesBlocksPublicV1MerchantPlacesBlocksGetParams) (PlacesBlocksPublicV1MerchantPlacesBlocksGetRes, error)
	// PlacesCreatePublicV1MerchantPlacesPost implements places_create_public_v1_merchant_places_post operation.
	//
	// Places:Create.
	//
	// POST /merchant/places
	PlacesCreatePublicV1MerchantPlacesPost(ctx context.Context, req *PlaceWithArea) (PlacesCreatePublicV1MerchantPlacesPostRes, error)
	// PlacesListPublicV1MerchantPlacesGet implements places_list_public_v1_merchant_places_get operation.
	//
	// Places:List.
	//
	// GET /merchant/places
	PlacesListPublicV1MerchantPlacesGet(ctx context.Context) (PlacesListPublicV1MerchantPlacesGetRes, error)
	// PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost implements places_order_check_public_v1_merchant_places_order_check_post operation.
	//
	// Check whether delivery from place is possible to adress.
	//
	// POST /merchant/places/order-check
	PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost(ctx context.Context, req *OrderCheckInRequest) (PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRes, error)
	// PlacesUnblockPublicV1MerchantPlacesUnblockPost implements places_unblock_public_v1_merchant_places_unblock__post operation.
	//
	// Unblocks places.
	//
	// POST /merchant/places/unblock/
	PlacesUnblockPublicV1MerchantPlacesUnblockPost(ctx context.Context, req *PlaceUnBlockInRequest) (PlacesUnblockPublicV1MerchantPlacesUnblockPostRes, error)
	// PlacesUpdatePublicV1MerchantPlacesPlaceIDPut implements places_update_public_v1_merchant_places__place_id__put operation.
	//
	// Places:Update.
	//
	// PUT /merchant/places/{place_id}
	PlacesUpdatePublicV1MerchantPlacesPlaceIDPut(ctx context.Context, req *PlaceUpdate, params PlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams) (PlacesUpdatePublicV1MerchantPlacesPlaceIDPutRes, error)
	// PublicCancelOrderPublicV1OrdersOrderIDDelete implements public_cancel_order_public_v1_orders__order_id___delete operation.
	//
	// Cancel order.
	//
	// DELETE /orders/{order_id}/
	PublicCancelOrderPublicV1OrdersOrderIDDelete(ctx context.Context, req *CancelOrderInRequest, params PublicCancelOrderPublicV1OrdersOrderIDDeleteParams) (PublicCancelOrderPublicV1OrdersOrderIDDeleteRes, error)
	// PublicCreateOrderPublicV1OrdersPost implements public_create_order_public_v1_orders__post operation.
	//
	// Create order to delivery.
	//
	// POST /orders/
	PublicCreateOrderPublicV1OrdersPost(ctx context.Context, req *CreateOrderInRequest) (PublicCreateOrderPublicV1OrdersPostRes, error)
	// PublicDryRunPublicV1OrdersDryRunPost implements public_dry_run_public_v1_orders_dry_run__post operation.
	//
	// Check whether order can be created.
	//
	// POST /orders/dry_run/
	PublicDryRunPublicV1OrdersDryRunPost(ctx context.Context, req *OrderDryRun) (PublicDryRunPublicV1OrdersDryRunPostRes, error)
	// PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet implements public_get_courier_position_public_v1_orders__order_id__tracking__get operation.
	//
	// Public:Get Courier Position.
	//
	// GET /orders/{order_id}/tracking/
	PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet(ctx context.Context, params PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams) (PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRes, error)
	// PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet implements public_get_order_photos_public_v1_orders__order_id__photos__get operation.
	//
	// Getting photos of the order.
	//
	// GET /orders/{order_id}/photos/
	PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet(ctx context.Context, params PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams) (PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRes, error)
	// PublicGetOrderPublicV1OrdersOrderIDGet implements public_get_order_public_v1_orders__order_id___get operation.
	//
	// Get order info.
	//
	// GET /orders/{order_id}/
	PublicGetOrderPublicV1OrdersOrderIDGet(ctx context.Context, params PublicGetOrderPublicV1OrdersOrderIDGetParams) (PublicGetOrderPublicV1OrdersOrderIDGetRes, error)
	// PublicReturnOrderPublicV1OrdersOrderIDReturnPatch implements public_return_order_public_v1_orders__order_id__return__patch operation.
	//
	// Return products of order (deprecated).
	//
	// PATCH /orders/{order_id}/return/
	PublicReturnOrderPublicV1OrdersOrderIDReturnPatch(ctx context.Context, req *ReturnProductsInRequest, params PublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams) (PublicReturnOrderPublicV1OrdersOrderIDReturnPatchRes, error)
	// PublicReturnOrderPublicV2OrdersOrderIDReturnPatch implements public_return_order_public_v2_orders__order_id__return__patch operation.
	//
	// Return products of order.
	//
	// PATCH /v2/orders/{order_id}/return/
	PublicReturnOrderPublicV2OrdersOrderIDReturnPatch(ctx context.Context, req *ReturnProductsInRequestV2, params PublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams) (PublicReturnOrderPublicV2OrdersOrderIDReturnPatchRes, error)
	// PublicUpdateOrderPublicV1OrdersOrderIDPatch implements public_update_order_public_v1_orders__order_id___patch operation.
	//
	// Update order. Undesirable to use to cancel an order.
	//
	// PATCH /orders/{order_id}/
	PublicUpdateOrderPublicV1OrdersOrderIDPatch(ctx context.Context, req *UpdateOrderInRequest, params PublicUpdateOrderPublicV1OrdersOrderIDPatchParams) (PublicUpdateOrderPublicV1OrdersOrderIDPatchRes, error)
}

// Server implements http server based on OpenAPI v3 specification and
// calls Handler to handle requests.
type Server struct {
	h   Handler
	sec SecurityHandler
	baseServer
}

// NewServer creates new Server.
func NewServer(h Handler, sec SecurityHandler, opts ...ServerOption) (*Server, error) {
	s, err := newServerConfig(opts...).baseServer()
	if err != nil {
		return nil, err
	}
	return &Server{
		h:          h,
		sec:        sec,
		baseServer: s,
	}, nil
}
