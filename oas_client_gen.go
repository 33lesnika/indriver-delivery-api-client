package api

import (
	"context"
	"net/url"
	"strings"
	"time"

	"github.com/go-faster/errors"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"

	"github.com/ogen-go/ogen/conv"
	ht "github.com/ogen-go/ogen/http"
	"github.com/ogen-go/ogen/ogenerrors"
	"github.com/ogen-go/ogen/otelogen"
	"github.com/ogen-go/ogen/uri"
)

// Client implements OAS client.
type Client struct {
	serverURL *url.URL
	sec       SecuritySource
	baseClient
}

var _ Handler = struct {
	*Client
}{}

func trimTrailingSlashes(u *url.URL) {
	u.Path = strings.TrimRight(u.Path, "/")
	u.RawPath = strings.TrimRight(u.RawPath, "/")
}

// NewClient initializes new Client defined by OAS.
func NewClient(serverURL string, sec SecuritySource, opts ...ClientOption) (*Client, error) {
	u, err := url.Parse(serverURL)
	if err != nil {
		return nil, err
	}
	trimTrailingSlashes(u)

	c, err := newClientConfig(opts...).baseClient()
	if err != nil {
		return nil, err
	}
	return &Client{
		serverURL:  u,
		sec:        sec,
		baseClient: c,
	}, nil
}

type serverURLKey struct{}

// WithServerURL sets context key to override server URL.
func WithServerURL(ctx context.Context, u *url.URL) context.Context {
	return context.WithValue(ctx, serverURLKey{}, u)
}

func (c *Client) requestURL(ctx context.Context) *url.URL {
	u, ok := ctx.Value(serverURLKey{}).(*url.URL)
	if !ok {
		return c.serverURL
	}
	return u
}

// MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost invokes POST /merchant_courier_near_delivery_address_callback_url/{order_id} operation.
//
// Notifies that the courier is located near the delivery address.
//
// POST /merchant_courier_near_delivery_address_callback_url/{order_id}
func (c *Client) MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost(ctx context.Context, params MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams) error {
	res, err := c.sendMerchantCourierNearDeliveryAddressCallbackURLOrderIDPost(ctx, params)
	_ = res
	return err
}

func (c *Client) sendMerchantCourierNearDeliveryAddressCallbackURLOrderIDPost(ctx context.Context, params MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams) (res *MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostOK, err error) {
	var otelAttrs []attribute.KeyValue

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost",
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [2]string
	pathParts[0] = "/merchant_courier_near_delivery_address_callback_url/"
	{
		// Encode "order_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "order_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.OrderID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "POST", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodeMerchantCourierNearDeliveryAddressCallbackURLOrderIDPostResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// MerchantCourierNearPlaceCallbackURLOrderIDPost invokes POST /merchant_courier_near_place_callback_url/{order_id} operation.
//
// Notifies that the courier is located near the place.
//
// POST /merchant_courier_near_place_callback_url/{order_id}
func (c *Client) MerchantCourierNearPlaceCallbackURLOrderIDPost(ctx context.Context, params MerchantCourierNearPlaceCallbackURLOrderIDPostParams) error {
	res, err := c.sendMerchantCourierNearPlaceCallbackURLOrderIDPost(ctx, params)
	_ = res
	return err
}

func (c *Client) sendMerchantCourierNearPlaceCallbackURLOrderIDPost(ctx context.Context, params MerchantCourierNearPlaceCallbackURLOrderIDPostParams) (res *MerchantCourierNearPlaceCallbackURLOrderIDPostOK, err error) {
	var otelAttrs []attribute.KeyValue

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "MerchantCourierNearPlaceCallbackURLOrderIDPost",
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [2]string
	pathParts[0] = "/merchant_courier_near_place_callback_url/"
	{
		// Encode "order_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "order_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.OrderID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "POST", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodeMerchantCourierNearPlaceCallbackURLOrderIDPostResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// MerchantOrderStatusCallbackURLOrderIDPost invokes POST /merchant_order_status_callback_url/{order_id} operation.
//
// Notifies about order's status update or about delivery delaying.
//
// POST /merchant_order_status_callback_url/{order_id}
func (c *Client) MerchantOrderStatusCallbackURLOrderIDPost(ctx context.Context, request OptOrderStatusCallback, params MerchantOrderStatusCallbackURLOrderIDPostParams) error {
	res, err := c.sendMerchantOrderStatusCallbackURLOrderIDPost(ctx, request, params)
	_ = res
	return err
}

func (c *Client) sendMerchantOrderStatusCallbackURLOrderIDPost(ctx context.Context, request OptOrderStatusCallback, params MerchantOrderStatusCallbackURLOrderIDPostParams) (res *MerchantOrderStatusCallbackURLOrderIDPostOK, err error) {
	var otelAttrs []attribute.KeyValue
	// Validate request before sending.
	if err := func() error {
		if request.Set {
			if err := func() error {
				if err := request.Value.Validate(); err != nil {
					return err
				}
				return nil
			}(); err != nil {
				return err
			}
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "MerchantOrderStatusCallbackURLOrderIDPost",
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [2]string
	pathParts[0] = "/merchant_order_status_callback_url/"
	{
		// Encode "order_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "order_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.OrderID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "POST", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodeMerchantOrderStatusCallbackURLOrderIDPostRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodeMerchantOrderStatusCallbackURLOrderIDPostResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet invokes merchant_schedule_get_public_v1_merchant__merchant_id__schedule_get operation.
//
// Merchant Schedule:Get.
//
// GET /merchant/{merchant_id}/schedule
func (c *Client) MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet(ctx context.Context, params MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams) (MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRes, error) {
	res, err := c.sendMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet(ctx, params)
	_ = res
	return res, err
}

func (c *Client) sendMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet(ctx context.Context, params MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams) (res MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("merchant_schedule_get_public_v1_merchant__merchant_id__schedule_get"),
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [3]string
	pathParts[0] = "/merchant/"
	{
		// Encode "merchant_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "merchant_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.MerchantID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	pathParts[2] = "/schedule"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "GET", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodeMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut invokes merchant_schedule_update_public_v1_merchant__merchant_id__schedule_put operation.
//
// Merchant Schedule:Update.
//
// PUT /merchant/{merchant_id}/schedule
func (c *Client) MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut(ctx context.Context, request *WorkTimesUpdate, params MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams) (MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRes, error) {
	res, err := c.sendMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut(ctx, request, params)
	_ = res
	return res, err
}

func (c *Client) sendMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut(ctx context.Context, request *WorkTimesUpdate, params MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams) (res MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("merchant_schedule_update_public_v1_merchant__merchant_id__schedule_put"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [3]string
	pathParts[0] = "/merchant/"
	{
		// Encode "merchant_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "merchant_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.MerchantID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	pathParts[2] = "/schedule"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "PUT", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodeMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodeMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet invokes place_schedule_get_public_v1_merchant_places__place_id__schedule_get operation.
//
// Place Schedule:Get.
//
// GET /merchant/places/{place_id}/schedule
func (c *Client) PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet(ctx context.Context, params PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams) (PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRes, error) {
	res, err := c.sendPlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet(ctx, params)
	_ = res
	return res, err
}

func (c *Client) sendPlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet(ctx context.Context, params PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams) (res PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("place_schedule_get_public_v1_merchant_places__place_id__schedule_get"),
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [3]string
	pathParts[0] = "/merchant/places/"
	{
		// Encode "place_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "place_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.PlaceID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	pathParts[2] = "/schedule"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "GET", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut invokes place_schedule_update_public_v1_merchant_places__place_id__schedule_put operation.
//
// Place Schedule:Update.
//
// PUT /merchant/places/{place_id}/schedule
func (c *Client) PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut(ctx context.Context, request *WorkTimesUpdate, params PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams) (PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRes, error) {
	res, err := c.sendPlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut(ctx, request, params)
	_ = res
	return res, err
}

func (c *Client) sendPlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut(ctx context.Context, request *WorkTimesUpdate, params PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams) (res PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("place_schedule_update_public_v1_merchant_places__place_id__schedule_put"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [3]string
	pathParts[0] = "/merchant/places/"
	{
		// Encode "place_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "place_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.PlaceID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	pathParts[2] = "/schedule"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "PUT", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PlacesBlockPublicV1MerchantPlacesBlockPost invokes places_block_public_v1_merchant_places_block__post operation.
//
// Blocks places for the specified period or endless. Use if you want to temporally stop orders
// creating for one or list of places.
//
// POST /merchant/places/block/
func (c *Client) PlacesBlockPublicV1MerchantPlacesBlockPost(ctx context.Context, request *PlaceBlockInRequest) (PlacesBlockPublicV1MerchantPlacesBlockPostRes, error) {
	res, err := c.sendPlacesBlockPublicV1MerchantPlacesBlockPost(ctx, request)
	_ = res
	return res, err
}

func (c *Client) sendPlacesBlockPublicV1MerchantPlacesBlockPost(ctx context.Context, request *PlaceBlockInRequest) (res PlacesBlockPublicV1MerchantPlacesBlockPostRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_block_public_v1_merchant_places_block__post"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PlacesBlockPublicV1MerchantPlacesBlockPost",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [1]string
	pathParts[0] = "/merchant/places/block/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "POST", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePlacesBlockPublicV1MerchantPlacesBlockPostRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePlacesBlockPublicV1MerchantPlacesBlockPostResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PlacesBlocksPublicV1MerchantPlacesBlocksGet invokes places_blocks_public_v1_merchant_places_blocks__get operation.
//
// Returns list of active places blocks.
//
// GET /merchant/places/blocks/
func (c *Client) PlacesBlocksPublicV1MerchantPlacesBlocksGet(ctx context.Context, params PlacesBlocksPublicV1MerchantPlacesBlocksGetParams) (PlacesBlocksPublicV1MerchantPlacesBlocksGetRes, error) {
	res, err := c.sendPlacesBlocksPublicV1MerchantPlacesBlocksGet(ctx, params)
	_ = res
	return res, err
}

func (c *Client) sendPlacesBlocksPublicV1MerchantPlacesBlocksGet(ctx context.Context, params PlacesBlocksPublicV1MerchantPlacesBlocksGetParams) (res PlacesBlocksPublicV1MerchantPlacesBlocksGetRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_blocks_public_v1_merchant_places_blocks__get"),
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PlacesBlocksPublicV1MerchantPlacesBlocksGet",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [1]string
	pathParts[0] = "/merchant/places/blocks/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeQueryParams"
	q := uri.NewQueryEncoder()
	{
		// Encode "place_ids" parameter.
		cfg := uri.QueryParameterEncodingConfig{
			Name:    "place_ids",
			Style:   uri.QueryStyleForm,
			Explode: true,
		}

		if err := q.EncodeParam(cfg, func(e uri.Encoder) error {
			return e.EncodeArray(func(e uri.Encoder) error {
				for i, item := range params.PlaceIds {
					if err := func() error {
						return e.EncodeValue(conv.UUIDToString(item))
					}(); err != nil {
						return errors.Wrapf(err, "[%d]", i)
					}
				}
				return nil
			})
		}); err != nil {
			return res, errors.Wrap(err, "encode query")
		}
	}
	{
		// Encode "limit" parameter.
		cfg := uri.QueryParameterEncodingConfig{
			Name:    "limit",
			Style:   uri.QueryStyleForm,
			Explode: true,
		}

		if err := q.EncodeParam(cfg, func(e uri.Encoder) error {
			if val, ok := params.Limit.Get(); ok {
				return e.EncodeValue(conv.IntToString(val))
			}
			return nil
		}); err != nil {
			return res, errors.Wrap(err, "encode query")
		}
	}
	{
		// Encode "offset" parameter.
		cfg := uri.QueryParameterEncodingConfig{
			Name:    "offset",
			Style:   uri.QueryStyleForm,
			Explode: true,
		}

		if err := q.EncodeParam(cfg, func(e uri.Encoder) error {
			if val, ok := params.Offset.Get(); ok {
				return e.EncodeValue(conv.IntToString(val))
			}
			return nil
		}); err != nil {
			return res, errors.Wrap(err, "encode query")
		}
	}
	u.RawQuery = q.Values().Encode()

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "GET", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePlacesBlocksPublicV1MerchantPlacesBlocksGetResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PlacesCreatePublicV1MerchantPlacesPost invokes places_create_public_v1_merchant_places_post operation.
//
// Places:Create.
//
// POST /merchant/places
func (c *Client) PlacesCreatePublicV1MerchantPlacesPost(ctx context.Context, request *PlaceWithArea) (PlacesCreatePublicV1MerchantPlacesPostRes, error) {
	res, err := c.sendPlacesCreatePublicV1MerchantPlacesPost(ctx, request)
	_ = res
	return res, err
}

func (c *Client) sendPlacesCreatePublicV1MerchantPlacesPost(ctx context.Context, request *PlaceWithArea) (res PlacesCreatePublicV1MerchantPlacesPostRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_create_public_v1_merchant_places_post"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PlacesCreatePublicV1MerchantPlacesPost",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [1]string
	pathParts[0] = "/merchant/places"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "POST", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePlacesCreatePublicV1MerchantPlacesPostRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PlacesCreatePublicV1MerchantPlacesPost", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePlacesCreatePublicV1MerchantPlacesPostResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PlacesListPublicV1MerchantPlacesGet invokes places_list_public_v1_merchant_places_get operation.
//
// Places:List.
//
// GET /merchant/places
func (c *Client) PlacesListPublicV1MerchantPlacesGet(ctx context.Context) (PlacesListPublicV1MerchantPlacesGetRes, error) {
	res, err := c.sendPlacesListPublicV1MerchantPlacesGet(ctx)
	_ = res
	return res, err
}

func (c *Client) sendPlacesListPublicV1MerchantPlacesGet(ctx context.Context) (res PlacesListPublicV1MerchantPlacesGetRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_list_public_v1_merchant_places_get"),
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PlacesListPublicV1MerchantPlacesGet",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [1]string
	pathParts[0] = "/merchant/places"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "GET", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PlacesListPublicV1MerchantPlacesGet", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePlacesListPublicV1MerchantPlacesGetResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost invokes places_order_check_public_v1_merchant_places_order_check_post operation.
//
// Check whether delivery from place is possible to adress.
//
// POST /merchant/places/order-check
func (c *Client) PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost(ctx context.Context, request *OrderCheckInRequest) (PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRes, error) {
	res, err := c.sendPlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost(ctx, request)
	_ = res
	return res, err
}

func (c *Client) sendPlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost(ctx context.Context, request *OrderCheckInRequest) (res PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_order_check_public_v1_merchant_places_order_check_post"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [1]string
	pathParts[0] = "/merchant/places/order-check"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "POST", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PlacesUnblockPublicV1MerchantPlacesUnblockPost invokes places_unblock_public_v1_merchant_places_unblock__post operation.
//
// Unblocks places.
//
// POST /merchant/places/unblock/
func (c *Client) PlacesUnblockPublicV1MerchantPlacesUnblockPost(ctx context.Context, request *PlaceUnBlockInRequest) (PlacesUnblockPublicV1MerchantPlacesUnblockPostRes, error) {
	res, err := c.sendPlacesUnblockPublicV1MerchantPlacesUnblockPost(ctx, request)
	_ = res
	return res, err
}

func (c *Client) sendPlacesUnblockPublicV1MerchantPlacesUnblockPost(ctx context.Context, request *PlaceUnBlockInRequest) (res PlacesUnblockPublicV1MerchantPlacesUnblockPostRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_unblock_public_v1_merchant_places_unblock__post"),
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PlacesUnblockPublicV1MerchantPlacesUnblockPost",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [1]string
	pathParts[0] = "/merchant/places/unblock/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "POST", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePlacesUnblockPublicV1MerchantPlacesUnblockPostRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePlacesUnblockPublicV1MerchantPlacesUnblockPostResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PlacesUpdatePublicV1MerchantPlacesPlaceIDPut invokes places_update_public_v1_merchant_places__place_id__put operation.
//
// Places:Update.
//
// PUT /merchant/places/{place_id}
func (c *Client) PlacesUpdatePublicV1MerchantPlacesPlaceIDPut(ctx context.Context, request *PlaceUpdate, params PlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams) (PlacesUpdatePublicV1MerchantPlacesPlaceIDPutRes, error) {
	res, err := c.sendPlacesUpdatePublicV1MerchantPlacesPlaceIDPut(ctx, request, params)
	_ = res
	return res, err
}

func (c *Client) sendPlacesUpdatePublicV1MerchantPlacesPlaceIDPut(ctx context.Context, request *PlaceUpdate, params PlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams) (res PlacesUpdatePublicV1MerchantPlacesPlaceIDPutRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("places_update_public_v1_merchant_places__place_id__put"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PlacesUpdatePublicV1MerchantPlacesPlaceIDPut",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [2]string
	pathParts[0] = "/merchant/places/"
	{
		// Encode "place_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "place_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.PlaceID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "PUT", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePlacesUpdatePublicV1MerchantPlacesPlaceIDPutRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PlacesUpdatePublicV1MerchantPlacesPlaceIDPut", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePlacesUpdatePublicV1MerchantPlacesPlaceIDPutResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PublicCancelOrderPublicV1OrdersOrderIDDelete invokes public_cancel_order_public_v1_orders__order_id___delete operation.
//
// Cancel order.
//
// DELETE /orders/{order_id}/
func (c *Client) PublicCancelOrderPublicV1OrdersOrderIDDelete(ctx context.Context, request *CancelOrderInRequest, params PublicCancelOrderPublicV1OrdersOrderIDDeleteParams) (PublicCancelOrderPublicV1OrdersOrderIDDeleteRes, error) {
	res, err := c.sendPublicCancelOrderPublicV1OrdersOrderIDDelete(ctx, request, params)
	_ = res
	return res, err
}

func (c *Client) sendPublicCancelOrderPublicV1OrdersOrderIDDelete(ctx context.Context, request *CancelOrderInRequest, params PublicCancelOrderPublicV1OrdersOrderIDDeleteParams) (res PublicCancelOrderPublicV1OrdersOrderIDDeleteRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_cancel_order_public_v1_orders__order_id___delete"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PublicCancelOrderPublicV1OrdersOrderIDDelete",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [3]string
	pathParts[0] = "/orders/"
	{
		// Encode "order_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "order_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.OrderID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	pathParts[2] = "/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "DELETE", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePublicCancelOrderPublicV1OrdersOrderIDDeleteRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PublicCancelOrderPublicV1OrdersOrderIDDelete", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePublicCancelOrderPublicV1OrdersOrderIDDeleteResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PublicCreateOrderPublicV1OrdersPost invokes public_create_order_public_v1_orders__post operation.
//
// Create order to delivery.
//
// POST /orders/
func (c *Client) PublicCreateOrderPublicV1OrdersPost(ctx context.Context, request *CreateOrderInRequest) (PublicCreateOrderPublicV1OrdersPostRes, error) {
	res, err := c.sendPublicCreateOrderPublicV1OrdersPost(ctx, request)
	_ = res
	return res, err
}

func (c *Client) sendPublicCreateOrderPublicV1OrdersPost(ctx context.Context, request *CreateOrderInRequest) (res PublicCreateOrderPublicV1OrdersPostRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_create_order_public_v1_orders__post"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PublicCreateOrderPublicV1OrdersPost",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [1]string
	pathParts[0] = "/orders/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "POST", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePublicCreateOrderPublicV1OrdersPostRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PublicCreateOrderPublicV1OrdersPost", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePublicCreateOrderPublicV1OrdersPostResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PublicDryRunPublicV1OrdersDryRunPost invokes public_dry_run_public_v1_orders_dry_run__post operation.
//
// Check whether order can be created.
//
// POST /orders/dry_run/
func (c *Client) PublicDryRunPublicV1OrdersDryRunPost(ctx context.Context, request *OrderDryRun) (PublicDryRunPublicV1OrdersDryRunPostRes, error) {
	res, err := c.sendPublicDryRunPublicV1OrdersDryRunPost(ctx, request)
	_ = res
	return res, err
}

func (c *Client) sendPublicDryRunPublicV1OrdersDryRunPost(ctx context.Context, request *OrderDryRun) (res PublicDryRunPublicV1OrdersDryRunPostRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_dry_run_public_v1_orders_dry_run__post"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PublicDryRunPublicV1OrdersDryRunPost",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [1]string
	pathParts[0] = "/orders/dry_run/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "POST", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePublicDryRunPublicV1OrdersDryRunPostRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PublicDryRunPublicV1OrdersDryRunPost", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePublicDryRunPublicV1OrdersDryRunPostResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet invokes public_get_courier_position_public_v1_orders__order_id__tracking__get operation.
//
// Public:Get Courier Position.
//
// GET /orders/{order_id}/tracking/
func (c *Client) PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet(ctx context.Context, params PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams) (PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRes, error) {
	res, err := c.sendPublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet(ctx, params)
	_ = res
	return res, err
}

func (c *Client) sendPublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet(ctx context.Context, params PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams) (res PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_get_courier_position_public_v1_orders__order_id__tracking__get"),
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [3]string
	pathParts[0] = "/orders/"
	{
		// Encode "order_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "order_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.OrderID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	pathParts[2] = "/tracking/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "GET", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet invokes public_get_order_photos_public_v1_orders__order_id__photos__get operation.
//
// Getting photos of the order.
//
// GET /orders/{order_id}/photos/
func (c *Client) PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet(ctx context.Context, params PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams) (PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRes, error) {
	res, err := c.sendPublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet(ctx, params)
	_ = res
	return res, err
}

func (c *Client) sendPublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet(ctx context.Context, params PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams) (res PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_get_order_photos_public_v1_orders__order_id__photos__get"),
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [3]string
	pathParts[0] = "/orders/"
	{
		// Encode "order_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "order_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.OrderID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	pathParts[2] = "/photos/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeQueryParams"
	q := uri.NewQueryEncoder()
	{
		// Encode "photo_type" parameter.
		cfg := uri.QueryParameterEncodingConfig{
			Name:    "photo_type",
			Style:   uri.QueryStyleForm,
			Explode: true,
		}

		if err := q.EncodeParam(cfg, func(e uri.Encoder) error {
			return e.EncodeArray(func(e uri.Encoder) error {
				for i, item := range params.PhotoType {
					if err := func() error {
						return e.EncodeValue(conv.IntToString(int(item)))
					}(); err != nil {
						return errors.Wrapf(err, "[%d]", i)
					}
				}
				return nil
			})
		}); err != nil {
			return res, errors.Wrap(err, "encode query")
		}
	}
	u.RawQuery = q.Values().Encode()

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "GET", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PublicGetOrderPublicV1OrdersOrderIDGet invokes public_get_order_public_v1_orders__order_id___get operation.
//
// Get order info.
//
// GET /orders/{order_id}/
func (c *Client) PublicGetOrderPublicV1OrdersOrderIDGet(ctx context.Context, params PublicGetOrderPublicV1OrdersOrderIDGetParams) (PublicGetOrderPublicV1OrdersOrderIDGetRes, error) {
	res, err := c.sendPublicGetOrderPublicV1OrdersOrderIDGet(ctx, params)
	_ = res
	return res, err
}

func (c *Client) sendPublicGetOrderPublicV1OrdersOrderIDGet(ctx context.Context, params PublicGetOrderPublicV1OrdersOrderIDGetParams) (res PublicGetOrderPublicV1OrdersOrderIDGetRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_get_order_public_v1_orders__order_id___get"),
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PublicGetOrderPublicV1OrdersOrderIDGet",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [3]string
	pathParts[0] = "/orders/"
	{
		// Encode "order_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "order_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.OrderID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	pathParts[2] = "/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "GET", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PublicGetOrderPublicV1OrdersOrderIDGet", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePublicGetOrderPublicV1OrdersOrderIDGetResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PublicReturnOrderPublicV1OrdersOrderIDReturnPatch invokes public_return_order_public_v1_orders__order_id__return__patch operation.
//
// Return products of order (deprecated).
//
// PATCH /orders/{order_id}/return/
func (c *Client) PublicReturnOrderPublicV1OrdersOrderIDReturnPatch(ctx context.Context, request *ReturnProductsInRequest, params PublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams) (PublicReturnOrderPublicV1OrdersOrderIDReturnPatchRes, error) {
	res, err := c.sendPublicReturnOrderPublicV1OrdersOrderIDReturnPatch(ctx, request, params)
	_ = res
	return res, err
}

func (c *Client) sendPublicReturnOrderPublicV1OrdersOrderIDReturnPatch(ctx context.Context, request *ReturnProductsInRequest, params PublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams) (res PublicReturnOrderPublicV1OrdersOrderIDReturnPatchRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_return_order_public_v1_orders__order_id__return__patch"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PublicReturnOrderPublicV1OrdersOrderIDReturnPatch",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [3]string
	pathParts[0] = "/orders/"
	{
		// Encode "order_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "order_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.OrderID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	pathParts[2] = "/return/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "PATCH", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePublicReturnOrderPublicV1OrdersOrderIDReturnPatchRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PublicReturnOrderPublicV1OrdersOrderIDReturnPatch", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePublicReturnOrderPublicV1OrdersOrderIDReturnPatchResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PublicReturnOrderPublicV2OrdersOrderIDReturnPatch invokes public_return_order_public_v2_orders__order_id__return__patch operation.
//
// Return products of order.
//
// PATCH /v2/orders/{order_id}/return/
func (c *Client) PublicReturnOrderPublicV2OrdersOrderIDReturnPatch(ctx context.Context, request *ReturnProductsInRequestV2, params PublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams) (PublicReturnOrderPublicV2OrdersOrderIDReturnPatchRes, error) {
	res, err := c.sendPublicReturnOrderPublicV2OrdersOrderIDReturnPatch(ctx, request, params)
	_ = res
	return res, err
}

func (c *Client) sendPublicReturnOrderPublicV2OrdersOrderIDReturnPatch(ctx context.Context, request *ReturnProductsInRequestV2, params PublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams) (res PublicReturnOrderPublicV2OrdersOrderIDReturnPatchRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_return_order_public_v2_orders__order_id__return__patch"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PublicReturnOrderPublicV2OrdersOrderIDReturnPatch",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [3]string
	pathParts[0] = "/v2/orders/"
	{
		// Encode "order_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "order_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.OrderID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	pathParts[2] = "/return/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "PATCH", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePublicReturnOrderPublicV2OrdersOrderIDReturnPatchRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PublicReturnOrderPublicV2OrdersOrderIDReturnPatch", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePublicReturnOrderPublicV2OrdersOrderIDReturnPatchResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}

// PublicUpdateOrderPublicV1OrdersOrderIDPatch invokes public_update_order_public_v1_orders__order_id___patch operation.
//
// Update order. Undesirable to use to cancel an order.
//
// PATCH /orders/{order_id}/
func (c *Client) PublicUpdateOrderPublicV1OrdersOrderIDPatch(ctx context.Context, request *UpdateOrderInRequest, params PublicUpdateOrderPublicV1OrdersOrderIDPatchParams) (PublicUpdateOrderPublicV1OrdersOrderIDPatchRes, error) {
	res, err := c.sendPublicUpdateOrderPublicV1OrdersOrderIDPatch(ctx, request, params)
	_ = res
	return res, err
}

func (c *Client) sendPublicUpdateOrderPublicV1OrdersOrderIDPatch(ctx context.Context, request *UpdateOrderInRequest, params PublicUpdateOrderPublicV1OrdersOrderIDPatchParams) (res PublicUpdateOrderPublicV1OrdersOrderIDPatchRes, err error) {
	otelAttrs := []attribute.KeyValue{
		otelogen.OperationID("public_update_order_public_v1_orders__order_id___patch"),
	}
	// Validate request before sending.
	if err := func() error {
		if err := request.Validate(); err != nil {
			return err
		}
		return nil
	}(); err != nil {
		return res, errors.Wrap(err, "validate")
	}

	// Run stopwatch.
	startTime := time.Now()
	defer func() {
		elapsedDuration := time.Since(startTime)
		c.duration.Record(ctx, elapsedDuration.Microseconds(), otelAttrs...)
	}()

	// Increment request counter.
	c.requests.Add(ctx, 1, otelAttrs...)

	// Start a span for this request.
	ctx, span := c.cfg.Tracer.Start(ctx, "PublicUpdateOrderPublicV1OrdersOrderIDPatch",
		trace.WithAttributes(otelAttrs...),
		clientSpanKind,
	)
	// Track stage for error reporting.
	var stage string
	defer func() {
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, stage)
			c.errors.Add(ctx, 1, otelAttrs...)
		}
		span.End()
	}()

	stage = "BuildURL"
	u := uri.Clone(c.requestURL(ctx))
	var pathParts [3]string
	pathParts[0] = "/orders/"
	{
		// Encode "order_id" parameter.
		e := uri.NewPathEncoder(uri.PathEncoderConfig{
			Param:   "order_id",
			Style:   uri.PathStyleSimple,
			Explode: false,
		})
		if err := func() error {
			return e.EncodeValue(conv.UUIDToString(params.OrderID))
		}(); err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		encoded, err := e.Result()
		if err != nil {
			return res, errors.Wrap(err, "encode path")
		}
		pathParts[1] = encoded
	}
	pathParts[2] = "/"
	uri.AddPathParts(u, pathParts[:]...)

	stage = "EncodeRequest"
	r, err := ht.NewRequest(ctx, "PATCH", u, nil)
	if err != nil {
		return res, errors.Wrap(err, "create request")
	}
	if err := encodePublicUpdateOrderPublicV1OrdersOrderIDPatchRequest(request, r); err != nil {
		return res, errors.Wrap(err, "encode request")
	}

	{
		type bitset = [1]uint8
		var satisfied bitset
		{
			stage = "Security:APIKeyHeader"
			switch err := c.securityAPIKeyHeader(ctx, "PublicUpdateOrderPublicV1OrdersOrderIDPatch", r); err {
			case nil:
				satisfied[0] |= 1 << 0
			case ogenerrors.ErrSkipClientSecurity:
				// Skip this security.
			default:
				return res, errors.Wrap(err, "security \"APIKeyHeader\"")
			}
		}

		if ok := func() bool {
		nextRequirement:
			for _, requirement := range []bitset{
				{0b00000001},
			} {
				for i, mask := range requirement {
					if satisfied[i]&mask != mask {
						continue nextRequirement
					}
				}
				return true
			}
			return false
		}(); !ok {
			return res, errors.New("no security requirement satisfied")
		}
	}

	stage = "SendRequest"
	resp, err := c.cfg.Client.Do(r)
	if err != nil {
		return res, errors.Wrap(err, "do request")
	}
	defer resp.Body.Close()

	stage = "DecodeResponse"
	result, err := decodePublicUpdateOrderPublicV1OrdersOrderIDPatchResponse(resp)
	if err != nil {
		return res, errors.Wrap(err, "decode response")
	}

	return result, nil
}
