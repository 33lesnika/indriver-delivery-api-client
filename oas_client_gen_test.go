package api

import (
	"context"
	"github.com/google/uuid"
	"github.com/motemen/go-loghttp"
	_ "github.com/motemen/go-loghttp/global"
	"log"
	"net/http"
	"net/http/httputil"
	"testing"
)

func TestDryRunApi(t *testing.T) {
	client, err := getClientWithLogging(t)
	result, err := client.PublicDryRunPublicV1OrdersDryRunPost(context.Background(), &OrderDryRun{
		PlaceID: uuid.New(),
		Cost: OrderCost{
			Cost:           100,
			AdditionalCost: 0,
		},
		PersonCount: 1,
		Comment:     NewOptString("TEST"),
		Products: []CreateProductInRequest{
			{
				Name:        "Лаваш королевский",
				Quantity:    1,
				Price:       100,
				TotalPrice:  100,
				VatCode:     OptString{},
				RkeeperCode: OptString{},
				ExternalID:  OptString{},
				Ingredients: []CreateIngredientInRequest{
					{
						Name:        "Ассорти",
						Price:       0,
						GroupName:   "Тип лаваша",
						VatCode:     OptString{},
						RkeeperCode: OptString{},
						ExternalID:  OptString{},
					},
				},
			},
		},
		AdditionalServices: []AdditionalServiceCodeNameOnly{
			{CodeName: AdditionalServiceCodeNameOnlyCodeNameLeaveAtTheDoor},
		},
		IsForceStart: NewOptBool(false),
		Delivery: DeliveryDryRunInRequest{
			ExpectedTime: OptDateTime{},
			ConsumerData: "",
			Region:       NewOptString("Астана"),
			Street:       "проспект Кабанбай батыра",
			HouseNumber:  NewOptString("111"),
			FlatNumber:   NewOptString("11"),
			Entrance:     NewOptString("1"),
			Intercom:     OptString{},
			Floor:        NewOptString("3"),
			Location:     OptPoint{},
			Comment:      NewOptString("Test"),
			Attributes:   OptDeliveryAttributes{},
		},
	})
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	t.Logf("%+v\n", result)
}

func TestCreateOrder(t *testing.T) {
	client, err := getClientWithLogging(t)
	result, err := client.PublicCreateOrderPublicV1OrdersPost(context.Background(), &CreateOrderInRequest{
		PlaceID: uuid.New(),
		Cost: OrderCost{
			Cost:           100,
			AdditionalCost: 0,
		},
		PersonCount: 1,
		Comment:     NewOptString("TEST"),
		Products: []CreateProductInRequest{
			{
				Name:        "Лаваш королевский",
				Quantity:    1,
				Price:       100,
				TotalPrice:  100,
				VatCode:     OptString{},
				RkeeperCode: OptString{},
				ExternalID:  OptString{},
				Ingredients: []CreateIngredientInRequest{
					{
						Name:        "Ассорти",
						Price:       0,
						GroupName:   "Тип лаваша",
						VatCode:     OptString{},
						RkeeperCode: OptString{},
						ExternalID:  OptString{},
					},
				},
			},
		},
		AdditionalServices: []AdditionalServiceCodeNameOnly{
			{CodeName: AdditionalServiceCodeNameOnlyCodeNameLeaveAtTheDoor},
		},
		IsForceStart: NewOptBool(false),
		Delivery: CreateDeliveryInRequest{
			ExpectedTime:  OptDateTime{},
			PaymentMethod: Cash,
			ConsumerData:  "",
			Region:        "Астана",
			Street:        "проспект Кабанбай батыра",
			HouseNumber:   NewOptString("111"),
			FlatNumber:    NewOptString("11"),
			Entrance:      NewOptString("1"),
			Intercom:      OptString{},
			Floor:         NewOptString("3"),
			Location:      OptPoint{},
			Comment:       NewOptString("Test"),
			Attributes:    OptDeliveryAttributes{},
		},
	})
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	switch p := result.(type) {
	case *PublicOrderDetailForResponse:
		// Success response, we can access order struct fields
		t.Logf("Order status: %v\n", p.Status)
	case *PublicCreateOrderPublicV1OrdersPostForbidden:
		// Error response, we cannot read order
		t.Fatalf("Response is PublicCreateOrderPublicV1OrdersPostForbidden type")
	default:
		// we got some unknown type, should never happen
		t.Fatalf("Unknown response")
	}
	t.Logf("%+v\n", result)
}

func getClientWithLogging(t *testing.T) (*Client, error) {
	transportWithLogging := &loghttp.Transport{
		LogRequest: func(req *http.Request) {
			reqDump, err := httputil.DumpRequestOut(req, true)
			if err == nil {
				log.Printf("REQUEST:\n%s\n", string(reqDump))
			}
		},
		LogResponse: func(resp *http.Response) {
			respDump, err := httputil.DumpResponse(resp, true)
			if err == nil {
				log.Printf("RESPONSE:\n%s\n", string(respDump))
			}
		},
	}
	client, err := NewClient("https://api.masterdelivery.ru/public/v1",
		StaticHeaderTokenSecuritySource{Header: "Bearer SuperSecretTokenFromIndriverSupport"},
		WithClient(&http.Client{Transport: transportWithLogging}),
	)
	if err != nil {
		t.Fatalf("%+v\n", err)
	}
	return client, err
}
