package api

import (
	"context"
	"net/http"
	"strings"

	"github.com/go-faster/errors"

	"github.com/ogen-go/ogen/ogenerrors"
)

// SecurityHandler is handler for security parameters.
type SecurityHandler interface {
	// HandleAPIKeyHeader handles APIKeyHeader security.
	HandleAPIKeyHeader(ctx context.Context, operationName string, t APIKeyHeader) (context.Context, error)
}

func findAuthorization(h http.Header, prefix string) (string, bool) {
	v, ok := h["Authorization"]
	if !ok {
		return "", false
	}
	for _, vv := range v {
		scheme, value, ok := strings.Cut(vv, " ")
		if !ok || !strings.EqualFold(scheme, prefix) {
			continue
		}
		return value, true
	}
	return "", false
}

func (s *Server) securityAPIKeyHeader(ctx context.Context, operationName string, req *http.Request) (context.Context, bool, error) {
	var t APIKeyHeader
	const parameterName = "Authorization"
	value := req.Header.Get(parameterName)
	if value == "" {
		return ctx, false, nil
	}
	t.APIKey = value
	rctx, err := s.sec.HandleAPIKeyHeader(ctx, operationName, t)
	if err != nil {
		return nil, false, err
	}
	return rctx, true, err
}

// SecuritySource is provider of security values (tokens, passwords, etc.).
type SecuritySource interface {
	// APIKeyHeader provides APIKeyHeader security value.
	APIKeyHeader(ctx context.Context, operationName string) (APIKeyHeader, error)
}

type StaticHeaderTokenSecuritySource struct {
	Header string
}

func (source StaticHeaderTokenSecuritySource) APIKeyHeader(ctx context.Context, operationName string) (APIKeyHeader, error) {
	return APIKeyHeader{source.Header}, nil
}

func (s *Client) securityAPIKeyHeader(ctx context.Context, operationName string, req *http.Request) error {
	t, err := s.sec.APIKeyHeader(ctx, operationName)
	if err != nil {
		if errors.Is(err, ogenerrors.ErrSkipClientSecurity) {
			return ogenerrors.ErrSkipClientSecurity
		}
		return errors.Wrap(err, "security source \"APIKeyHeader\"")
	}
	req.Header.Set("Authorization", t.APIKey)
	return nil
}
