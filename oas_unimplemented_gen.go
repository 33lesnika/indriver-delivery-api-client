package api

import (
	"context"

	ht "github.com/ogen-go/ogen/http"
)

// UnimplementedHandler is no-op Handler which returns http.ErrNotImplemented.
type UnimplementedHandler struct{}

var _ Handler = UnimplementedHandler{}

// MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost implements POST /merchant_courier_near_delivery_address_callback_url/{order_id} operation.
//
// Notifies that the courier is located near the delivery address.
//
// POST /merchant_courier_near_delivery_address_callback_url/{order_id}
func (UnimplementedHandler) MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost(ctx context.Context, params MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams) error {
	return ht.ErrNotImplemented
}

// MerchantCourierNearPlaceCallbackURLOrderIDPost implements POST /merchant_courier_near_place_callback_url/{order_id} operation.
//
// Notifies that the courier is located near the place.
//
// POST /merchant_courier_near_place_callback_url/{order_id}
func (UnimplementedHandler) MerchantCourierNearPlaceCallbackURLOrderIDPost(ctx context.Context, params MerchantCourierNearPlaceCallbackURLOrderIDPostParams) error {
	return ht.ErrNotImplemented
}

// MerchantOrderStatusCallbackURLOrderIDPost implements POST /merchant_order_status_callback_url/{order_id} operation.
//
// Notifies about order's status update or about delivery delaying.
//
// POST /merchant_order_status_callback_url/{order_id}
func (UnimplementedHandler) MerchantOrderStatusCallbackURLOrderIDPost(ctx context.Context, req OptOrderStatusCallback, params MerchantOrderStatusCallbackURLOrderIDPostParams) error {
	return ht.ErrNotImplemented
}

// MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet implements merchant_schedule_get_public_v1_merchant__merchant_id__schedule_get operation.
//
// Merchant Schedule:Get.
//
// GET /merchant/{merchant_id}/schedule
func (UnimplementedHandler) MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGet(ctx context.Context, params MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams) (r MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRes, _ error) {
	return r, ht.ErrNotImplemented
}

// MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut implements merchant_schedule_update_public_v1_merchant__merchant_id__schedule_put operation.
//
// Merchant Schedule:Update.
//
// PUT /merchant/{merchant_id}/schedule
func (UnimplementedHandler) MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePut(ctx context.Context, req *WorkTimesUpdate, params MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams) (r MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet implements place_schedule_get_public_v1_merchant_places__place_id__schedule_get operation.
//
// Place Schedule:Get.
//
// GET /merchant/places/{place_id}/schedule
func (UnimplementedHandler) PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGet(ctx context.Context, params PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams) (r PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut implements place_schedule_update_public_v1_merchant_places__place_id__schedule_put operation.
//
// Place Schedule:Update.
//
// PUT /merchant/places/{place_id}/schedule
func (UnimplementedHandler) PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePut(ctx context.Context, req *WorkTimesUpdate, params PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams) (r PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PlacesBlockPublicV1MerchantPlacesBlockPost implements places_block_public_v1_merchant_places_block__post operation.
//
// Blocks places for the specified period or endless. Use if you want to temporally stop orders
// creating for one or list of places.
//
// POST /merchant/places/block/
func (UnimplementedHandler) PlacesBlockPublicV1MerchantPlacesBlockPost(ctx context.Context, req *PlaceBlockInRequest) (r PlacesBlockPublicV1MerchantPlacesBlockPostRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PlacesBlocksPublicV1MerchantPlacesBlocksGet implements places_blocks_public_v1_merchant_places_blocks__get operation.
//
// Returns list of active places blocks.
//
// GET /merchant/places/blocks/
func (UnimplementedHandler) PlacesBlocksPublicV1MerchantPlacesBlocksGet(ctx context.Context, params PlacesBlocksPublicV1MerchantPlacesBlocksGetParams) (r PlacesBlocksPublicV1MerchantPlacesBlocksGetRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PlacesCreatePublicV1MerchantPlacesPost implements places_create_public_v1_merchant_places_post operation.
//
// Places:Create.
//
// POST /merchant/places
func (UnimplementedHandler) PlacesCreatePublicV1MerchantPlacesPost(ctx context.Context, req *PlaceWithArea) (r PlacesCreatePublicV1MerchantPlacesPostRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PlacesListPublicV1MerchantPlacesGet implements places_list_public_v1_merchant_places_get operation.
//
// Places:List.
//
// GET /merchant/places
func (UnimplementedHandler) PlacesListPublicV1MerchantPlacesGet(ctx context.Context) (r PlacesListPublicV1MerchantPlacesGetRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost implements places_order_check_public_v1_merchant_places_order_check_post operation.
//
// Check whether delivery from place is possible to adress.
//
// POST /merchant/places/order-check
func (UnimplementedHandler) PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPost(ctx context.Context, req *OrderCheckInRequest) (r PlacesOrderCheckPublicV1MerchantPlacesOrderCheckPostRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PlacesUnblockPublicV1MerchantPlacesUnblockPost implements places_unblock_public_v1_merchant_places_unblock__post operation.
//
// Unblocks places.
//
// POST /merchant/places/unblock/
func (UnimplementedHandler) PlacesUnblockPublicV1MerchantPlacesUnblockPost(ctx context.Context, req *PlaceUnBlockInRequest) (r PlacesUnblockPublicV1MerchantPlacesUnblockPostRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PlacesUpdatePublicV1MerchantPlacesPlaceIDPut implements places_update_public_v1_merchant_places__place_id__put operation.
//
// Places:Update.
//
// PUT /merchant/places/{place_id}
func (UnimplementedHandler) PlacesUpdatePublicV1MerchantPlacesPlaceIDPut(ctx context.Context, req *PlaceUpdate, params PlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams) (r PlacesUpdatePublicV1MerchantPlacesPlaceIDPutRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PublicCancelOrderPublicV1OrdersOrderIDDelete implements public_cancel_order_public_v1_orders__order_id___delete operation.
//
// Cancel order.
//
// DELETE /orders/{order_id}/
func (UnimplementedHandler) PublicCancelOrderPublicV1OrdersOrderIDDelete(ctx context.Context, req *CancelOrderInRequest, params PublicCancelOrderPublicV1OrdersOrderIDDeleteParams) (r PublicCancelOrderPublicV1OrdersOrderIDDeleteRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PublicCreateOrderPublicV1OrdersPost implements public_create_order_public_v1_orders__post operation.
//
// Create order to delivery.
//
// POST /orders/
func (UnimplementedHandler) PublicCreateOrderPublicV1OrdersPost(ctx context.Context, req *CreateOrderInRequest) (r PublicCreateOrderPublicV1OrdersPostRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PublicDryRunPublicV1OrdersDryRunPost implements public_dry_run_public_v1_orders_dry_run__post operation.
//
// Check whether order can be created.
//
// POST /orders/dry_run/
func (UnimplementedHandler) PublicDryRunPublicV1OrdersDryRunPost(ctx context.Context, req *OrderDryRun) (r PublicDryRunPublicV1OrdersDryRunPostRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet implements public_get_courier_position_public_v1_orders__order_id__tracking__get operation.
//
// Public:Get Courier Position.
//
// GET /orders/{order_id}/tracking/
func (UnimplementedHandler) PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGet(ctx context.Context, params PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams) (r PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet implements public_get_order_photos_public_v1_orders__order_id__photos__get operation.
//
// Getting photos of the order.
//
// GET /orders/{order_id}/photos/
func (UnimplementedHandler) PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGet(ctx context.Context, params PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams) (r PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PublicGetOrderPublicV1OrdersOrderIDGet implements public_get_order_public_v1_orders__order_id___get operation.
//
// Get order info.
//
// GET /orders/{order_id}/
func (UnimplementedHandler) PublicGetOrderPublicV1OrdersOrderIDGet(ctx context.Context, params PublicGetOrderPublicV1OrdersOrderIDGetParams) (r PublicGetOrderPublicV1OrdersOrderIDGetRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PublicReturnOrderPublicV1OrdersOrderIDReturnPatch implements public_return_order_public_v1_orders__order_id__return__patch operation.
//
// Return products of order (deprecated).
//
// PATCH /orders/{order_id}/return/
func (UnimplementedHandler) PublicReturnOrderPublicV1OrdersOrderIDReturnPatch(ctx context.Context, req *ReturnProductsInRequest, params PublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams) (r PublicReturnOrderPublicV1OrdersOrderIDReturnPatchRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PublicReturnOrderPublicV2OrdersOrderIDReturnPatch implements public_return_order_public_v2_orders__order_id__return__patch operation.
//
// Return products of order.
//
// PATCH /v2/orders/{order_id}/return/
func (UnimplementedHandler) PublicReturnOrderPublicV2OrdersOrderIDReturnPatch(ctx context.Context, req *ReturnProductsInRequestV2, params PublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams) (r PublicReturnOrderPublicV2OrdersOrderIDReturnPatchRes, _ error) {
	return r, ht.ErrNotImplemented
}

// PublicUpdateOrderPublicV1OrdersOrderIDPatch implements public_update_order_public_v1_orders__order_id___patch operation.
//
// Update order. Undesirable to use to cancel an order.
//
// PATCH /orders/{order_id}/
func (UnimplementedHandler) PublicUpdateOrderPublicV1OrdersOrderIDPatch(ctx context.Context, req *UpdateOrderInRequest, params PublicUpdateOrderPublicV1OrdersOrderIDPatchParams) (r PublicUpdateOrderPublicV1OrdersOrderIDPatchRes, _ error) {
	return r, ht.ErrNotImplemented
}
