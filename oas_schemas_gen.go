package api

import (
	"time"

	"github.com/go-faster/errors"
	"github.com/go-faster/jx"
	"github.com/google/uuid"
)

type APIKeyHeader struct {
	APIKey string
}

// GetAPIKey returns the value of APIKey.
func (s *APIKeyHeader) GetAPIKey() string {
	return s.APIKey
}

// SetAPIKey sets the value of APIKey.
func (s *APIKeyHeader) SetAPIKey(val string) {
	s.APIKey = val
}

// Ref: #/components/schemas/AdditionalServiceCodeNameOnly
type AdditionalServiceCodeNameOnly struct {
	CodeName AdditionalServiceCodeNameOnlyCodeName `json:"code_name"`
}

// GetCodeName returns the value of CodeName.
func (s *AdditionalServiceCodeNameOnly) GetCodeName() AdditionalServiceCodeNameOnlyCodeName {
	return s.CodeName
}

// SetCodeName sets the value of CodeName.
func (s *AdditionalServiceCodeNameOnly) SetCodeName(val AdditionalServiceCodeNameOnlyCodeName) {
	s.CodeName = val
}

type AdditionalServiceCodeNameOnlyCodeName string

const (
	AdditionalServiceCodeNameOnlyCodeNameLeaveAtTheDoor AdditionalServiceCodeNameOnlyCodeName = "leave_at_the_door"
	AdditionalServiceCodeNameOnlyCodeNameAdult          AdditionalServiceCodeNameOnlyCodeName = "adult"
	AdditionalServiceCodeNameOnlyCodeNameNotCallMe      AdditionalServiceCodeNameOnlyCodeName = "not_call_me"
)

// MarshalText implements encoding.TextMarshaler.
func (s AdditionalServiceCodeNameOnlyCodeName) MarshalText() ([]byte, error) {
	switch s {
	case AdditionalServiceCodeNameOnlyCodeNameLeaveAtTheDoor:
		return []byte(s), nil
	case AdditionalServiceCodeNameOnlyCodeNameAdult:
		return []byte(s), nil
	case AdditionalServiceCodeNameOnlyCodeNameNotCallMe:
		return []byte(s), nil
	default:
		return nil, errors.Errorf("invalid value: %q", s)
	}
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (s *AdditionalServiceCodeNameOnlyCodeName) UnmarshalText(data []byte) error {
	switch AdditionalServiceCodeNameOnlyCodeName(data) {
	case AdditionalServiceCodeNameOnlyCodeNameLeaveAtTheDoor:
		*s = AdditionalServiceCodeNameOnlyCodeNameLeaveAtTheDoor
		return nil
	case AdditionalServiceCodeNameOnlyCodeNameAdult:
		*s = AdditionalServiceCodeNameOnlyCodeNameAdult
		return nil
	case AdditionalServiceCodeNameOnlyCodeNameNotCallMe:
		*s = AdditionalServiceCodeNameOnlyCodeNameNotCallMe
		return nil
	default:
		return errors.Errorf("invalid value: %q", data)
	}
}

type AdditionalServiceInDB jx.Raw

// Ref: #/components/schemas/AreaCreateWithoutPlace
type AreaCreateWithoutPlace struct {
	MinDeliveryDuration int `json:"min_delivery_duration"`
	MaxDeliveryDuration int `json:"max_delivery_duration"`
	// Kml as geojson.
	Geometry JsonGeometry `json:"geometry"`
}

// GetMinDeliveryDuration returns the value of MinDeliveryDuration.
func (s *AreaCreateWithoutPlace) GetMinDeliveryDuration() int {
	return s.MinDeliveryDuration
}

// GetMaxDeliveryDuration returns the value of MaxDeliveryDuration.
func (s *AreaCreateWithoutPlace) GetMaxDeliveryDuration() int {
	return s.MaxDeliveryDuration
}

// GetGeometry returns the value of Geometry.
func (s *AreaCreateWithoutPlace) GetGeometry() JsonGeometry {
	return s.Geometry
}

// SetMinDeliveryDuration sets the value of MinDeliveryDuration.
func (s *AreaCreateWithoutPlace) SetMinDeliveryDuration(val int) {
	s.MinDeliveryDuration = val
}

// SetMaxDeliveryDuration sets the value of MaxDeliveryDuration.
func (s *AreaCreateWithoutPlace) SetMaxDeliveryDuration(val int) {
	s.MaxDeliveryDuration = val
}

// SetGeometry sets the value of Geometry.
func (s *AreaCreateWithoutPlace) SetGeometry(val JsonGeometry) {
	s.Geometry = val
}

// Ref: #/components/schemas/BaseSchemaResponse
type BaseSchemaResponse struct{}

func (*BaseSchemaResponse) publicDryRunPublicV1OrdersDryRunPostRes() {}

// Cancel the order.
// Ref: #/components/schemas/CancelOrderInRequest
type CancelOrderInRequest struct {
	CancelCode   OrderCancelCodeEnum `json:"cancel_code"`
	CancelReason OptString           `json:"cancel_reason"`
}

// GetCancelCode returns the value of CancelCode.
func (s *CancelOrderInRequest) GetCancelCode() OrderCancelCodeEnum {
	return s.CancelCode
}

// GetCancelReason returns the value of CancelReason.
func (s *CancelOrderInRequest) GetCancelReason() OptString {
	return s.CancelReason
}

// SetCancelCode sets the value of CancelCode.
func (s *CancelOrderInRequest) SetCancelCode(val OrderCancelCodeEnum) {
	s.CancelCode = val
}

// SetCancelReason sets the value of CancelReason.
func (s *CancelOrderInRequest) SetCancelReason(val OptString) {
	s.CancelReason = val
}

// Ref: #/components/schemas/CreateDeliveryInRequest
type CreateDeliveryInRequest struct {
	ExpectedTime  OptDateTime           `json:"expected_time"`
	PaymentMethod PaymentMethodEnum     `json:"payment_method"`
	ConsumerData  string                `json:"consumer_data"`
	Region        string                `json:"region"`
	Street        string                `json:"street"`
	HouseNumber   OptString             `json:"house_number"`
	FlatNumber    OptString             `json:"flat_number"`
	Entrance      OptString             `json:"entrance"`
	Intercom      OptString             `json:"intercom"`
	Floor         OptString             `json:"floor"`
	Location      OptPoint              `json:"location"`
	Comment       OptString             `json:"comment"`
	Attributes    OptDeliveryAttributes `json:"attributes"`
}

// GetExpectedTime returns the value of ExpectedTime.
func (s *CreateDeliveryInRequest) GetExpectedTime() OptDateTime {
	return s.ExpectedTime
}

// GetPaymentMethod returns the value of PaymentMethod.
func (s *CreateDeliveryInRequest) GetPaymentMethod() PaymentMethodEnum {
	return s.PaymentMethod
}

// GetConsumerData returns the value of ConsumerData.
func (s *CreateDeliveryInRequest) GetConsumerData() string {
	return s.ConsumerData
}

// GetRegion returns the value of Region.
func (s *CreateDeliveryInRequest) GetRegion() string {
	return s.Region
}

// GetStreet returns the value of Street.
func (s *CreateDeliveryInRequest) GetStreet() string {
	return s.Street
}

// GetHouseNumber returns the value of HouseNumber.
func (s *CreateDeliveryInRequest) GetHouseNumber() OptString {
	return s.HouseNumber
}

// GetFlatNumber returns the value of FlatNumber.
func (s *CreateDeliveryInRequest) GetFlatNumber() OptString {
	return s.FlatNumber
}

// GetEntrance returns the value of Entrance.
func (s *CreateDeliveryInRequest) GetEntrance() OptString {
	return s.Entrance
}

// GetIntercom returns the value of Intercom.
func (s *CreateDeliveryInRequest) GetIntercom() OptString {
	return s.Intercom
}

// GetFloor returns the value of Floor.
func (s *CreateDeliveryInRequest) GetFloor() OptString {
	return s.Floor
}

// GetLocation returns the value of Location.
func (s *CreateDeliveryInRequest) GetLocation() OptPoint {
	return s.Location
}

// GetComment returns the value of Comment.
func (s *CreateDeliveryInRequest) GetComment() OptString {
	return s.Comment
}

// GetAttributes returns the value of Attributes.
func (s *CreateDeliveryInRequest) GetAttributes() OptDeliveryAttributes {
	return s.Attributes
}

// SetExpectedTime sets the value of ExpectedTime.
func (s *CreateDeliveryInRequest) SetExpectedTime(val OptDateTime) {
	s.ExpectedTime = val
}

// SetPaymentMethod sets the value of PaymentMethod.
func (s *CreateDeliveryInRequest) SetPaymentMethod(val PaymentMethodEnum) {
	s.PaymentMethod = val
}

// SetConsumerData sets the value of ConsumerData.
func (s *CreateDeliveryInRequest) SetConsumerData(val string) {
	s.ConsumerData = val
}

// SetRegion sets the value of Region.
func (s *CreateDeliveryInRequest) SetRegion(val string) {
	s.Region = val
}

// SetStreet sets the value of Street.
func (s *CreateDeliveryInRequest) SetStreet(val string) {
	s.Street = val
}

// SetHouseNumber sets the value of HouseNumber.
func (s *CreateDeliveryInRequest) SetHouseNumber(val OptString) {
	s.HouseNumber = val
}

// SetFlatNumber sets the value of FlatNumber.
func (s *CreateDeliveryInRequest) SetFlatNumber(val OptString) {
	s.FlatNumber = val
}

// SetEntrance sets the value of Entrance.
func (s *CreateDeliveryInRequest) SetEntrance(val OptString) {
	s.Entrance = val
}

// SetIntercom sets the value of Intercom.
func (s *CreateDeliveryInRequest) SetIntercom(val OptString) {
	s.Intercom = val
}

// SetFloor sets the value of Floor.
func (s *CreateDeliveryInRequest) SetFloor(val OptString) {
	s.Floor = val
}

// SetLocation sets the value of Location.
func (s *CreateDeliveryInRequest) SetLocation(val OptPoint) {
	s.Location = val
}

// SetComment sets the value of Comment.
func (s *CreateDeliveryInRequest) SetComment(val OptString) {
	s.Comment = val
}

// SetAttributes sets the value of Attributes.
func (s *CreateDeliveryInRequest) SetAttributes(val OptDeliveryAttributes) {
	s.Attributes = val
}

// Ref: #/components/schemas/CreateIngredientInRequest
type CreateIngredientInRequest struct {
	Name        string    `json:"name"`
	Price       float64   `json:"price"`
	GroupName   string    `json:"group_name"`
	VatCode     OptString `json:"vat_code"`
	RkeeperCode OptString `json:"rkeeper_code"`
	ExternalID  OptString `json:"external_id"`
}

// GetName returns the value of Name.
func (s *CreateIngredientInRequest) GetName() string {
	return s.Name
}

// GetPrice returns the value of Price.
func (s *CreateIngredientInRequest) GetPrice() float64 {
	return s.Price
}

// GetGroupName returns the value of GroupName.
func (s *CreateIngredientInRequest) GetGroupName() string {
	return s.GroupName
}

// GetVatCode returns the value of VatCode.
func (s *CreateIngredientInRequest) GetVatCode() OptString {
	return s.VatCode
}

// GetRkeeperCode returns the value of RkeeperCode.
func (s *CreateIngredientInRequest) GetRkeeperCode() OptString {
	return s.RkeeperCode
}

// GetExternalID returns the value of ExternalID.
func (s *CreateIngredientInRequest) GetExternalID() OptString {
	return s.ExternalID
}

// SetName sets the value of Name.
func (s *CreateIngredientInRequest) SetName(val string) {
	s.Name = val
}

// SetPrice sets the value of Price.
func (s *CreateIngredientInRequest) SetPrice(val float64) {
	s.Price = val
}

// SetGroupName sets the value of GroupName.
func (s *CreateIngredientInRequest) SetGroupName(val string) {
	s.GroupName = val
}

// SetVatCode sets the value of VatCode.
func (s *CreateIngredientInRequest) SetVatCode(val OptString) {
	s.VatCode = val
}

// SetRkeeperCode sets the value of RkeeperCode.
func (s *CreateIngredientInRequest) SetRkeeperCode(val OptString) {
	s.RkeeperCode = val
}

// SetExternalID sets the value of ExternalID.
func (s *CreateIngredientInRequest) SetExternalID(val OptString) {
	s.ExternalID = val
}

// Ref: #/components/schemas/CreateOrderInRequest
type CreateOrderInRequest struct {
	PlaceID            uuid.UUID                       `json:"place_id"`
	Cost               OrderCost                       `json:"cost"`
	PersonCount        int                             `json:"person_count"`
	Comment            OptString                       `json:"comment"`
	Products           []CreateProductInRequest        `json:"products"`
	AdditionalServices []AdditionalServiceCodeNameOnly `json:"additional_services"`
	IsForceStart       OptBool                         `json:"is_force_start"`
	OrderID            string                          `json:"order_id"`
	Customer           OrderCustomer                   `json:"customer"`
	Delivery           CreateDeliveryInRequest         `json:"delivery"`
}

// GetPlaceID returns the value of PlaceID.
func (s *CreateOrderInRequest) GetPlaceID() uuid.UUID {
	return s.PlaceID
}

// GetCost returns the value of Cost.
func (s *CreateOrderInRequest) GetCost() OrderCost {
	return s.Cost
}

// GetPersonCount returns the value of PersonCount.
func (s *CreateOrderInRequest) GetPersonCount() int {
	return s.PersonCount
}

// GetComment returns the value of Comment.
func (s *CreateOrderInRequest) GetComment() OptString {
	return s.Comment
}

// GetProducts returns the value of Products.
func (s *CreateOrderInRequest) GetProducts() []CreateProductInRequest {
	return s.Products
}

// GetAdditionalServices returns the value of AdditionalServices.
func (s *CreateOrderInRequest) GetAdditionalServices() []AdditionalServiceCodeNameOnly {
	return s.AdditionalServices
}

// GetIsForceStart returns the value of IsForceStart.
func (s *CreateOrderInRequest) GetIsForceStart() OptBool {
	return s.IsForceStart
}

// GetOrderID returns the value of OrderID.
func (s *CreateOrderInRequest) GetOrderID() string {
	return s.OrderID
}

// GetCustomer returns the value of Customer.
func (s *CreateOrderInRequest) GetCustomer() OrderCustomer {
	return s.Customer
}

// GetDelivery returns the value of Delivery.
func (s *CreateOrderInRequest) GetDelivery() CreateDeliveryInRequest {
	return s.Delivery
}

// SetPlaceID sets the value of PlaceID.
func (s *CreateOrderInRequest) SetPlaceID(val uuid.UUID) {
	s.PlaceID = val
}

// SetCost sets the value of Cost.
func (s *CreateOrderInRequest) SetCost(val OrderCost) {
	s.Cost = val
}

// SetPersonCount sets the value of PersonCount.
func (s *CreateOrderInRequest) SetPersonCount(val int) {
	s.PersonCount = val
}

// SetComment sets the value of Comment.
func (s *CreateOrderInRequest) SetComment(val OptString) {
	s.Comment = val
}

// SetProducts sets the value of Products.
func (s *CreateOrderInRequest) SetProducts(val []CreateProductInRequest) {
	s.Products = val
}

// SetAdditionalServices sets the value of AdditionalServices.
func (s *CreateOrderInRequest) SetAdditionalServices(val []AdditionalServiceCodeNameOnly) {
	s.AdditionalServices = val
}

// SetIsForceStart sets the value of IsForceStart.
func (s *CreateOrderInRequest) SetIsForceStart(val OptBool) {
	s.IsForceStart = val
}

// SetOrderID sets the value of OrderID.
func (s *CreateOrderInRequest) SetOrderID(val string) {
	s.OrderID = val
}

// SetCustomer sets the value of Customer.
func (s *CreateOrderInRequest) SetCustomer(val OrderCustomer) {
	s.Customer = val
}

// SetDelivery sets the value of Delivery.
func (s *CreateOrderInRequest) SetDelivery(val CreateDeliveryInRequest) {
	s.Delivery = val
}

// Ref: #/components/schemas/CreateProductInRequest
type CreateProductInRequest struct {
	Name        string                      `json:"name"`
	Quantity    int                         `json:"quantity"`
	Price       float64                     `json:"price"`
	TotalPrice  float64                     `json:"total_price"`
	VatCode     OptString                   `json:"vat_code"`
	RkeeperCode OptString                   `json:"rkeeper_code"`
	ExternalID  OptString                   `json:"external_id"`
	Ingredients []CreateIngredientInRequest `json:"ingredients"`
}

// GetName returns the value of Name.
func (s *CreateProductInRequest) GetName() string {
	return s.Name
}

// GetQuantity returns the value of Quantity.
func (s *CreateProductInRequest) GetQuantity() int {
	return s.Quantity
}

// GetPrice returns the value of Price.
func (s *CreateProductInRequest) GetPrice() float64 {
	return s.Price
}

// GetTotalPrice returns the value of TotalPrice.
func (s *CreateProductInRequest) GetTotalPrice() float64 {
	return s.TotalPrice
}

// GetVatCode returns the value of VatCode.
func (s *CreateProductInRequest) GetVatCode() OptString {
	return s.VatCode
}

// GetRkeeperCode returns the value of RkeeperCode.
func (s *CreateProductInRequest) GetRkeeperCode() OptString {
	return s.RkeeperCode
}

// GetExternalID returns the value of ExternalID.
func (s *CreateProductInRequest) GetExternalID() OptString {
	return s.ExternalID
}

// GetIngredients returns the value of Ingredients.
func (s *CreateProductInRequest) GetIngredients() []CreateIngredientInRequest {
	return s.Ingredients
}

// SetName sets the value of Name.
func (s *CreateProductInRequest) SetName(val string) {
	s.Name = val
}

// SetQuantity sets the value of Quantity.
func (s *CreateProductInRequest) SetQuantity(val int) {
	s.Quantity = val
}

// SetPrice sets the value of Price.
func (s *CreateProductInRequest) SetPrice(val float64) {
	s.Price = val
}

// SetTotalPrice sets the value of TotalPrice.
func (s *CreateProductInRequest) SetTotalPrice(val float64) {
	s.TotalPrice = val
}

// SetVatCode sets the value of VatCode.
func (s *CreateProductInRequest) SetVatCode(val OptString) {
	s.VatCode = val
}

// SetRkeeperCode sets the value of RkeeperCode.
func (s *CreateProductInRequest) SetRkeeperCode(val OptString) {
	s.RkeeperCode = val
}

// SetExternalID sets the value of ExternalID.
func (s *CreateProductInRequest) SetExternalID(val OptString) {
	s.ExternalID = val
}

// SetIngredients sets the value of Ingredients.
func (s *CreateProductInRequest) SetIngredients(val []CreateIngredientInRequest) {
	s.Ingredients = val
}

// Ref: #/components/schemas/db__models__base__Point
type DbModelsBasePoint struct {
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
}

// GetLongitude returns the value of Longitude.
func (s *DbModelsBasePoint) GetLongitude() float64 {
	return s.Longitude
}

// GetLatitude returns the value of Latitude.
func (s *DbModelsBasePoint) GetLatitude() float64 {
	return s.Latitude
}

// SetLongitude sets the value of Longitude.
func (s *DbModelsBasePoint) SetLongitude(val float64) {
	s.Longitude = val
}

// SetLatitude sets the value of Latitude.
func (s *DbModelsBasePoint) SetLatitude(val float64) {
	s.Latitude = val
}

// Ref: #/components/schemas/DeliveryAttributes
type DeliveryAttributes struct {
	ShortCode         OptString `json:"short_code"`
	PinCode           OptString `json:"pin_code"`
	Channel           OptString `json:"channel"`
	BankTransactionID OptString `json:"bank_transaction_id"`
}

// GetShortCode returns the value of ShortCode.
func (s *DeliveryAttributes) GetShortCode() OptString {
	return s.ShortCode
}

// GetPinCode returns the value of PinCode.
func (s *DeliveryAttributes) GetPinCode() OptString {
	return s.PinCode
}

// GetChannel returns the value of Channel.
func (s *DeliveryAttributes) GetChannel() OptString {
	return s.Channel
}

// GetBankTransactionID returns the value of BankTransactionID.
func (s *DeliveryAttributes) GetBankTransactionID() OptString {
	return s.BankTransactionID
}

// SetShortCode sets the value of ShortCode.
func (s *DeliveryAttributes) SetShortCode(val OptString) {
	s.ShortCode = val
}

// SetPinCode sets the value of PinCode.
func (s *DeliveryAttributes) SetPinCode(val OptString) {
	s.PinCode = val
}

// SetChannel sets the value of Channel.
func (s *DeliveryAttributes) SetChannel(val OptString) {
	s.Channel = val
}

// SetBankTransactionID sets the value of BankTransactionID.
func (s *DeliveryAttributes) SetBankTransactionID(val OptString) {
	s.BankTransactionID = val
}

// Ref: #/components/schemas/DeliveryDryRunInRequest
type DeliveryDryRunInRequest struct {
	ExpectedTime OptDateTime           `json:"expected_time"`
	ConsumerData string                `json:"consumer_data"`
	Region       OptString             `json:"region"`
	Street       string                `json:"street"`
	HouseNumber  OptString             `json:"house_number"`
	FlatNumber   OptString             `json:"flat_number"`
	Entrance     OptString             `json:"entrance"`
	Intercom     OptString             `json:"intercom"`
	Floor        OptString             `json:"floor"`
	Location     OptPoint              `json:"location"`
	Comment      OptString             `json:"comment"`
	Attributes   OptDeliveryAttributes `json:"attributes"`
}

// GetExpectedTime returns the value of ExpectedTime.
func (s *DeliveryDryRunInRequest) GetExpectedTime() OptDateTime {
	return s.ExpectedTime
}

// GetConsumerData returns the value of ConsumerData.
func (s *DeliveryDryRunInRequest) GetConsumerData() string {
	return s.ConsumerData
}

// GetRegion returns the value of Region.
func (s *DeliveryDryRunInRequest) GetRegion() OptString {
	return s.Region
}

// GetStreet returns the value of Street.
func (s *DeliveryDryRunInRequest) GetStreet() string {
	return s.Street
}

// GetHouseNumber returns the value of HouseNumber.
func (s *DeliveryDryRunInRequest) GetHouseNumber() OptString {
	return s.HouseNumber
}

// GetFlatNumber returns the value of FlatNumber.
func (s *DeliveryDryRunInRequest) GetFlatNumber() OptString {
	return s.FlatNumber
}

// GetEntrance returns the value of Entrance.
func (s *DeliveryDryRunInRequest) GetEntrance() OptString {
	return s.Entrance
}

// GetIntercom returns the value of Intercom.
func (s *DeliveryDryRunInRequest) GetIntercom() OptString {
	return s.Intercom
}

// GetFloor returns the value of Floor.
func (s *DeliveryDryRunInRequest) GetFloor() OptString {
	return s.Floor
}

// GetLocation returns the value of Location.
func (s *DeliveryDryRunInRequest) GetLocation() OptPoint {
	return s.Location
}

// GetComment returns the value of Comment.
func (s *DeliveryDryRunInRequest) GetComment() OptString {
	return s.Comment
}

// GetAttributes returns the value of Attributes.
func (s *DeliveryDryRunInRequest) GetAttributes() OptDeliveryAttributes {
	return s.Attributes
}

// SetExpectedTime sets the value of ExpectedTime.
func (s *DeliveryDryRunInRequest) SetExpectedTime(val OptDateTime) {
	s.ExpectedTime = val
}

// SetConsumerData sets the value of ConsumerData.
func (s *DeliveryDryRunInRequest) SetConsumerData(val string) {
	s.ConsumerData = val
}

// SetRegion sets the value of Region.
func (s *DeliveryDryRunInRequest) SetRegion(val OptString) {
	s.Region = val
}

// SetStreet sets the value of Street.
func (s *DeliveryDryRunInRequest) SetStreet(val string) {
	s.Street = val
}

// SetHouseNumber sets the value of HouseNumber.
func (s *DeliveryDryRunInRequest) SetHouseNumber(val OptString) {
	s.HouseNumber = val
}

// SetFlatNumber sets the value of FlatNumber.
func (s *DeliveryDryRunInRequest) SetFlatNumber(val OptString) {
	s.FlatNumber = val
}

// SetEntrance sets the value of Entrance.
func (s *DeliveryDryRunInRequest) SetEntrance(val OptString) {
	s.Entrance = val
}

// SetIntercom sets the value of Intercom.
func (s *DeliveryDryRunInRequest) SetIntercom(val OptString) {
	s.Intercom = val
}

// SetFloor sets the value of Floor.
func (s *DeliveryDryRunInRequest) SetFloor(val OptString) {
	s.Floor = val
}

// SetLocation sets the value of Location.
func (s *DeliveryDryRunInRequest) SetLocation(val OptPoint) {
	s.Location = val
}

// SetComment sets the value of Comment.
func (s *DeliveryDryRunInRequest) SetComment(val OptString) {
	s.Comment = val
}

// SetAttributes sets the value of Attributes.
func (s *DeliveryDryRunInRequest) SetAttributes(val OptDeliveryAttributes) {
	s.Attributes = val
}

// Ref: #/components/schemas/HTTPValidationError
type HTTPValidationError struct {
	Errors []ValidationError `json:"errors"`
}

// GetErrors returns the value of Errors.
func (s *HTTPValidationError) GetErrors() []ValidationError {
	return s.Errors
}

// SetErrors sets the value of Errors.
func (s *HTTPValidationError) SetErrors(val []ValidationError) {
	s.Errors = val
}

func (*HTTPValidationError) merchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRes()    {}
func (*HTTPValidationError) merchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRes() {}
func (*HTTPValidationError) placeScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRes()    {}
func (*HTTPValidationError) placeScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRes() {}
func (*HTTPValidationError) placesBlockPublicV1MerchantPlacesBlockPostRes()                  {}
func (*HTTPValidationError) placesBlocksPublicV1MerchantPlacesBlocksGetRes()                 {}
func (*HTTPValidationError) placesCreatePublicV1MerchantPlacesPostRes()                      {}
func (*HTTPValidationError) placesListPublicV1MerchantPlacesGetRes()                         {}
func (*HTTPValidationError) placesOrderCheckPublicV1MerchantPlacesOrderCheckPostRes()        {}
func (*HTTPValidationError) placesUnblockPublicV1MerchantPlacesUnblockPostRes()              {}
func (*HTTPValidationError) placesUpdatePublicV1MerchantPlacesPlaceIDPutRes()                {}
func (*HTTPValidationError) publicCancelOrderPublicV1OrdersOrderIDDeleteRes()                {}
func (*HTTPValidationError) publicCreateOrderPublicV1OrdersPostRes()                         {}
func (*HTTPValidationError) publicDryRunPublicV1OrdersDryRunPostRes()                        {}
func (*HTTPValidationError) publicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRes()    {}
func (*HTTPValidationError) publicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRes()          {}
func (*HTTPValidationError) publicGetOrderPublicV1OrdersOrderIDGetRes()                      {}
func (*HTTPValidationError) publicReturnOrderPublicV1OrdersOrderIDReturnPatchRes()           {}
func (*HTTPValidationError) publicReturnOrderPublicV2OrdersOrderIDReturnPatchRes()           {}
func (*HTTPValidationError) publicUpdateOrderPublicV1OrdersOrderIDPatchRes()                 {}

// Ref: #/components/schemas/IngredientForResponse
type IngredientForResponse struct {
	ID          uuid.UUID `json:"id"`
	Name        string    `json:"name"`
	Price       float64   `json:"price"`
	GroupName   string    `json:"group_name"`
	VatCode     OptString `json:"vat_code"`
	RkeeperCode OptString `json:"rkeeper_code"`
	ExternalID  OptString `json:"external_id"`
}

// GetID returns the value of ID.
func (s *IngredientForResponse) GetID() uuid.UUID {
	return s.ID
}

// GetName returns the value of Name.
func (s *IngredientForResponse) GetName() string {
	return s.Name
}

// GetPrice returns the value of Price.
func (s *IngredientForResponse) GetPrice() float64 {
	return s.Price
}

// GetGroupName returns the value of GroupName.
func (s *IngredientForResponse) GetGroupName() string {
	return s.GroupName
}

// GetVatCode returns the value of VatCode.
func (s *IngredientForResponse) GetVatCode() OptString {
	return s.VatCode
}

// GetRkeeperCode returns the value of RkeeperCode.
func (s *IngredientForResponse) GetRkeeperCode() OptString {
	return s.RkeeperCode
}

// GetExternalID returns the value of ExternalID.
func (s *IngredientForResponse) GetExternalID() OptString {
	return s.ExternalID
}

// SetID sets the value of ID.
func (s *IngredientForResponse) SetID(val uuid.UUID) {
	s.ID = val
}

// SetName sets the value of Name.
func (s *IngredientForResponse) SetName(val string) {
	s.Name = val
}

// SetPrice sets the value of Price.
func (s *IngredientForResponse) SetPrice(val float64) {
	s.Price = val
}

// SetGroupName sets the value of GroupName.
func (s *IngredientForResponse) SetGroupName(val string) {
	s.GroupName = val
}

// SetVatCode sets the value of VatCode.
func (s *IngredientForResponse) SetVatCode(val OptString) {
	s.VatCode = val
}

// SetRkeeperCode sets the value of RkeeperCode.
func (s *IngredientForResponse) SetRkeeperCode(val OptString) {
	s.RkeeperCode = val
}

// SetExternalID sets the value of ExternalID.
func (s *IngredientForResponse) SetExternalID(val OptString) {
	s.ExternalID = val
}

// Ref: #/components/schemas/JsonGeometry
type JsonGeometry struct {
	Data JsonGeometryData `json:"data"`
}

// GetData returns the value of Data.
func (s *JsonGeometry) GetData() JsonGeometryData {
	return s.Data
}

// SetData sets the value of Data.
func (s *JsonGeometry) SetData(val JsonGeometryData) {
	s.Data = val
}

type JsonGeometryData struct{}

// MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostOK is response for MerchantCourierNearDeliveryAddressCallbackURLOrderIDPost operation.
type MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostOK struct{}

// MerchantCourierNearPlaceCallbackURLOrderIDPostOK is response for MerchantCourierNearPlaceCallbackURLOrderIDPost operation.
type MerchantCourierNearPlaceCallbackURLOrderIDPostOK struct{}

// MerchantOrderStatusCallbackURLOrderIDPostOK is response for MerchantOrderStatusCallbackURLOrderIDPost operation.
type MerchantOrderStatusCallbackURLOrderIDPostOK struct{}

// NewOptBool returns new OptBool with value set to v.
func NewOptBool(v bool) OptBool {
	return OptBool{
		Value: v,
		Set:   true,
	}
}

// OptBool is optional bool.
type OptBool struct {
	Value bool
	Set   bool
}

// IsSet returns true if OptBool was set.
func (o OptBool) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptBool) Reset() {
	var v bool
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptBool) SetTo(v bool) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptBool) Get() (v bool, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptBool) Or(d bool) bool {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptDateTime returns new OptDateTime with value set to v.
func NewOptDateTime(v time.Time) OptDateTime {
	return OptDateTime{
		Value: v,
		Set:   true,
	}
}

// OptDateTime is optional time.Time.
type OptDateTime struct {
	Value time.Time
	Set   bool
}

// IsSet returns true if OptDateTime was set.
func (o OptDateTime) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptDateTime) Reset() {
	var v time.Time
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptDateTime) SetTo(v time.Time) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptDateTime) Get() (v time.Time, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptDateTime) Or(d time.Time) time.Time {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptDbModelsBasePoint returns new OptDbModelsBasePoint with value set to v.
func NewOptDbModelsBasePoint(v DbModelsBasePoint) OptDbModelsBasePoint {
	return OptDbModelsBasePoint{
		Value: v,
		Set:   true,
	}
}

// OptDbModelsBasePoint is optional DbModelsBasePoint.
type OptDbModelsBasePoint struct {
	Value DbModelsBasePoint
	Set   bool
}

// IsSet returns true if OptDbModelsBasePoint was set.
func (o OptDbModelsBasePoint) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptDbModelsBasePoint) Reset() {
	var v DbModelsBasePoint
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptDbModelsBasePoint) SetTo(v DbModelsBasePoint) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptDbModelsBasePoint) Get() (v DbModelsBasePoint, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptDbModelsBasePoint) Or(d DbModelsBasePoint) DbModelsBasePoint {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptDeliveryAttributes returns new OptDeliveryAttributes with value set to v.
func NewOptDeliveryAttributes(v DeliveryAttributes) OptDeliveryAttributes {
	return OptDeliveryAttributes{
		Value: v,
		Set:   true,
	}
}

// OptDeliveryAttributes is optional DeliveryAttributes.
type OptDeliveryAttributes struct {
	Value DeliveryAttributes
	Set   bool
}

// IsSet returns true if OptDeliveryAttributes was set.
func (o OptDeliveryAttributes) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptDeliveryAttributes) Reset() {
	var v DeliveryAttributes
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptDeliveryAttributes) SetTo(v DeliveryAttributes) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptDeliveryAttributes) Get() (v DeliveryAttributes, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptDeliveryAttributes) Or(d DeliveryAttributes) DeliveryAttributes {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptFloat64 returns new OptFloat64 with value set to v.
func NewOptFloat64(v float64) OptFloat64 {
	return OptFloat64{
		Value: v,
		Set:   true,
	}
}

// OptFloat64 is optional float64.
type OptFloat64 struct {
	Value float64
	Set   bool
}

// IsSet returns true if OptFloat64 was set.
func (o OptFloat64) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptFloat64) Reset() {
	var v float64
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptFloat64) SetTo(v float64) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptFloat64) Get() (v float64, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptFloat64) Or(d float64) float64 {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptInt returns new OptInt with value set to v.
func NewOptInt(v int) OptInt {
	return OptInt{
		Value: v,
		Set:   true,
	}
}

// OptInt is optional int.
type OptInt struct {
	Value int
	Set   bool
}

// IsSet returns true if OptInt was set.
func (o OptInt) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptInt) Reset() {
	var v int
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptInt) SetTo(v int) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptInt) Get() (v int, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptInt) Or(d int) int {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptOrderCancelCodeEnum returns new OptOrderCancelCodeEnum with value set to v.
func NewOptOrderCancelCodeEnum(v OrderCancelCodeEnum) OptOrderCancelCodeEnum {
	return OptOrderCancelCodeEnum{
		Value: v,
		Set:   true,
	}
}

// OptOrderCancelCodeEnum is optional OrderCancelCodeEnum.
type OptOrderCancelCodeEnum struct {
	Value OrderCancelCodeEnum
	Set   bool
}

// IsSet returns true if OptOrderCancelCodeEnum was set.
func (o OptOrderCancelCodeEnum) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptOrderCancelCodeEnum) Reset() {
	var v OrderCancelCodeEnum
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptOrderCancelCodeEnum) SetTo(v OrderCancelCodeEnum) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptOrderCancelCodeEnum) Get() (v OrderCancelCodeEnum, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptOrderCancelCodeEnum) Or(d OrderCancelCodeEnum) OrderCancelCodeEnum {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptOrderStatusCallback returns new OptOrderStatusCallback with value set to v.
func NewOptOrderStatusCallback(v OrderStatusCallback) OptOrderStatusCallback {
	return OptOrderStatusCallback{
		Value: v,
		Set:   true,
	}
}

// OptOrderStatusCallback is optional OrderStatusCallback.
type OptOrderStatusCallback struct {
	Value OrderStatusCallback
	Set   bool
}

// IsSet returns true if OptOrderStatusCallback was set.
func (o OptOrderStatusCallback) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptOrderStatusCallback) Reset() {
	var v OrderStatusCallback
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptOrderStatusCallback) SetTo(v OrderStatusCallback) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptOrderStatusCallback) Get() (v OrderStatusCallback, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptOrderStatusCallback) Or(d OrderStatusCallback) OrderStatusCallback {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptOrderStatusCallbackPhotosPhotoType returns new OptOrderStatusCallbackPhotosPhotoType with value set to v.
func NewOptOrderStatusCallbackPhotosPhotoType(v OrderStatusCallbackPhotosPhotoType) OptOrderStatusCallbackPhotosPhotoType {
	return OptOrderStatusCallbackPhotosPhotoType{
		Value: v,
		Set:   true,
	}
}

// OptOrderStatusCallbackPhotosPhotoType is optional OrderStatusCallbackPhotosPhotoType.
type OptOrderStatusCallbackPhotosPhotoType struct {
	Value OrderStatusCallbackPhotosPhotoType
	Set   bool
}

// IsSet returns true if OptOrderStatusCallbackPhotosPhotoType was set.
func (o OptOrderStatusCallbackPhotosPhotoType) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptOrderStatusCallbackPhotosPhotoType) Reset() {
	var v OrderStatusCallbackPhotosPhotoType
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptOrderStatusCallbackPhotosPhotoType) SetTo(v OrderStatusCallbackPhotosPhotoType) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptOrderStatusCallbackPhotosPhotoType) Get() (v OrderStatusCallbackPhotosPhotoType, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptOrderStatusCallbackPhotosPhotoType) Or(d OrderStatusCallbackPhotosPhotoType) OrderStatusCallbackPhotosPhotoType {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptOrderStatusEnum returns new OptOrderStatusEnum with value set to v.
func NewOptOrderStatusEnum(v OrderStatusEnum) OptOrderStatusEnum {
	return OptOrderStatusEnum{
		Value: v,
		Set:   true,
	}
}

// OptOrderStatusEnum is optional OrderStatusEnum.
type OptOrderStatusEnum struct {
	Value OrderStatusEnum
	Set   bool
}

// IsSet returns true if OptOrderStatusEnum was set.
func (o OptOrderStatusEnum) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptOrderStatusEnum) Reset() {
	var v OrderStatusEnum
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptOrderStatusEnum) SetTo(v OrderStatusEnum) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptOrderStatusEnum) Get() (v OrderStatusEnum, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptOrderStatusEnum) Or(d OrderStatusEnum) OrderStatusEnum {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptPassport returns new OptPassport with value set to v.
func NewOptPassport(v Passport) OptPassport {
	return OptPassport{
		Value: v,
		Set:   true,
	}
}

// OptPassport is optional Passport.
type OptPassport struct {
	Value Passport
	Set   bool
}

// IsSet returns true if OptPassport was set.
func (o OptPassport) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptPassport) Reset() {
	var v Passport
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptPassport) SetTo(v Passport) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptPassport) Get() (v Passport, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptPassport) Or(d Passport) Passport {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptPaymentMethodEnum returns new OptPaymentMethodEnum with value set to v.
func NewOptPaymentMethodEnum(v PaymentMethodEnum) OptPaymentMethodEnum {
	return OptPaymentMethodEnum{
		Value: v,
		Set:   true,
	}
}

// OptPaymentMethodEnum is optional PaymentMethodEnum.
type OptPaymentMethodEnum struct {
	Value PaymentMethodEnum
	Set   bool
}

// IsSet returns true if OptPaymentMethodEnum was set.
func (o OptPaymentMethodEnum) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptPaymentMethodEnum) Reset() {
	var v PaymentMethodEnum
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptPaymentMethodEnum) SetTo(v PaymentMethodEnum) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptPaymentMethodEnum) Get() (v PaymentMethodEnum, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptPaymentMethodEnum) Or(d PaymentMethodEnum) PaymentMethodEnum {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptPaymentStatusEnum returns new OptPaymentStatusEnum with value set to v.
func NewOptPaymentStatusEnum(v PaymentStatusEnum) OptPaymentStatusEnum {
	return OptPaymentStatusEnum{
		Value: v,
		Set:   true,
	}
}

// OptPaymentStatusEnum is optional PaymentStatusEnum.
type OptPaymentStatusEnum struct {
	Value PaymentStatusEnum
	Set   bool
}

// IsSet returns true if OptPaymentStatusEnum was set.
func (o OptPaymentStatusEnum) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptPaymentStatusEnum) Reset() {
	var v PaymentStatusEnum
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptPaymentStatusEnum) SetTo(v PaymentStatusEnum) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptPaymentStatusEnum) Get() (v PaymentStatusEnum, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptPaymentStatusEnum) Or(d PaymentStatusEnum) PaymentStatusEnum {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptPoint returns new OptPoint with value set to v.
func NewOptPoint(v Point) OptPoint {
	return OptPoint{
		Value: v,
		Set:   true,
	}
}

// OptPoint is optional Point.
type OptPoint struct {
	Value Point
	Set   bool
}

// IsSet returns true if OptPoint was set.
func (o OptPoint) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptPoint) Reset() {
	var v Point
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptPoint) SetTo(v Point) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptPoint) Get() (v Point, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptPoint) Or(d Point) Point {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptString returns new OptString with value set to v.
func NewOptString(v string) OptString {
	return OptString{
		Value: v,
		Set:   true,
	}
}

// OptString is optional string.
type OptString struct {
	Value string
	Set   bool
}

// IsSet returns true if OptString was set.
func (o OptString) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptString) Reset() {
	var v string
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptString) SetTo(v string) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptString) Get() (v string, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptString) Or(d string) string {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptUUID returns new OptUUID with value set to v.
func NewOptUUID(v uuid.UUID) OptUUID {
	return OptUUID{
		Value: v,
		Set:   true,
	}
}

// OptUUID is optional uuid.UUID.
type OptUUID struct {
	Value uuid.UUID
	Set   bool
}

// IsSet returns true if OptUUID was set.
func (o OptUUID) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptUUID) Reset() {
	var v uuid.UUID
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptUUID) SetTo(v uuid.UUID) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptUUID) Get() (v uuid.UUID, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptUUID) Or(d uuid.UUID) uuid.UUID {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptUpdateDeliveryInRequest returns new OptUpdateDeliveryInRequest with value set to v.
func NewOptUpdateDeliveryInRequest(v UpdateDeliveryInRequest) OptUpdateDeliveryInRequest {
	return OptUpdateDeliveryInRequest{
		Value: v,
		Set:   true,
	}
}

// OptUpdateDeliveryInRequest is optional UpdateDeliveryInRequest.
type OptUpdateDeliveryInRequest struct {
	Value UpdateDeliveryInRequest
	Set   bool
}

// IsSet returns true if OptUpdateDeliveryInRequest was set.
func (o OptUpdateDeliveryInRequest) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptUpdateDeliveryInRequest) Reset() {
	var v UpdateDeliveryInRequest
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptUpdateDeliveryInRequest) SetTo(v UpdateDeliveryInRequest) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptUpdateDeliveryInRequest) Get() (v UpdateDeliveryInRequest, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptUpdateDeliveryInRequest) Or(d UpdateDeliveryInRequest) UpdateDeliveryInRequest {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptVehicle returns new OptVehicle with value set to v.
func NewOptVehicle(v Vehicle) OptVehicle {
	return OptVehicle{
		Value: v,
		Set:   true,
	}
}

// OptVehicle is optional Vehicle.
type OptVehicle struct {
	Value Vehicle
	Set   bool
}

// IsSet returns true if OptVehicle was set.
func (o OptVehicle) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptVehicle) Reset() {
	var v Vehicle
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptVehicle) SetTo(v Vehicle) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptVehicle) Get() (v Vehicle, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptVehicle) Or(d Vehicle) Vehicle {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// NewOptWorkTimes returns new OptWorkTimes with value set to v.
func NewOptWorkTimes(v WorkTimes) OptWorkTimes {
	return OptWorkTimes{
		Value: v,
		Set:   true,
	}
}

// OptWorkTimes is optional WorkTimes.
type OptWorkTimes struct {
	Value WorkTimes
	Set   bool
}

// IsSet returns true if OptWorkTimes was set.
func (o OptWorkTimes) IsSet() bool { return o.Set }

// Reset unsets value.
func (o *OptWorkTimes) Reset() {
	var v WorkTimes
	o.Value = v
	o.Set = false
}

// SetTo sets value to v.
func (o *OptWorkTimes) SetTo(v WorkTimes) {
	o.Set = true
	o.Value = v
}

// Get returns value and boolean that denotes whether value was set.
func (o OptWorkTimes) Get() (v WorkTimes, ok bool) {
	if !o.Set {
		return v, false
	}
	return o.Value, true
}

// Or returns value if set, or given parameter if does not.
func (o OptWorkTimes) Or(d WorkTimes) WorkTimes {
	if v, ok := o.Get(); ok {
		return v
	}
	return d
}

// Ref: #/components/schemas/OrderCancelCodeEnum
type OrderCancelCodeEnum int

const (
	ClientChangedMind                OrderCancelCodeEnum = 1
	ClientNotResponding              OrderCancelCodeEnum = 2
	PlaceClosed                      OrderCancelCodeEnum = 3
	ProductUnavailable               OrderCancelCodeEnum = 4
	DeliveryNotAvailableForAddress   OrderCancelCodeEnum = 5
	PlaceTooBusy                     OrderCancelCodeEnum = 6
	TechnicalProblem                 OrderCancelCodeEnum = 7
	OrderCostTooLow                  OrderCancelCodeEnum = 8
	CourierUnavailable               OrderCancelCodeEnum = 9
	CourierAccident                  OrderCancelCodeEnum = 10
	OrderSpoiledByCourier            OrderCancelCodeEnum = 11
	DuplicateOrder                   OrderCancelCodeEnum = 12
	Joke                             OrderCancelCodeEnum = 13
	TestOrder                        OrderCancelCodeEnum = 14
	Other                            OrderCancelCodeEnum = 15
	DeliveryTakesTooLong             OrderCancelCodeEnum = 16
	OrderReceiveTakesTooLong         OrderCancelCodeEnum = 17
	CourierCantFindPickupPlace       OrderCancelCodeEnum = 18
	NoCourierNearPickupPlace         OrderCancelCodeEnum = 19
	CourierDidNotShowUpAtPlace       OrderCancelCodeEnum = 20
	CourierOutOfService              OrderCancelCodeEnum = 21
	MerchantMessedOrdersUp           OrderCancelCodeEnum = 22
	CourierMessedOrdersUp            OrderCancelCodeEnum = 23
	OrderWithShortageUntouched       OrderCancelCodeEnum = 24
	OrderWithShortageTouched         OrderCancelCodeEnum = 25
	OrderMessedUpCompletelyUntouched OrderCancelCodeEnum = 26
	OrderMessedUpCompletelyTTouched  OrderCancelCodeEnum = 27
	ClientChangedMindAfterPickup     OrderCancelCodeEnum = 28
	OrderFulfillmentExceeded         OrderCancelCodeEnum = 90
)

// Ref: #/components/schemas/OrderCheckInRequest
type OrderCheckInRequest struct {
	Longitude   float64     `json:"longitude"`
	Latitude    float64     `json:"latitude"`
	Region      OptString   `json:"region"`
	CityID      OptInt      `json:"city_id"`
	Street      OptString   `json:"street"`
	HouseNumber OptString   `json:"house_number"`
	FlatNumber  OptString   `json:"flat_number"`
	Entrance    OptString   `json:"entrance"`
	Intercom    OptString   `json:"intercom"`
	Floor       OptString   `json:"floor"`
	PickupTime  OptDateTime `json:"pickup_time"`
	Channel     OptString   `json:"channel"`
}

// GetLongitude returns the value of Longitude.
func (s *OrderCheckInRequest) GetLongitude() float64 {
	return s.Longitude
}

// GetLatitude returns the value of Latitude.
func (s *OrderCheckInRequest) GetLatitude() float64 {
	return s.Latitude
}

// GetRegion returns the value of Region.
func (s *OrderCheckInRequest) GetRegion() OptString {
	return s.Region
}

// GetCityID returns the value of CityID.
func (s *OrderCheckInRequest) GetCityID() OptInt {
	return s.CityID
}

// GetStreet returns the value of Street.
func (s *OrderCheckInRequest) GetStreet() OptString {
	return s.Street
}

// GetHouseNumber returns the value of HouseNumber.
func (s *OrderCheckInRequest) GetHouseNumber() OptString {
	return s.HouseNumber
}

// GetFlatNumber returns the value of FlatNumber.
func (s *OrderCheckInRequest) GetFlatNumber() OptString {
	return s.FlatNumber
}

// GetEntrance returns the value of Entrance.
func (s *OrderCheckInRequest) GetEntrance() OptString {
	return s.Entrance
}

// GetIntercom returns the value of Intercom.
func (s *OrderCheckInRequest) GetIntercom() OptString {
	return s.Intercom
}

// GetFloor returns the value of Floor.
func (s *OrderCheckInRequest) GetFloor() OptString {
	return s.Floor
}

// GetPickupTime returns the value of PickupTime.
func (s *OrderCheckInRequest) GetPickupTime() OptDateTime {
	return s.PickupTime
}

// GetChannel returns the value of Channel.
func (s *OrderCheckInRequest) GetChannel() OptString {
	return s.Channel
}

// SetLongitude sets the value of Longitude.
func (s *OrderCheckInRequest) SetLongitude(val float64) {
	s.Longitude = val
}

// SetLatitude sets the value of Latitude.
func (s *OrderCheckInRequest) SetLatitude(val float64) {
	s.Latitude = val
}

// SetRegion sets the value of Region.
func (s *OrderCheckInRequest) SetRegion(val OptString) {
	s.Region = val
}

// SetCityID sets the value of CityID.
func (s *OrderCheckInRequest) SetCityID(val OptInt) {
	s.CityID = val
}

// SetStreet sets the value of Street.
func (s *OrderCheckInRequest) SetStreet(val OptString) {
	s.Street = val
}

// SetHouseNumber sets the value of HouseNumber.
func (s *OrderCheckInRequest) SetHouseNumber(val OptString) {
	s.HouseNumber = val
}

// SetFlatNumber sets the value of FlatNumber.
func (s *OrderCheckInRequest) SetFlatNumber(val OptString) {
	s.FlatNumber = val
}

// SetEntrance sets the value of Entrance.
func (s *OrderCheckInRequest) SetEntrance(val OptString) {
	s.Entrance = val
}

// SetIntercom sets the value of Intercom.
func (s *OrderCheckInRequest) SetIntercom(val OptString) {
	s.Intercom = val
}

// SetFloor sets the value of Floor.
func (s *OrderCheckInRequest) SetFloor(val OptString) {
	s.Floor = val
}

// SetPickupTime sets the value of PickupTime.
func (s *OrderCheckInRequest) SetPickupTime(val OptDateTime) {
	s.PickupTime = val
}

// SetChannel sets the value of Channel.
func (s *OrderCheckInRequest) SetChannel(val OptString) {
	s.Channel = val
}

// Ref: #/components/schemas/OrderCost
type OrderCost struct {
	Cost           float64    `json:"cost"`
	AdditionalCost float64    `json:"additional_cost"`
	ChangeCost     OptFloat64 `json:"change_cost"`
}

// GetCost returns the value of Cost.
func (s *OrderCost) GetCost() float64 {
	return s.Cost
}

// GetAdditionalCost returns the value of AdditionalCost.
func (s *OrderCost) GetAdditionalCost() float64 {
	return s.AdditionalCost
}

// GetChangeCost returns the value of ChangeCost.
func (s *OrderCost) GetChangeCost() OptFloat64 {
	return s.ChangeCost
}

// SetCost sets the value of Cost.
func (s *OrderCost) SetCost(val float64) {
	s.Cost = val
}

// SetAdditionalCost sets the value of AdditionalCost.
func (s *OrderCost) SetAdditionalCost(val float64) {
	s.AdditionalCost = val
}

// SetChangeCost sets the value of ChangeCost.
func (s *OrderCost) SetChangeCost(val OptFloat64) {
	s.ChangeCost = val
}

// Ref: #/components/schemas/OrderCustomer
type OrderCustomer struct {
	Name  OptString `json:"name"`
	Phone OptString `json:"phone"`
	Email OptString `json:"email"`
}

// GetName returns the value of Name.
func (s *OrderCustomer) GetName() OptString {
	return s.Name
}

// GetPhone returns the value of Phone.
func (s *OrderCustomer) GetPhone() OptString {
	return s.Phone
}

// GetEmail returns the value of Email.
func (s *OrderCustomer) GetEmail() OptString {
	return s.Email
}

// SetName sets the value of Name.
func (s *OrderCustomer) SetName(val OptString) {
	s.Name = val
}

// SetPhone sets the value of Phone.
func (s *OrderCustomer) SetPhone(val OptString) {
	s.Phone = val
}

// SetEmail sets the value of Email.
func (s *OrderCustomer) SetEmail(val OptString) {
	s.Email = val
}

// Ref: #/components/schemas/OrderDryRun
type OrderDryRun struct {
	PlaceID            uuid.UUID                       `json:"place_id"`
	Cost               OrderCost                       `json:"cost"`
	PersonCount        int                             `json:"person_count"`
	Comment            OptString                       `json:"comment"`
	Products           []CreateProductInRequest        `json:"products"`
	AdditionalServices []AdditionalServiceCodeNameOnly `json:"additional_services"`
	IsForceStart       OptBool                         `json:"is_force_start"`
	Delivery           DeliveryDryRunInRequest         `json:"delivery"`
}

// GetPlaceID returns the value of PlaceID.
func (s *OrderDryRun) GetPlaceID() uuid.UUID {
	return s.PlaceID
}

// GetCost returns the value of Cost.
func (s *OrderDryRun) GetCost() OrderCost {
	return s.Cost
}

// GetPersonCount returns the value of PersonCount.
func (s *OrderDryRun) GetPersonCount() int {
	return s.PersonCount
}

// GetComment returns the value of Comment.
func (s *OrderDryRun) GetComment() OptString {
	return s.Comment
}

// GetProducts returns the value of Products.
func (s *OrderDryRun) GetProducts() []CreateProductInRequest {
	return s.Products
}

// GetAdditionalServices returns the value of AdditionalServices.
func (s *OrderDryRun) GetAdditionalServices() []AdditionalServiceCodeNameOnly {
	return s.AdditionalServices
}

// GetIsForceStart returns the value of IsForceStart.
func (s *OrderDryRun) GetIsForceStart() OptBool {
	return s.IsForceStart
}

// GetDelivery returns the value of Delivery.
func (s *OrderDryRun) GetDelivery() DeliveryDryRunInRequest {
	return s.Delivery
}

// SetPlaceID sets the value of PlaceID.
func (s *OrderDryRun) SetPlaceID(val uuid.UUID) {
	s.PlaceID = val
}

// SetCost sets the value of Cost.
func (s *OrderDryRun) SetCost(val OrderCost) {
	s.Cost = val
}

// SetPersonCount sets the value of PersonCount.
func (s *OrderDryRun) SetPersonCount(val int) {
	s.PersonCount = val
}

// SetComment sets the value of Comment.
func (s *OrderDryRun) SetComment(val OptString) {
	s.Comment = val
}

// SetProducts sets the value of Products.
func (s *OrderDryRun) SetProducts(val []CreateProductInRequest) {
	s.Products = val
}

// SetAdditionalServices sets the value of AdditionalServices.
func (s *OrderDryRun) SetAdditionalServices(val []AdditionalServiceCodeNameOnly) {
	s.AdditionalServices = val
}

// SetIsForceStart sets the value of IsForceStart.
func (s *OrderDryRun) SetIsForceStart(val OptBool) {
	s.IsForceStart = val
}

// SetDelivery sets the value of Delivery.
func (s *OrderDryRun) SetDelivery(val DeliveryDryRunInRequest) {
	s.Delivery = val
}

// Ref: #/components/schemas/OrderMerchant
type OrderMerchant struct {
	ID   uuid.UUID `json:"id"`
	Name OptString `json:"name"`
}

// GetID returns the value of ID.
func (s *OrderMerchant) GetID() uuid.UUID {
	return s.ID
}

// GetName returns the value of Name.
func (s *OrderMerchant) GetName() OptString {
	return s.Name
}

// SetID sets the value of ID.
func (s *OrderMerchant) SetID(val uuid.UUID) {
	s.ID = val
}

// SetName sets the value of Name.
func (s *OrderMerchant) SetName(val OptString) {
	s.Name = val
}

// Ref: #/components/schemas/OrderPhotoForResponse
type OrderPhotoForResponse struct {
	ID        uuid.UUID      `json:"id"`
	PhotoURL  string         `json:"photo_url"`
	PhotoType OrderPhotoType `json:"photo_type"`
	CreatedAt time.Time      `json:"created_at"`
}

// GetID returns the value of ID.
func (s *OrderPhotoForResponse) GetID() uuid.UUID {
	return s.ID
}

// GetPhotoURL returns the value of PhotoURL.
func (s *OrderPhotoForResponse) GetPhotoURL() string {
	return s.PhotoURL
}

// GetPhotoType returns the value of PhotoType.
func (s *OrderPhotoForResponse) GetPhotoType() OrderPhotoType {
	return s.PhotoType
}

// GetCreatedAt returns the value of CreatedAt.
func (s *OrderPhotoForResponse) GetCreatedAt() time.Time {
	return s.CreatedAt
}

// SetID sets the value of ID.
func (s *OrderPhotoForResponse) SetID(val uuid.UUID) {
	s.ID = val
}

// SetPhotoURL sets the value of PhotoURL.
func (s *OrderPhotoForResponse) SetPhotoURL(val string) {
	s.PhotoURL = val
}

// SetPhotoType sets the value of PhotoType.
func (s *OrderPhotoForResponse) SetPhotoType(val OrderPhotoType) {
	s.PhotoType = val
}

// SetCreatedAt sets the value of CreatedAt.
func (s *OrderPhotoForResponse) SetCreatedAt(val time.Time) {
	s.CreatedAt = val
}

// An enumeration.
// Ref: #/components/schemas/OrderPhotoType
type OrderPhotoType int

const (
	OrderPhotoType1 OrderPhotoType = 1
	OrderPhotoType2 OrderPhotoType = 2
)

// Ref: #/components/schemas/OrderStatusCallback
type OrderStatusCallback struct {
	Status               OptFloat64                  `json:"status"`
	CancelCode           OptFloat64                  `json:"cancel_code"`
	CancelReason         OptString                   `json:"cancel_reason"`
	DeliveryExpectedTime OptDateTime                 `json:"delivery_expected_time"`
	Photos               []OrderStatusCallbackPhotos `json:"photos"`
}

// GetStatus returns the value of Status.
func (s *OrderStatusCallback) GetStatus() OptFloat64 {
	return s.Status
}

// GetCancelCode returns the value of CancelCode.
func (s *OrderStatusCallback) GetCancelCode() OptFloat64 {
	return s.CancelCode
}

// GetCancelReason returns the value of CancelReason.
func (s *OrderStatusCallback) GetCancelReason() OptString {
	return s.CancelReason
}

// GetDeliveryExpectedTime returns the value of DeliveryExpectedTime.
func (s *OrderStatusCallback) GetDeliveryExpectedTime() OptDateTime {
	return s.DeliveryExpectedTime
}

// GetPhotos returns the value of Photos.
func (s *OrderStatusCallback) GetPhotos() []OrderStatusCallbackPhotos {
	return s.Photos
}

// SetStatus sets the value of Status.
func (s *OrderStatusCallback) SetStatus(val OptFloat64) {
	s.Status = val
}

// SetCancelCode sets the value of CancelCode.
func (s *OrderStatusCallback) SetCancelCode(val OptFloat64) {
	s.CancelCode = val
}

// SetCancelReason sets the value of CancelReason.
func (s *OrderStatusCallback) SetCancelReason(val OptString) {
	s.CancelReason = val
}

// SetDeliveryExpectedTime sets the value of DeliveryExpectedTime.
func (s *OrderStatusCallback) SetDeliveryExpectedTime(val OptDateTime) {
	s.DeliveryExpectedTime = val
}

// SetPhotos sets the value of Photos.
func (s *OrderStatusCallback) SetPhotos(val []OrderStatusCallbackPhotos) {
	s.Photos = val
}

// Ref: #/components/schemas/OrderStatusCallback_photos
type OrderStatusCallbackPhotos struct {
	ID  OptUUID   `json:"id"`
	URL OptString `json:"url"`
	// 1 - the courier received the order, 2 - the coureir delivered the order.
	PhotoType OptOrderStatusCallbackPhotosPhotoType `json:"photo_type"`
}

// GetID returns the value of ID.
func (s *OrderStatusCallbackPhotos) GetID() OptUUID {
	return s.ID
}

// GetURL returns the value of URL.
func (s *OrderStatusCallbackPhotos) GetURL() OptString {
	return s.URL
}

// GetPhotoType returns the value of PhotoType.
func (s *OrderStatusCallbackPhotos) GetPhotoType() OptOrderStatusCallbackPhotosPhotoType {
	return s.PhotoType
}

// SetID sets the value of ID.
func (s *OrderStatusCallbackPhotos) SetID(val OptUUID) {
	s.ID = val
}

// SetURL sets the value of URL.
func (s *OrderStatusCallbackPhotos) SetURL(val OptString) {
	s.URL = val
}

// SetPhotoType sets the value of PhotoType.
func (s *OrderStatusCallbackPhotos) SetPhotoType(val OptOrderStatusCallbackPhotosPhotoType) {
	s.PhotoType = val
}

// 1 - the courier received the order, 2 - the coureir delivered the order.
type OrderStatusCallbackPhotosPhotoType float64

const (
	OrderStatusCallbackPhotosPhotoType1 OrderStatusCallbackPhotosPhotoType = 1
	OrderStatusCallbackPhotosPhotoType2 OrderStatusCallbackPhotosPhotoType = 2
)

// Ref: #/components/schemas/OrderStatusEnum
type OrderStatusEnum int

const (
	OrderPosted                     OrderStatusEnum = 101
	OrderPaid                       OrderStatusEnum = 102
	OrderReceived                   OrderStatusEnum = 103
	CourierAppointed                OrderStatusEnum = 104
	CourierAcceptedOrder            OrderStatusEnum = 201
	OrderReadyToPickup              OrderStatusEnum = 202
	CourierArrivedToPlace           OrderStatusEnum = 203
	CourierTookOrder                OrderStatusEnum = 204
	CourierArrivedAtDeliveryAddress OrderStatusEnum = 205
	CourierStartedPayment           OrderStatusEnum = 210
	ProblemWithOrder                OrderStatusEnum = 299
	OrderDelivered                  OrderStatusEnum = 301
	OrderCanceledToBeRefunded       OrderStatusEnum = 401
	CourierAcceptedOrderReturn      OrderStatusEnum = 402
	CourierDeliveredCancelledOrder  OrderStatusEnum = 403
	RefundConfirmedByMerchant       OrderStatusEnum = 404
	OrderCancelledNoReturnNeeded    OrderStatusEnum = 501
	PaymentError                    OrderStatusEnum = 601
)

// Ref: #/components/schemas/Passport
type Passport struct {
	Series string `json:"series"`
	Number string `json:"number"`
}

// GetSeries returns the value of Series.
func (s *Passport) GetSeries() string {
	return s.Series
}

// GetNumber returns the value of Number.
func (s *Passport) GetNumber() string {
	return s.Number
}

// SetSeries sets the value of Series.
func (s *Passport) SetSeries(val string) {
	s.Series = val
}

// SetNumber sets the value of Number.
func (s *Passport) SetNumber(val string) {
	s.Number = val
}

// Ref: #/components/schemas/PaymentMethodEnum
type PaymentMethodEnum int

const (
	Cash   PaymentMethodEnum = 1
	Card   PaymentMethodEnum = 2
	Online PaymentMethodEnum = 4
)

// Ref: #/components/schemas/PaymentStatusEnum
type PaymentStatusEnum int

const (
	NotPaid           PaymentStatusEnum = 1
	PaidPartially     PaymentStatusEnum = 2
	Paid              PaymentStatusEnum = 3
	RefundedPartially PaymentStatusEnum = 4
	Refunded          PaymentStatusEnum = 5
)

// Ref: #/components/schemas/PlaceAccessForResponse
type PlaceAccessForResponse struct {
	ID              uuid.UUID                    `json:"id"`
	MerchantPlaceID string                       `json:"merchant_place_id"`
	PickupTime      OptDateTime                  `json:"pickup_time"`
	IntervalStart   OptDateTime                  `json:"interval_start"`
	IntervalEnd     OptDateTime                  `json:"interval_end"`
	Status          PlaceAccessForResponseStatus `json:"status"`
	DeliveryFee     OptFloat64                   `json:"delivery_fee"`
}

// GetID returns the value of ID.
func (s *PlaceAccessForResponse) GetID() uuid.UUID {
	return s.ID
}

// GetMerchantPlaceID returns the value of MerchantPlaceID.
func (s *PlaceAccessForResponse) GetMerchantPlaceID() string {
	return s.MerchantPlaceID
}

// GetPickupTime returns the value of PickupTime.
func (s *PlaceAccessForResponse) GetPickupTime() OptDateTime {
	return s.PickupTime
}

// GetIntervalStart returns the value of IntervalStart.
func (s *PlaceAccessForResponse) GetIntervalStart() OptDateTime {
	return s.IntervalStart
}

// GetIntervalEnd returns the value of IntervalEnd.
func (s *PlaceAccessForResponse) GetIntervalEnd() OptDateTime {
	return s.IntervalEnd
}

// GetStatus returns the value of Status.
func (s *PlaceAccessForResponse) GetStatus() PlaceAccessForResponseStatus {
	return s.Status
}

// GetDeliveryFee returns the value of DeliveryFee.
func (s *PlaceAccessForResponse) GetDeliveryFee() OptFloat64 {
	return s.DeliveryFee
}

// SetID sets the value of ID.
func (s *PlaceAccessForResponse) SetID(val uuid.UUID) {
	s.ID = val
}

// SetMerchantPlaceID sets the value of MerchantPlaceID.
func (s *PlaceAccessForResponse) SetMerchantPlaceID(val string) {
	s.MerchantPlaceID = val
}

// SetPickupTime sets the value of PickupTime.
func (s *PlaceAccessForResponse) SetPickupTime(val OptDateTime) {
	s.PickupTime = val
}

// SetIntervalStart sets the value of IntervalStart.
func (s *PlaceAccessForResponse) SetIntervalStart(val OptDateTime) {
	s.IntervalStart = val
}

// SetIntervalEnd sets the value of IntervalEnd.
func (s *PlaceAccessForResponse) SetIntervalEnd(val OptDateTime) {
	s.IntervalEnd = val
}

// SetStatus sets the value of Status.
func (s *PlaceAccessForResponse) SetStatus(val PlaceAccessForResponseStatus) {
	s.Status = val
}

// SetDeliveryFee sets the value of DeliveryFee.
func (s *PlaceAccessForResponse) SetDeliveryFee(val OptFloat64) {
	s.DeliveryFee = val
}

type PlaceAccessForResponseStatus struct{}

// Ref: #/components/schemas/PlaceAccessList
type PlaceAccessList struct {
	IsAccess bool                     `json:"is_access"`
	Places   []PlaceAccessForResponse `json:"places"`
}

// GetIsAccess returns the value of IsAccess.
func (s *PlaceAccessList) GetIsAccess() bool {
	return s.IsAccess
}

// GetPlaces returns the value of Places.
func (s *PlaceAccessList) GetPlaces() []PlaceAccessForResponse {
	return s.Places
}

// SetIsAccess sets the value of IsAccess.
func (s *PlaceAccessList) SetIsAccess(val bool) {
	s.IsAccess = val
}

// SetPlaces sets the value of Places.
func (s *PlaceAccessList) SetPlaces(val []PlaceAccessForResponse) {
	s.Places = val
}

func (*PlaceAccessList) placesOrderCheckPublicV1MerchantPlacesOrderCheckPostRes() {}

// Ref: #/components/schemas/PlaceBlockInRequest
type PlaceBlockInRequest struct {
	PlaceIds     []uuid.UUID                   `json:"place_ids"`
	DatetimeFrom time.Time                     `json:"datetime_from"`
	DatetimeTo   OptDateTime                   `json:"datetime_to"`
	BlockReason  OptString                     `json:"block_reason"`
	ExtraData    *PlaceBlockInRequestExtraData `json:"extra_data"`
}

// GetPlaceIds returns the value of PlaceIds.
func (s *PlaceBlockInRequest) GetPlaceIds() []uuid.UUID {
	return s.PlaceIds
}

// GetDatetimeFrom returns the value of DatetimeFrom.
func (s *PlaceBlockInRequest) GetDatetimeFrom() time.Time {
	return s.DatetimeFrom
}

// GetDatetimeTo returns the value of DatetimeTo.
func (s *PlaceBlockInRequest) GetDatetimeTo() OptDateTime {
	return s.DatetimeTo
}

// GetBlockReason returns the value of BlockReason.
func (s *PlaceBlockInRequest) GetBlockReason() OptString {
	return s.BlockReason
}

// GetExtraData returns the value of ExtraData.
func (s *PlaceBlockInRequest) GetExtraData() *PlaceBlockInRequestExtraData {
	return s.ExtraData
}

// SetPlaceIds sets the value of PlaceIds.
func (s *PlaceBlockInRequest) SetPlaceIds(val []uuid.UUID) {
	s.PlaceIds = val
}

// SetDatetimeFrom sets the value of DatetimeFrom.
func (s *PlaceBlockInRequest) SetDatetimeFrom(val time.Time) {
	s.DatetimeFrom = val
}

// SetDatetimeTo sets the value of DatetimeTo.
func (s *PlaceBlockInRequest) SetDatetimeTo(val OptDateTime) {
	s.DatetimeTo = val
}

// SetBlockReason sets the value of BlockReason.
func (s *PlaceBlockInRequest) SetBlockReason(val OptString) {
	s.BlockReason = val
}

// SetExtraData sets the value of ExtraData.
func (s *PlaceBlockInRequest) SetExtraData(val *PlaceBlockInRequestExtraData) {
	s.ExtraData = val
}

type PlaceBlockInRequestExtraData struct{}

// Ref: #/components/schemas/PlaceBlockInResponse
type PlaceBlockInResponse struct {
	ID           uuid.UUID   `json:"id"`
	PlaceID      uuid.UUID   `json:"place_id"`
	DatetimeFrom time.Time   `json:"datetime_from"`
	DatetimeTo   OptDateTime `json:"datetime_to"`
	BlockReason  OptString   `json:"block_reason"`
	IsBlocked    bool        `json:"is_blocked"`
}

// GetID returns the value of ID.
func (s *PlaceBlockInResponse) GetID() uuid.UUID {
	return s.ID
}

// GetPlaceID returns the value of PlaceID.
func (s *PlaceBlockInResponse) GetPlaceID() uuid.UUID {
	return s.PlaceID
}

// GetDatetimeFrom returns the value of DatetimeFrom.
func (s *PlaceBlockInResponse) GetDatetimeFrom() time.Time {
	return s.DatetimeFrom
}

// GetDatetimeTo returns the value of DatetimeTo.
func (s *PlaceBlockInResponse) GetDatetimeTo() OptDateTime {
	return s.DatetimeTo
}

// GetBlockReason returns the value of BlockReason.
func (s *PlaceBlockInResponse) GetBlockReason() OptString {
	return s.BlockReason
}

// GetIsBlocked returns the value of IsBlocked.
func (s *PlaceBlockInResponse) GetIsBlocked() bool {
	return s.IsBlocked
}

// SetID sets the value of ID.
func (s *PlaceBlockInResponse) SetID(val uuid.UUID) {
	s.ID = val
}

// SetPlaceID sets the value of PlaceID.
func (s *PlaceBlockInResponse) SetPlaceID(val uuid.UUID) {
	s.PlaceID = val
}

// SetDatetimeFrom sets the value of DatetimeFrom.
func (s *PlaceBlockInResponse) SetDatetimeFrom(val time.Time) {
	s.DatetimeFrom = val
}

// SetDatetimeTo sets the value of DatetimeTo.
func (s *PlaceBlockInResponse) SetDatetimeTo(val OptDateTime) {
	s.DatetimeTo = val
}

// SetBlockReason sets the value of BlockReason.
func (s *PlaceBlockInResponse) SetBlockReason(val OptString) {
	s.BlockReason = val
}

// SetIsBlocked sets the value of IsBlocked.
func (s *PlaceBlockInResponse) SetIsBlocked(val bool) {
	s.IsBlocked = val
}

// Ref: #/components/schemas/PlaceInDB
type PlaceInDB struct {
	Location        DbModelsBasePoint `json:"location"`
	MerchantID      uuid.UUID         `json:"merchant_id"`
	MerchantPlaceID OptString         `json:"merchant_place_id"`
	Address         string            `json:"address"`
	Name            string            `json:"name"`
	CityID          int               `json:"city_id"`
	Email           OptString         `json:"email"`
	ID              uuid.UUID         `json:"id"`
	WorkTimes       OptWorkTimes      `json:"work_times"`
	IsBlocked       OptBool           `json:"is_blocked"`
}

// GetLocation returns the value of Location.
func (s *PlaceInDB) GetLocation() DbModelsBasePoint {
	return s.Location
}

// GetMerchantID returns the value of MerchantID.
func (s *PlaceInDB) GetMerchantID() uuid.UUID {
	return s.MerchantID
}

// GetMerchantPlaceID returns the value of MerchantPlaceID.
func (s *PlaceInDB) GetMerchantPlaceID() OptString {
	return s.MerchantPlaceID
}

// GetAddress returns the value of Address.
func (s *PlaceInDB) GetAddress() string {
	return s.Address
}

// GetName returns the value of Name.
func (s *PlaceInDB) GetName() string {
	return s.Name
}

// GetCityID returns the value of CityID.
func (s *PlaceInDB) GetCityID() int {
	return s.CityID
}

// GetEmail returns the value of Email.
func (s *PlaceInDB) GetEmail() OptString {
	return s.Email
}

// GetID returns the value of ID.
func (s *PlaceInDB) GetID() uuid.UUID {
	return s.ID
}

// GetWorkTimes returns the value of WorkTimes.
func (s *PlaceInDB) GetWorkTimes() OptWorkTimes {
	return s.WorkTimes
}

// GetIsBlocked returns the value of IsBlocked.
func (s *PlaceInDB) GetIsBlocked() OptBool {
	return s.IsBlocked
}

// SetLocation sets the value of Location.
func (s *PlaceInDB) SetLocation(val DbModelsBasePoint) {
	s.Location = val
}

// SetMerchantID sets the value of MerchantID.
func (s *PlaceInDB) SetMerchantID(val uuid.UUID) {
	s.MerchantID = val
}

// SetMerchantPlaceID sets the value of MerchantPlaceID.
func (s *PlaceInDB) SetMerchantPlaceID(val OptString) {
	s.MerchantPlaceID = val
}

// SetAddress sets the value of Address.
func (s *PlaceInDB) SetAddress(val string) {
	s.Address = val
}

// SetName sets the value of Name.
func (s *PlaceInDB) SetName(val string) {
	s.Name = val
}

// SetCityID sets the value of CityID.
func (s *PlaceInDB) SetCityID(val int) {
	s.CityID = val
}

// SetEmail sets the value of Email.
func (s *PlaceInDB) SetEmail(val OptString) {
	s.Email = val
}

// SetID sets the value of ID.
func (s *PlaceInDB) SetID(val uuid.UUID) {
	s.ID = val
}

// SetWorkTimes sets the value of WorkTimes.
func (s *PlaceInDB) SetWorkTimes(val OptWorkTimes) {
	s.WorkTimes = val
}

// SetIsBlocked sets the value of IsBlocked.
func (s *PlaceInDB) SetIsBlocked(val OptBool) {
	s.IsBlocked = val
}

func (*PlaceInDB) placesCreatePublicV1MerchantPlacesPostRes()       {}
func (*PlaceInDB) placesUpdatePublicV1MerchantPlacesPlaceIDPutRes() {}

// Ref: #/components/schemas/PlaceUnBlockInRequest
type PlaceUnBlockInRequest struct {
	PlaceIds []uuid.UUID `json:"place_ids"`
	BlockIds []uuid.UUID `json:"block_ids"`
}

// GetPlaceIds returns the value of PlaceIds.
func (s *PlaceUnBlockInRequest) GetPlaceIds() []uuid.UUID {
	return s.PlaceIds
}

// GetBlockIds returns the value of BlockIds.
func (s *PlaceUnBlockInRequest) GetBlockIds() []uuid.UUID {
	return s.BlockIds
}

// SetPlaceIds sets the value of PlaceIds.
func (s *PlaceUnBlockInRequest) SetPlaceIds(val []uuid.UUID) {
	s.PlaceIds = val
}

// SetBlockIds sets the value of BlockIds.
func (s *PlaceUnBlockInRequest) SetBlockIds(val []uuid.UUID) {
	s.BlockIds = val
}

// Ref: #/components/schemas/PlaceUpdate
type PlaceUpdate struct {
	Location        OptDbModelsBasePoint `json:"location"`
	Address         OptString            `json:"address"`
	Name            OptString            `json:"name"`
	CityID          OptInt               `json:"city_id"`
	Email           OptString            `json:"email"`
	MerchantPlaceID OptString            `json:"merchant_place_id"`
}

// GetLocation returns the value of Location.
func (s *PlaceUpdate) GetLocation() OptDbModelsBasePoint {
	return s.Location
}

// GetAddress returns the value of Address.
func (s *PlaceUpdate) GetAddress() OptString {
	return s.Address
}

// GetName returns the value of Name.
func (s *PlaceUpdate) GetName() OptString {
	return s.Name
}

// GetCityID returns the value of CityID.
func (s *PlaceUpdate) GetCityID() OptInt {
	return s.CityID
}

// GetEmail returns the value of Email.
func (s *PlaceUpdate) GetEmail() OptString {
	return s.Email
}

// GetMerchantPlaceID returns the value of MerchantPlaceID.
func (s *PlaceUpdate) GetMerchantPlaceID() OptString {
	return s.MerchantPlaceID
}

// SetLocation sets the value of Location.
func (s *PlaceUpdate) SetLocation(val OptDbModelsBasePoint) {
	s.Location = val
}

// SetAddress sets the value of Address.
func (s *PlaceUpdate) SetAddress(val OptString) {
	s.Address = val
}

// SetName sets the value of Name.
func (s *PlaceUpdate) SetName(val OptString) {
	s.Name = val
}

// SetCityID sets the value of CityID.
func (s *PlaceUpdate) SetCityID(val OptInt) {
	s.CityID = val
}

// SetEmail sets the value of Email.
func (s *PlaceUpdate) SetEmail(val OptString) {
	s.Email = val
}

// SetMerchantPlaceID sets the value of MerchantPlaceID.
func (s *PlaceUpdate) SetMerchantPlaceID(val OptString) {
	s.MerchantPlaceID = val
}

// Ref: #/components/schemas/PlaceWithArea
type PlaceWithArea struct {
	Location        DbModelsBasePoint      `json:"location"`
	MerchantPlaceID string                 `json:"merchant_place_id"`
	Address         string                 `json:"address"`
	Name            string                 `json:"name"`
	CityID          int                    `json:"city_id"`
	Area            AreaCreateWithoutPlace `json:"area"`
}

// GetLocation returns the value of Location.
func (s *PlaceWithArea) GetLocation() DbModelsBasePoint {
	return s.Location
}

// GetMerchantPlaceID returns the value of MerchantPlaceID.
func (s *PlaceWithArea) GetMerchantPlaceID() string {
	return s.MerchantPlaceID
}

// GetAddress returns the value of Address.
func (s *PlaceWithArea) GetAddress() string {
	return s.Address
}

// GetName returns the value of Name.
func (s *PlaceWithArea) GetName() string {
	return s.Name
}

// GetCityID returns the value of CityID.
func (s *PlaceWithArea) GetCityID() int {
	return s.CityID
}

// GetArea returns the value of Area.
func (s *PlaceWithArea) GetArea() AreaCreateWithoutPlace {
	return s.Area
}

// SetLocation sets the value of Location.
func (s *PlaceWithArea) SetLocation(val DbModelsBasePoint) {
	s.Location = val
}

// SetMerchantPlaceID sets the value of MerchantPlaceID.
func (s *PlaceWithArea) SetMerchantPlaceID(val string) {
	s.MerchantPlaceID = val
}

// SetAddress sets the value of Address.
func (s *PlaceWithArea) SetAddress(val string) {
	s.Address = val
}

// SetName sets the value of Name.
func (s *PlaceWithArea) SetName(val string) {
	s.Name = val
}

// SetCityID sets the value of CityID.
func (s *PlaceWithArea) SetCityID(val int) {
	s.CityID = val
}

// SetArea sets the value of Area.
func (s *PlaceWithArea) SetArea(val AreaCreateWithoutPlace) {
	s.Area = val
}

type PlacesBlockPublicV1MerchantPlacesBlockPostOKApplicationJSON []PlaceBlockInResponse

func (*PlacesBlockPublicV1MerchantPlacesBlockPostOKApplicationJSON) placesBlockPublicV1MerchantPlacesBlockPostRes() {
}

type PlacesBlocksPublicV1MerchantPlacesBlocksGetOKApplicationJSON []PlaceBlockInResponse

func (*PlacesBlocksPublicV1MerchantPlacesBlocksGetOKApplicationJSON) placesBlocksPublicV1MerchantPlacesBlocksGetRes() {
}

type PlacesListPublicV1MerchantPlacesGetOKApplicationJSON []PlaceInDB

func (*PlacesListPublicV1MerchantPlacesGetOKApplicationJSON) placesListPublicV1MerchantPlacesGetRes() {
}

type PlacesUnblockPublicV1MerchantPlacesUnblockPostOKApplicationJSON []PlaceBlockInResponse

func (*PlacesUnblockPublicV1MerchantPlacesUnblockPostOKApplicationJSON) placesUnblockPublicV1MerchantPlacesUnblockPostRes() {
}

// Ref: #/components/schemas/Point
type Point struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

// GetLatitude returns the value of Latitude.
func (s *Point) GetLatitude() float64 {
	return s.Latitude
}

// GetLongitude returns the value of Longitude.
func (s *Point) GetLongitude() float64 {
	return s.Longitude
}

// SetLatitude sets the value of Latitude.
func (s *Point) SetLatitude(val float64) {
	s.Latitude = val
}

// SetLongitude sets the value of Longitude.
func (s *Point) SetLongitude(val float64) {
	s.Longitude = val
}

// Ref: #/components/schemas/ProductForResponse
type ProductForResponse struct {
	ID             uuid.UUID               `json:"id"`
	Name           string                  `json:"name"`
	Quantity       int                     `json:"quantity"`
	Price          float64                 `json:"price"`
	TotalPrice     float64                 `json:"total_price"`
	VatCode        OptString               `json:"vat_code"`
	ExternalID     OptString               `json:"external_id"`
	RkeeperCode    OptString               `json:"rkeeper_code"`
	RefundQuantity int                     `json:"refund_quantity"`
	Ingredients    []IngredientForResponse `json:"ingredients"`
}

// GetID returns the value of ID.
func (s *ProductForResponse) GetID() uuid.UUID {
	return s.ID
}

// GetName returns the value of Name.
func (s *ProductForResponse) GetName() string {
	return s.Name
}

// GetQuantity returns the value of Quantity.
func (s *ProductForResponse) GetQuantity() int {
	return s.Quantity
}

// GetPrice returns the value of Price.
func (s *ProductForResponse) GetPrice() float64 {
	return s.Price
}

// GetTotalPrice returns the value of TotalPrice.
func (s *ProductForResponse) GetTotalPrice() float64 {
	return s.TotalPrice
}

// GetVatCode returns the value of VatCode.
func (s *ProductForResponse) GetVatCode() OptString {
	return s.VatCode
}

// GetExternalID returns the value of ExternalID.
func (s *ProductForResponse) GetExternalID() OptString {
	return s.ExternalID
}

// GetRkeeperCode returns the value of RkeeperCode.
func (s *ProductForResponse) GetRkeeperCode() OptString {
	return s.RkeeperCode
}

// GetRefundQuantity returns the value of RefundQuantity.
func (s *ProductForResponse) GetRefundQuantity() int {
	return s.RefundQuantity
}

// GetIngredients returns the value of Ingredients.
func (s *ProductForResponse) GetIngredients() []IngredientForResponse {
	return s.Ingredients
}

// SetID sets the value of ID.
func (s *ProductForResponse) SetID(val uuid.UUID) {
	s.ID = val
}

// SetName sets the value of Name.
func (s *ProductForResponse) SetName(val string) {
	s.Name = val
}

// SetQuantity sets the value of Quantity.
func (s *ProductForResponse) SetQuantity(val int) {
	s.Quantity = val
}

// SetPrice sets the value of Price.
func (s *ProductForResponse) SetPrice(val float64) {
	s.Price = val
}

// SetTotalPrice sets the value of TotalPrice.
func (s *ProductForResponse) SetTotalPrice(val float64) {
	s.TotalPrice = val
}

// SetVatCode sets the value of VatCode.
func (s *ProductForResponse) SetVatCode(val OptString) {
	s.VatCode = val
}

// SetExternalID sets the value of ExternalID.
func (s *ProductForResponse) SetExternalID(val OptString) {
	s.ExternalID = val
}

// SetRkeeperCode sets the value of RkeeperCode.
func (s *ProductForResponse) SetRkeeperCode(val OptString) {
	s.RkeeperCode = val
}

// SetRefundQuantity sets the value of RefundQuantity.
func (s *ProductForResponse) SetRefundQuantity(val int) {
	s.RefundQuantity = val
}

// SetIngredients sets the value of Ingredients.
func (s *ProductForResponse) SetIngredients(val []IngredientForResponse) {
	s.Ingredients = val
}

// PublicCancelOrderPublicV1OrdersOrderIDDeleteNoContent is response for PublicCancelOrderPublicV1OrdersOrderIDDelete operation.
type PublicCancelOrderPublicV1OrdersOrderIDDeleteNoContent struct{}

func (*PublicCancelOrderPublicV1OrdersOrderIDDeleteNoContent) publicCancelOrderPublicV1OrdersOrderIDDeleteRes() {
}

// Ref: #/components/schemas/PublicCourierPositionForResponse
type PublicCourierPositionForResponse struct {
	OrderID     uuid.UUID   `json:"order_id"`
	CourierID   uuid.UUID   `json:"courier_id"`
	OrderStatus string      `json:"order_status"`
	CourierName string      `json:"courier_name"`
	PhoneNumber OptString   `json:"phone_number"`
	Location    Point       `json:"location"`
	Passport    OptPassport `json:"passport"`
	Vehicle     OptVehicle  `json:"vehicle"`
}

// GetOrderID returns the value of OrderID.
func (s *PublicCourierPositionForResponse) GetOrderID() uuid.UUID {
	return s.OrderID
}

// GetCourierID returns the value of CourierID.
func (s *PublicCourierPositionForResponse) GetCourierID() uuid.UUID {
	return s.CourierID
}

// GetOrderStatus returns the value of OrderStatus.
func (s *PublicCourierPositionForResponse) GetOrderStatus() string {
	return s.OrderStatus
}

// GetCourierName returns the value of CourierName.
func (s *PublicCourierPositionForResponse) GetCourierName() string {
	return s.CourierName
}

// GetPhoneNumber returns the value of PhoneNumber.
func (s *PublicCourierPositionForResponse) GetPhoneNumber() OptString {
	return s.PhoneNumber
}

// GetLocation returns the value of Location.
func (s *PublicCourierPositionForResponse) GetLocation() Point {
	return s.Location
}

// GetPassport returns the value of Passport.
func (s *PublicCourierPositionForResponse) GetPassport() OptPassport {
	return s.Passport
}

// GetVehicle returns the value of Vehicle.
func (s *PublicCourierPositionForResponse) GetVehicle() OptVehicle {
	return s.Vehicle
}

// SetOrderID sets the value of OrderID.
func (s *PublicCourierPositionForResponse) SetOrderID(val uuid.UUID) {
	s.OrderID = val
}

// SetCourierID sets the value of CourierID.
func (s *PublicCourierPositionForResponse) SetCourierID(val uuid.UUID) {
	s.CourierID = val
}

// SetOrderStatus sets the value of OrderStatus.
func (s *PublicCourierPositionForResponse) SetOrderStatus(val string) {
	s.OrderStatus = val
}

// SetCourierName sets the value of CourierName.
func (s *PublicCourierPositionForResponse) SetCourierName(val string) {
	s.CourierName = val
}

// SetPhoneNumber sets the value of PhoneNumber.
func (s *PublicCourierPositionForResponse) SetPhoneNumber(val OptString) {
	s.PhoneNumber = val
}

// SetLocation sets the value of Location.
func (s *PublicCourierPositionForResponse) SetLocation(val Point) {
	s.Location = val
}

// SetPassport sets the value of Passport.
func (s *PublicCourierPositionForResponse) SetPassport(val OptPassport) {
	s.Passport = val
}

// SetVehicle sets the value of Vehicle.
func (s *PublicCourierPositionForResponse) SetVehicle(val OptVehicle) {
	s.Vehicle = val
}

func (*PublicCourierPositionForResponse) publicGetCourierPositionPublicV1OrdersOrderIDTrackingGetRes() {
}

// PublicCreateOrderPublicV1OrdersPostForbidden is response for PublicCreateOrderPublicV1OrdersPost operation.
type PublicCreateOrderPublicV1OrdersPostForbidden struct{}

func (*PublicCreateOrderPublicV1OrdersPostForbidden) publicCreateOrderPublicV1OrdersPostRes() {}

// Ref: #/components/schemas/PublicDeliveryForResponse
type PublicDeliveryForResponse struct {
	ExpectedTime  OptDateTime           `json:"expected_time"`
	PaymentMethod OptPaymentMethodEnum  `json:"payment_method"`
	ConsumerData  OptString             `json:"consumer_data"`
	Region        OptString             `json:"region"`
	City          OptString             `json:"city"`
	Street        OptString             `json:"street"`
	HouseNumber   OptString             `json:"house_number"`
	FlatNumber    OptString             `json:"flat_number"`
	Entrance      OptString             `json:"entrance"`
	Intercom      OptString             `json:"intercom"`
	Floor         OptString             `json:"floor"`
	Location      OptPoint              `json:"location"`
	Comment       OptString             `json:"comment"`
	Attributes    OptDeliveryAttributes `json:"attributes"`
}

// GetExpectedTime returns the value of ExpectedTime.
func (s *PublicDeliveryForResponse) GetExpectedTime() OptDateTime {
	return s.ExpectedTime
}

// GetPaymentMethod returns the value of PaymentMethod.
func (s *PublicDeliveryForResponse) GetPaymentMethod() OptPaymentMethodEnum {
	return s.PaymentMethod
}

// GetConsumerData returns the value of ConsumerData.
func (s *PublicDeliveryForResponse) GetConsumerData() OptString {
	return s.ConsumerData
}

// GetRegion returns the value of Region.
func (s *PublicDeliveryForResponse) GetRegion() OptString {
	return s.Region
}

// GetCity returns the value of City.
func (s *PublicDeliveryForResponse) GetCity() OptString {
	return s.City
}

// GetStreet returns the value of Street.
func (s *PublicDeliveryForResponse) GetStreet() OptString {
	return s.Street
}

// GetHouseNumber returns the value of HouseNumber.
func (s *PublicDeliveryForResponse) GetHouseNumber() OptString {
	return s.HouseNumber
}

// GetFlatNumber returns the value of FlatNumber.
func (s *PublicDeliveryForResponse) GetFlatNumber() OptString {
	return s.FlatNumber
}

// GetEntrance returns the value of Entrance.
func (s *PublicDeliveryForResponse) GetEntrance() OptString {
	return s.Entrance
}

// GetIntercom returns the value of Intercom.
func (s *PublicDeliveryForResponse) GetIntercom() OptString {
	return s.Intercom
}

// GetFloor returns the value of Floor.
func (s *PublicDeliveryForResponse) GetFloor() OptString {
	return s.Floor
}

// GetLocation returns the value of Location.
func (s *PublicDeliveryForResponse) GetLocation() OptPoint {
	return s.Location
}

// GetComment returns the value of Comment.
func (s *PublicDeliveryForResponse) GetComment() OptString {
	return s.Comment
}

// GetAttributes returns the value of Attributes.
func (s *PublicDeliveryForResponse) GetAttributes() OptDeliveryAttributes {
	return s.Attributes
}

// SetExpectedTime sets the value of ExpectedTime.
func (s *PublicDeliveryForResponse) SetExpectedTime(val OptDateTime) {
	s.ExpectedTime = val
}

// SetPaymentMethod sets the value of PaymentMethod.
func (s *PublicDeliveryForResponse) SetPaymentMethod(val OptPaymentMethodEnum) {
	s.PaymentMethod = val
}

// SetConsumerData sets the value of ConsumerData.
func (s *PublicDeliveryForResponse) SetConsumerData(val OptString) {
	s.ConsumerData = val
}

// SetRegion sets the value of Region.
func (s *PublicDeliveryForResponse) SetRegion(val OptString) {
	s.Region = val
}

// SetCity sets the value of City.
func (s *PublicDeliveryForResponse) SetCity(val OptString) {
	s.City = val
}

// SetStreet sets the value of Street.
func (s *PublicDeliveryForResponse) SetStreet(val OptString) {
	s.Street = val
}

// SetHouseNumber sets the value of HouseNumber.
func (s *PublicDeliveryForResponse) SetHouseNumber(val OptString) {
	s.HouseNumber = val
}

// SetFlatNumber sets the value of FlatNumber.
func (s *PublicDeliveryForResponse) SetFlatNumber(val OptString) {
	s.FlatNumber = val
}

// SetEntrance sets the value of Entrance.
func (s *PublicDeliveryForResponse) SetEntrance(val OptString) {
	s.Entrance = val
}

// SetIntercom sets the value of Intercom.
func (s *PublicDeliveryForResponse) SetIntercom(val OptString) {
	s.Intercom = val
}

// SetFloor sets the value of Floor.
func (s *PublicDeliveryForResponse) SetFloor(val OptString) {
	s.Floor = val
}

// SetLocation sets the value of Location.
func (s *PublicDeliveryForResponse) SetLocation(val OptPoint) {
	s.Location = val
}

// SetComment sets the value of Comment.
func (s *PublicDeliveryForResponse) SetComment(val OptString) {
	s.Comment = val
}

// SetAttributes sets the value of Attributes.
func (s *PublicDeliveryForResponse) SetAttributes(val OptDeliveryAttributes) {
	s.Attributes = val
}

// PublicDryRunPublicV1OrdersDryRunPostForbidden is response for PublicDryRunPublicV1OrdersDryRunPost operation.
type PublicDryRunPublicV1OrdersDryRunPostForbidden struct{}

func (*PublicDryRunPublicV1OrdersDryRunPostForbidden) publicDryRunPublicV1OrdersDryRunPostRes() {}

type PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetOKApplicationJSON []OrderPhotoForResponse

func (*PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetOKApplicationJSON) publicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetRes() {
}

// PublicGetOrderPublicV1OrdersOrderIDGetNotFound is response for PublicGetOrderPublicV1OrdersOrderIDGet operation.
type PublicGetOrderPublicV1OrdersOrderIDGetNotFound struct{}

func (*PublicGetOrderPublicV1OrdersOrderIDGetNotFound) publicGetOrderPublicV1OrdersOrderIDGetRes() {}

// Ref: #/components/schemas/PublicOrderDetailForResponse
type PublicOrderDetailForResponse struct {
	ID                 uuid.UUID                 `json:"id"`
	OrderID            string                    `json:"order_id"`
	RouteID            OptUUID                   `json:"route_id"`
	Merchant           OrderMerchant             `json:"merchant"`
	PlaceID            OptUUID                   `json:"place_id"`
	Status             OrderStatusEnum           `json:"status"`
	PaymentStatus      PaymentStatusEnum         `json:"payment_status"`
	Cost               OrderCost                 `json:"cost"`
	Customer           OrderCustomer             `json:"customer"`
	PersonCount        int                       `json:"person_count"`
	Comment            OptString                 `json:"comment"`
	CancelCode         OptOrderCancelCodeEnum    `json:"cancel_code"`
	CancelReason       OptString                 `json:"cancel_reason"`
	IsForceStart       bool                      `json:"is_force_start"`
	Delivery           PublicDeliveryForResponse `json:"delivery"`
	Products           []ProductForResponse      `json:"products"`
	CreatedAt          time.Time                 `json:"created_at"`
	UpdatedAt          time.Time                 `json:"updated_at"`
	AdditionalServices []AdditionalServiceInDB   `json:"additional_services"`
}

// GetID returns the value of ID.
func (s *PublicOrderDetailForResponse) GetID() uuid.UUID {
	return s.ID
}

// GetOrderID returns the value of OrderID.
func (s *PublicOrderDetailForResponse) GetOrderID() string {
	return s.OrderID
}

// GetRouteID returns the value of RouteID.
func (s *PublicOrderDetailForResponse) GetRouteID() OptUUID {
	return s.RouteID
}

// GetMerchant returns the value of Merchant.
func (s *PublicOrderDetailForResponse) GetMerchant() OrderMerchant {
	return s.Merchant
}

// GetPlaceID returns the value of PlaceID.
func (s *PublicOrderDetailForResponse) GetPlaceID() OptUUID {
	return s.PlaceID
}

// GetStatus returns the value of Status.
func (s *PublicOrderDetailForResponse) GetStatus() OrderStatusEnum {
	return s.Status
}

// GetPaymentStatus returns the value of PaymentStatus.
func (s *PublicOrderDetailForResponse) GetPaymentStatus() PaymentStatusEnum {
	return s.PaymentStatus
}

// GetCost returns the value of Cost.
func (s *PublicOrderDetailForResponse) GetCost() OrderCost {
	return s.Cost
}

// GetCustomer returns the value of Customer.
func (s *PublicOrderDetailForResponse) GetCustomer() OrderCustomer {
	return s.Customer
}

// GetPersonCount returns the value of PersonCount.
func (s *PublicOrderDetailForResponse) GetPersonCount() int {
	return s.PersonCount
}

// GetComment returns the value of Comment.
func (s *PublicOrderDetailForResponse) GetComment() OptString {
	return s.Comment
}

// GetCancelCode returns the value of CancelCode.
func (s *PublicOrderDetailForResponse) GetCancelCode() OptOrderCancelCodeEnum {
	return s.CancelCode
}

// GetCancelReason returns the value of CancelReason.
func (s *PublicOrderDetailForResponse) GetCancelReason() OptString {
	return s.CancelReason
}

// GetIsForceStart returns the value of IsForceStart.
func (s *PublicOrderDetailForResponse) GetIsForceStart() bool {
	return s.IsForceStart
}

// GetDelivery returns the value of Delivery.
func (s *PublicOrderDetailForResponse) GetDelivery() PublicDeliveryForResponse {
	return s.Delivery
}

// GetProducts returns the value of Products.
func (s *PublicOrderDetailForResponse) GetProducts() []ProductForResponse {
	return s.Products
}

// GetCreatedAt returns the value of CreatedAt.
func (s *PublicOrderDetailForResponse) GetCreatedAt() time.Time {
	return s.CreatedAt
}

// GetUpdatedAt returns the value of UpdatedAt.
func (s *PublicOrderDetailForResponse) GetUpdatedAt() time.Time {
	return s.UpdatedAt
}

// GetAdditionalServices returns the value of AdditionalServices.
func (s *PublicOrderDetailForResponse) GetAdditionalServices() []AdditionalServiceInDB {
	return s.AdditionalServices
}

// SetID sets the value of ID.
func (s *PublicOrderDetailForResponse) SetID(val uuid.UUID) {
	s.ID = val
}

// SetOrderID sets the value of OrderID.
func (s *PublicOrderDetailForResponse) SetOrderID(val string) {
	s.OrderID = val
}

// SetRouteID sets the value of RouteID.
func (s *PublicOrderDetailForResponse) SetRouteID(val OptUUID) {
	s.RouteID = val
}

// SetMerchant sets the value of Merchant.
func (s *PublicOrderDetailForResponse) SetMerchant(val OrderMerchant) {
	s.Merchant = val
}

// SetPlaceID sets the value of PlaceID.
func (s *PublicOrderDetailForResponse) SetPlaceID(val OptUUID) {
	s.PlaceID = val
}

// SetStatus sets the value of Status.
func (s *PublicOrderDetailForResponse) SetStatus(val OrderStatusEnum) {
	s.Status = val
}

// SetPaymentStatus sets the value of PaymentStatus.
func (s *PublicOrderDetailForResponse) SetPaymentStatus(val PaymentStatusEnum) {
	s.PaymentStatus = val
}

// SetCost sets the value of Cost.
func (s *PublicOrderDetailForResponse) SetCost(val OrderCost) {
	s.Cost = val
}

// SetCustomer sets the value of Customer.
func (s *PublicOrderDetailForResponse) SetCustomer(val OrderCustomer) {
	s.Customer = val
}

// SetPersonCount sets the value of PersonCount.
func (s *PublicOrderDetailForResponse) SetPersonCount(val int) {
	s.PersonCount = val
}

// SetComment sets the value of Comment.
func (s *PublicOrderDetailForResponse) SetComment(val OptString) {
	s.Comment = val
}

// SetCancelCode sets the value of CancelCode.
func (s *PublicOrderDetailForResponse) SetCancelCode(val OptOrderCancelCodeEnum) {
	s.CancelCode = val
}

// SetCancelReason sets the value of CancelReason.
func (s *PublicOrderDetailForResponse) SetCancelReason(val OptString) {
	s.CancelReason = val
}

// SetIsForceStart sets the value of IsForceStart.
func (s *PublicOrderDetailForResponse) SetIsForceStart(val bool) {
	s.IsForceStart = val
}

// SetDelivery sets the value of Delivery.
func (s *PublicOrderDetailForResponse) SetDelivery(val PublicDeliveryForResponse) {
	s.Delivery = val
}

// SetProducts sets the value of Products.
func (s *PublicOrderDetailForResponse) SetProducts(val []ProductForResponse) {
	s.Products = val
}

// SetCreatedAt sets the value of CreatedAt.
func (s *PublicOrderDetailForResponse) SetCreatedAt(val time.Time) {
	s.CreatedAt = val
}

// SetUpdatedAt sets the value of UpdatedAt.
func (s *PublicOrderDetailForResponse) SetUpdatedAt(val time.Time) {
	s.UpdatedAt = val
}

// SetAdditionalServices sets the value of AdditionalServices.
func (s *PublicOrderDetailForResponse) SetAdditionalServices(val []AdditionalServiceInDB) {
	s.AdditionalServices = val
}

func (*PublicOrderDetailForResponse) publicCreateOrderPublicV1OrdersPostRes()         {}
func (*PublicOrderDetailForResponse) publicGetOrderPublicV1OrdersOrderIDGetRes()      {}
func (*PublicOrderDetailForResponse) publicUpdateOrderPublicV1OrdersOrderIDPatchRes() {}

type PublicReturnOrderPublicV1OrdersOrderIDReturnPatchOKApplicationJSON jx.Raw

func (*PublicReturnOrderPublicV1OrdersOrderIDReturnPatchOKApplicationJSON) publicReturnOrderPublicV1OrdersOrderIDReturnPatchRes() {
}

type PublicReturnOrderPublicV2OrdersOrderIDReturnPatchOKApplicationJSON jx.Raw

func (*PublicReturnOrderPublicV2OrdersOrderIDReturnPatchOKApplicationJSON) publicReturnOrderPublicV2OrdersOrderIDReturnPatchRes() {
}

// PublicUpdateOrderPublicV1OrdersOrderIDPatchNotFound is response for PublicUpdateOrderPublicV1OrdersOrderIDPatch operation.
type PublicUpdateOrderPublicV1OrdersOrderIDPatchNotFound struct{}

func (*PublicUpdateOrderPublicV1OrdersOrderIDPatchNotFound) publicUpdateOrderPublicV1OrdersOrderIDPatchRes() {
}

// Ref: #/components/schemas/RegularSchedule
type RegularSchedule struct {
	WeekDay   int       `json:"week_day"`
	TimeStart time.Time `json:"time_start"`
	TimeEnd   time.Time `json:"time_end"`
	IsWork    bool      `json:"is_work"`
}

// GetWeekDay returns the value of WeekDay.
func (s *RegularSchedule) GetWeekDay() int {
	return s.WeekDay
}

// GetTimeStart returns the value of TimeStart.
func (s *RegularSchedule) GetTimeStart() time.Time {
	return s.TimeStart
}

// GetTimeEnd returns the value of TimeEnd.
func (s *RegularSchedule) GetTimeEnd() time.Time {
	return s.TimeEnd
}

// GetIsWork returns the value of IsWork.
func (s *RegularSchedule) GetIsWork() bool {
	return s.IsWork
}

// SetWeekDay sets the value of WeekDay.
func (s *RegularSchedule) SetWeekDay(val int) {
	s.WeekDay = val
}

// SetTimeStart sets the value of TimeStart.
func (s *RegularSchedule) SetTimeStart(val time.Time) {
	s.TimeStart = val
}

// SetTimeEnd sets the value of TimeEnd.
func (s *RegularSchedule) SetTimeEnd(val time.Time) {
	s.TimeEnd = val
}

// SetIsWork sets the value of IsWork.
func (s *RegularSchedule) SetIsWork(val bool) {
	s.IsWork = val
}

// Validation of request for returning products of order.
// Ref: #/components/schemas/ReturnProductsInRequest
type ReturnProductsInRequest struct {
	Products       []string `json:"products"`
	ReturnAmount   float64  `json:"return_amount"`
	NewTotalAmount float64  `json:"new_total_amount"`
	Reason         string   `json:"reason"`
}

// GetProducts returns the value of Products.
func (s *ReturnProductsInRequest) GetProducts() []string {
	return s.Products
}

// GetReturnAmount returns the value of ReturnAmount.
func (s *ReturnProductsInRequest) GetReturnAmount() float64 {
	return s.ReturnAmount
}

// GetNewTotalAmount returns the value of NewTotalAmount.
func (s *ReturnProductsInRequest) GetNewTotalAmount() float64 {
	return s.NewTotalAmount
}

// GetReason returns the value of Reason.
func (s *ReturnProductsInRequest) GetReason() string {
	return s.Reason
}

// SetProducts sets the value of Products.
func (s *ReturnProductsInRequest) SetProducts(val []string) {
	s.Products = val
}

// SetReturnAmount sets the value of ReturnAmount.
func (s *ReturnProductsInRequest) SetReturnAmount(val float64) {
	s.ReturnAmount = val
}

// SetNewTotalAmount sets the value of NewTotalAmount.
func (s *ReturnProductsInRequest) SetNewTotalAmount(val float64) {
	s.NewTotalAmount = val
}

// SetReason sets the value of Reason.
func (s *ReturnProductsInRequest) SetReason(val string) {
	s.Reason = val
}

// Ref: #/components/schemas/ReturnProductsInRequestV2
type ReturnProductsInRequestV2 struct {
	Products []ReturnProductsInRequestV2Products `json:"products"`
	// Refunded amount for entire order (money).
	ReturnAmount float64 `json:"return_amount"`
	// New order cost (money).
	NewTotalAmount float64                `json:"new_total_amount"`
	Reason         string                 `json:"reason"`
	CancelCode     OptOrderCancelCodeEnum `json:"cancel_code"`
}

// GetProducts returns the value of Products.
func (s *ReturnProductsInRequestV2) GetProducts() []ReturnProductsInRequestV2Products {
	return s.Products
}

// GetReturnAmount returns the value of ReturnAmount.
func (s *ReturnProductsInRequestV2) GetReturnAmount() float64 {
	return s.ReturnAmount
}

// GetNewTotalAmount returns the value of NewTotalAmount.
func (s *ReturnProductsInRequestV2) GetNewTotalAmount() float64 {
	return s.NewTotalAmount
}

// GetReason returns the value of Reason.
func (s *ReturnProductsInRequestV2) GetReason() string {
	return s.Reason
}

// GetCancelCode returns the value of CancelCode.
func (s *ReturnProductsInRequestV2) GetCancelCode() OptOrderCancelCodeEnum {
	return s.CancelCode
}

// SetProducts sets the value of Products.
func (s *ReturnProductsInRequestV2) SetProducts(val []ReturnProductsInRequestV2Products) {
	s.Products = val
}

// SetReturnAmount sets the value of ReturnAmount.
func (s *ReturnProductsInRequestV2) SetReturnAmount(val float64) {
	s.ReturnAmount = val
}

// SetNewTotalAmount sets the value of NewTotalAmount.
func (s *ReturnProductsInRequestV2) SetNewTotalAmount(val float64) {
	s.NewTotalAmount = val
}

// SetReason sets the value of Reason.
func (s *ReturnProductsInRequestV2) SetReason(val string) {
	s.Reason = val
}

// SetCancelCode sets the value of CancelCode.
func (s *ReturnProductsInRequestV2) SetCancelCode(val OptOrderCancelCodeEnum) {
	s.CancelCode = val
}

// Ref: #/components/schemas/ReturnProductsInRequestV2_products
type ReturnProductsInRequestV2Products struct {
	// External product id (id in  merchant system).
	ID OptString `json:"id"`
	// Price of one product item.
	Price OptFloat64 `json:"price"`
	// Total amount of returned products (money).
	RefundAmount OptFloat64 `json:"refund_amount"`
	// Refunded products quantity.
	RefundQuantity OptFloat64 `json:"refund_quantity"`
}

// GetID returns the value of ID.
func (s *ReturnProductsInRequestV2Products) GetID() OptString {
	return s.ID
}

// GetPrice returns the value of Price.
func (s *ReturnProductsInRequestV2Products) GetPrice() OptFloat64 {
	return s.Price
}

// GetRefundAmount returns the value of RefundAmount.
func (s *ReturnProductsInRequestV2Products) GetRefundAmount() OptFloat64 {
	return s.RefundAmount
}

// GetRefundQuantity returns the value of RefundQuantity.
func (s *ReturnProductsInRequestV2Products) GetRefundQuantity() OptFloat64 {
	return s.RefundQuantity
}

// SetID sets the value of ID.
func (s *ReturnProductsInRequestV2Products) SetID(val OptString) {
	s.ID = val
}

// SetPrice sets the value of Price.
func (s *ReturnProductsInRequestV2Products) SetPrice(val OptFloat64) {
	s.Price = val
}

// SetRefundAmount sets the value of RefundAmount.
func (s *ReturnProductsInRequestV2Products) SetRefundAmount(val OptFloat64) {
	s.RefundAmount = val
}

// SetRefundQuantity sets the value of RefundQuantity.
func (s *ReturnProductsInRequestV2Products) SetRefundQuantity(val OptFloat64) {
	s.RefundQuantity = val
}

// Ref: #/components/schemas/SpecialSchedule
type SpecialSchedule struct {
	Day       time.Time `json:"day"`
	TimeStart time.Time `json:"time_start"`
	TimeEnd   time.Time `json:"time_end"`
	IsWork    bool      `json:"is_work"`
}

// GetDay returns the value of Day.
func (s *SpecialSchedule) GetDay() time.Time {
	return s.Day
}

// GetTimeStart returns the value of TimeStart.
func (s *SpecialSchedule) GetTimeStart() time.Time {
	return s.TimeStart
}

// GetTimeEnd returns the value of TimeEnd.
func (s *SpecialSchedule) GetTimeEnd() time.Time {
	return s.TimeEnd
}

// GetIsWork returns the value of IsWork.
func (s *SpecialSchedule) GetIsWork() bool {
	return s.IsWork
}

// SetDay sets the value of Day.
func (s *SpecialSchedule) SetDay(val time.Time) {
	s.Day = val
}

// SetTimeStart sets the value of TimeStart.
func (s *SpecialSchedule) SetTimeStart(val time.Time) {
	s.TimeStart = val
}

// SetTimeEnd sets the value of TimeEnd.
func (s *SpecialSchedule) SetTimeEnd(val time.Time) {
	s.TimeEnd = val
}

// SetIsWork sets the value of IsWork.
func (s *SpecialSchedule) SetIsWork(val bool) {
	s.IsWork = val
}

// Ref: #/components/schemas/UpdateDeliveryInRequest
type UpdateDeliveryInRequest struct {
	ExpectedTime  OptDateTime           `json:"expected_time"`
	PaymentMethod OptPaymentMethodEnum  `json:"payment_method"`
	ConsumerData  OptString             `json:"consumer_data"`
	Region        OptString             `json:"region"`
	Street        OptString             `json:"street"`
	HouseNumber   OptString             `json:"house_number"`
	FlatNumber    OptString             `json:"flat_number"`
	Entrance      OptString             `json:"entrance"`
	Intercom      OptString             `json:"intercom"`
	Floor         OptString             `json:"floor"`
	Location      OptPoint              `json:"location"`
	Comment       OptString             `json:"comment"`
	Attributes    OptDeliveryAttributes `json:"attributes"`
}

// GetExpectedTime returns the value of ExpectedTime.
func (s *UpdateDeliveryInRequest) GetExpectedTime() OptDateTime {
	return s.ExpectedTime
}

// GetPaymentMethod returns the value of PaymentMethod.
func (s *UpdateDeliveryInRequest) GetPaymentMethod() OptPaymentMethodEnum {
	return s.PaymentMethod
}

// GetConsumerData returns the value of ConsumerData.
func (s *UpdateDeliveryInRequest) GetConsumerData() OptString {
	return s.ConsumerData
}

// GetRegion returns the value of Region.
func (s *UpdateDeliveryInRequest) GetRegion() OptString {
	return s.Region
}

// GetStreet returns the value of Street.
func (s *UpdateDeliveryInRequest) GetStreet() OptString {
	return s.Street
}

// GetHouseNumber returns the value of HouseNumber.
func (s *UpdateDeliveryInRequest) GetHouseNumber() OptString {
	return s.HouseNumber
}

// GetFlatNumber returns the value of FlatNumber.
func (s *UpdateDeliveryInRequest) GetFlatNumber() OptString {
	return s.FlatNumber
}

// GetEntrance returns the value of Entrance.
func (s *UpdateDeliveryInRequest) GetEntrance() OptString {
	return s.Entrance
}

// GetIntercom returns the value of Intercom.
func (s *UpdateDeliveryInRequest) GetIntercom() OptString {
	return s.Intercom
}

// GetFloor returns the value of Floor.
func (s *UpdateDeliveryInRequest) GetFloor() OptString {
	return s.Floor
}

// GetLocation returns the value of Location.
func (s *UpdateDeliveryInRequest) GetLocation() OptPoint {
	return s.Location
}

// GetComment returns the value of Comment.
func (s *UpdateDeliveryInRequest) GetComment() OptString {
	return s.Comment
}

// GetAttributes returns the value of Attributes.
func (s *UpdateDeliveryInRequest) GetAttributes() OptDeliveryAttributes {
	return s.Attributes
}

// SetExpectedTime sets the value of ExpectedTime.
func (s *UpdateDeliveryInRequest) SetExpectedTime(val OptDateTime) {
	s.ExpectedTime = val
}

// SetPaymentMethod sets the value of PaymentMethod.
func (s *UpdateDeliveryInRequest) SetPaymentMethod(val OptPaymentMethodEnum) {
	s.PaymentMethod = val
}

// SetConsumerData sets the value of ConsumerData.
func (s *UpdateDeliveryInRequest) SetConsumerData(val OptString) {
	s.ConsumerData = val
}

// SetRegion sets the value of Region.
func (s *UpdateDeliveryInRequest) SetRegion(val OptString) {
	s.Region = val
}

// SetStreet sets the value of Street.
func (s *UpdateDeliveryInRequest) SetStreet(val OptString) {
	s.Street = val
}

// SetHouseNumber sets the value of HouseNumber.
func (s *UpdateDeliveryInRequest) SetHouseNumber(val OptString) {
	s.HouseNumber = val
}

// SetFlatNumber sets the value of FlatNumber.
func (s *UpdateDeliveryInRequest) SetFlatNumber(val OptString) {
	s.FlatNumber = val
}

// SetEntrance sets the value of Entrance.
func (s *UpdateDeliveryInRequest) SetEntrance(val OptString) {
	s.Entrance = val
}

// SetIntercom sets the value of Intercom.
func (s *UpdateDeliveryInRequest) SetIntercom(val OptString) {
	s.Intercom = val
}

// SetFloor sets the value of Floor.
func (s *UpdateDeliveryInRequest) SetFloor(val OptString) {
	s.Floor = val
}

// SetLocation sets the value of Location.
func (s *UpdateDeliveryInRequest) SetLocation(val OptPoint) {
	s.Location = val
}

// SetComment sets the value of Comment.
func (s *UpdateDeliveryInRequest) SetComment(val OptString) {
	s.Comment = val
}

// SetAttributes sets the value of Attributes.
func (s *UpdateDeliveryInRequest) SetAttributes(val OptDeliveryAttributes) {
	s.Attributes = val
}

// Ref: #/components/schemas/UpdateOrderInRequest
type UpdateOrderInRequest struct {
	Status         OptOrderStatusEnum         `json:"status"`
	PaymentStatus  OptPaymentStatusEnum       `json:"payment_status"`
	Cost           OptFloat64                 `json:"cost"`
	AdditionalCost OptFloat64                 `json:"additional_cost"`
	ChangeCost     OptFloat64                 `json:"change_cost"`
	CustomerName   OptString                  `json:"customer_name"`
	CustomerPhone  OptString                  `json:"customer_phone"`
	CustomerEmail  OptString                  `json:"customer_email"`
	PersonCount    OptInt                     `json:"person_count"`
	Comment        OptString                  `json:"comment"`
	CancelCode     OptOrderCancelCodeEnum     `json:"cancel_code"`
	CancelReason   OptString                  `json:"cancel_reason"`
	Delivery       OptUpdateDeliveryInRequest `json:"delivery"`
	Products       []CreateProductInRequest   `json:"products"`
}

// GetStatus returns the value of Status.
func (s *UpdateOrderInRequest) GetStatus() OptOrderStatusEnum {
	return s.Status
}

// GetPaymentStatus returns the value of PaymentStatus.
func (s *UpdateOrderInRequest) GetPaymentStatus() OptPaymentStatusEnum {
	return s.PaymentStatus
}

// GetCost returns the value of Cost.
func (s *UpdateOrderInRequest) GetCost() OptFloat64 {
	return s.Cost
}

// GetAdditionalCost returns the value of AdditionalCost.
func (s *UpdateOrderInRequest) GetAdditionalCost() OptFloat64 {
	return s.AdditionalCost
}

// GetChangeCost returns the value of ChangeCost.
func (s *UpdateOrderInRequest) GetChangeCost() OptFloat64 {
	return s.ChangeCost
}

// GetCustomerName returns the value of CustomerName.
func (s *UpdateOrderInRequest) GetCustomerName() OptString {
	return s.CustomerName
}

// GetCustomerPhone returns the value of CustomerPhone.
func (s *UpdateOrderInRequest) GetCustomerPhone() OptString {
	return s.CustomerPhone
}

// GetCustomerEmail returns the value of CustomerEmail.
func (s *UpdateOrderInRequest) GetCustomerEmail() OptString {
	return s.CustomerEmail
}

// GetPersonCount returns the value of PersonCount.
func (s *UpdateOrderInRequest) GetPersonCount() OptInt {
	return s.PersonCount
}

// GetComment returns the value of Comment.
func (s *UpdateOrderInRequest) GetComment() OptString {
	return s.Comment
}

// GetCancelCode returns the value of CancelCode.
func (s *UpdateOrderInRequest) GetCancelCode() OptOrderCancelCodeEnum {
	return s.CancelCode
}

// GetCancelReason returns the value of CancelReason.
func (s *UpdateOrderInRequest) GetCancelReason() OptString {
	return s.CancelReason
}

// GetDelivery returns the value of Delivery.
func (s *UpdateOrderInRequest) GetDelivery() OptUpdateDeliveryInRequest {
	return s.Delivery
}

// GetProducts returns the value of Products.
func (s *UpdateOrderInRequest) GetProducts() []CreateProductInRequest {
	return s.Products
}

// SetStatus sets the value of Status.
func (s *UpdateOrderInRequest) SetStatus(val OptOrderStatusEnum) {
	s.Status = val
}

// SetPaymentStatus sets the value of PaymentStatus.
func (s *UpdateOrderInRequest) SetPaymentStatus(val OptPaymentStatusEnum) {
	s.PaymentStatus = val
}

// SetCost sets the value of Cost.
func (s *UpdateOrderInRequest) SetCost(val OptFloat64) {
	s.Cost = val
}

// SetAdditionalCost sets the value of AdditionalCost.
func (s *UpdateOrderInRequest) SetAdditionalCost(val OptFloat64) {
	s.AdditionalCost = val
}

// SetChangeCost sets the value of ChangeCost.
func (s *UpdateOrderInRequest) SetChangeCost(val OptFloat64) {
	s.ChangeCost = val
}

// SetCustomerName sets the value of CustomerName.
func (s *UpdateOrderInRequest) SetCustomerName(val OptString) {
	s.CustomerName = val
}

// SetCustomerPhone sets the value of CustomerPhone.
func (s *UpdateOrderInRequest) SetCustomerPhone(val OptString) {
	s.CustomerPhone = val
}

// SetCustomerEmail sets the value of CustomerEmail.
func (s *UpdateOrderInRequest) SetCustomerEmail(val OptString) {
	s.CustomerEmail = val
}

// SetPersonCount sets the value of PersonCount.
func (s *UpdateOrderInRequest) SetPersonCount(val OptInt) {
	s.PersonCount = val
}

// SetComment sets the value of Comment.
func (s *UpdateOrderInRequest) SetComment(val OptString) {
	s.Comment = val
}

// SetCancelCode sets the value of CancelCode.
func (s *UpdateOrderInRequest) SetCancelCode(val OptOrderCancelCodeEnum) {
	s.CancelCode = val
}

// SetCancelReason sets the value of CancelReason.
func (s *UpdateOrderInRequest) SetCancelReason(val OptString) {
	s.CancelReason = val
}

// SetDelivery sets the value of Delivery.
func (s *UpdateOrderInRequest) SetDelivery(val OptUpdateDeliveryInRequest) {
	s.Delivery = val
}

// SetProducts sets the value of Products.
func (s *UpdateOrderInRequest) SetProducts(val []CreateProductInRequest) {
	s.Products = val
}

// Ref: #/components/schemas/ValidationError
type ValidationError struct {
	Loc  []string `json:"loc"`
	Msg  string   `json:"msg"`
	Type string   `json:"type"`
}

// GetLoc returns the value of Loc.
func (s *ValidationError) GetLoc() []string {
	return s.Loc
}

// GetMsg returns the value of Msg.
func (s *ValidationError) GetMsg() string {
	return s.Msg
}

// GetType returns the value of Type.
func (s *ValidationError) GetType() string {
	return s.Type
}

// SetLoc sets the value of Loc.
func (s *ValidationError) SetLoc(val []string) {
	s.Loc = val
}

// SetMsg sets the value of Msg.
func (s *ValidationError) SetMsg(val string) {
	s.Msg = val
}

// SetType sets the value of Type.
func (s *ValidationError) SetType(val string) {
	s.Type = val
}

// Ref: #/components/schemas/Vehicle
type Vehicle struct {
	LicensePlate string `json:"license_plate"`
	Model        string `json:"model"`
}

// GetLicensePlate returns the value of LicensePlate.
func (s *Vehicle) GetLicensePlate() string {
	return s.LicensePlate
}

// GetModel returns the value of Model.
func (s *Vehicle) GetModel() string {
	return s.Model
}

// SetLicensePlate sets the value of LicensePlate.
func (s *Vehicle) SetLicensePlate(val string) {
	s.LicensePlate = val
}

// SetModel sets the value of Model.
func (s *Vehicle) SetModel(val string) {
	s.Model = val
}

// Ref: #/components/schemas/WorkTimes
type WorkTimes struct {
	Regular []RegularSchedule `json:"regular"`
	Special []SpecialSchedule `json:"special"`
}

// GetRegular returns the value of Regular.
func (s *WorkTimes) GetRegular() []RegularSchedule {
	return s.Regular
}

// GetSpecial returns the value of Special.
func (s *WorkTimes) GetSpecial() []SpecialSchedule {
	return s.Special
}

// SetRegular sets the value of Regular.
func (s *WorkTimes) SetRegular(val []RegularSchedule) {
	s.Regular = val
}

// SetSpecial sets the value of Special.
func (s *WorkTimes) SetSpecial(val []SpecialSchedule) {
	s.Special = val
}

func (*WorkTimes) merchantScheduleGetPublicV1MerchantMerchantIDScheduleGetRes()    {}
func (*WorkTimes) merchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutRes() {}
func (*WorkTimes) placeScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetRes()    {}
func (*WorkTimes) placeScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutRes() {}

// Ref: #/components/schemas/WorkTimesUpdate
type WorkTimesUpdate struct {
	Regular []RegularSchedule `json:"regular"`
	Special []SpecialSchedule `json:"special"`
}

// GetRegular returns the value of Regular.
func (s *WorkTimesUpdate) GetRegular() []RegularSchedule {
	return s.Regular
}

// GetSpecial returns the value of Special.
func (s *WorkTimesUpdate) GetSpecial() []SpecialSchedule {
	return s.Special
}

// SetRegular sets the value of Regular.
func (s *WorkTimesUpdate) SetRegular(val []RegularSchedule) {
	s.Regular = val
}

// SetSpecial sets the value of Special.
func (s *WorkTimesUpdate) SetSpecial(val []SpecialSchedule) {
	s.Special = val
}
