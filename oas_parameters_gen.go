package api

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/go-faster/errors"
	"github.com/google/uuid"

	"github.com/ogen-go/ogen/conv"
	"github.com/ogen-go/ogen/middleware"
	"github.com/ogen-go/ogen/ogenerrors"
	"github.com/ogen-go/ogen/uri"
	"github.com/ogen-go/ogen/validate"
)

// MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams is parameters of POST /merchant_courier_near_delivery_address_callback_url/{order_id} operation.
type MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams struct {
	OrderID uuid.UUID
}

func unpackMerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams(packed middleware.Parameters) (params MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams) {
	{
		key := middleware.ParameterKey{
			Name: "order_id",
			In:   "path",
		}
		params.OrderID = packed[key].(uuid.UUID)
	}
	return params
}

func decodeMerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams(args [1]string, argsEscaped bool, r *http.Request) (params MerchantCourierNearDeliveryAddressCallbackURLOrderIDPostParams, _ error) {
	// Decode path: order_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "order_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.OrderID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "order_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// MerchantCourierNearPlaceCallbackURLOrderIDPostParams is parameters of POST /merchant_courier_near_place_callback_url/{order_id} operation.
type MerchantCourierNearPlaceCallbackURLOrderIDPostParams struct {
	OrderID uuid.UUID
}

func unpackMerchantCourierNearPlaceCallbackURLOrderIDPostParams(packed middleware.Parameters) (params MerchantCourierNearPlaceCallbackURLOrderIDPostParams) {
	{
		key := middleware.ParameterKey{
			Name: "order_id",
			In:   "path",
		}
		params.OrderID = packed[key].(uuid.UUID)
	}
	return params
}

func decodeMerchantCourierNearPlaceCallbackURLOrderIDPostParams(args [1]string, argsEscaped bool, r *http.Request) (params MerchantCourierNearPlaceCallbackURLOrderIDPostParams, _ error) {
	// Decode path: order_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "order_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.OrderID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "order_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// MerchantOrderStatusCallbackURLOrderIDPostParams is parameters of POST /merchant_order_status_callback_url/{order_id} operation.
type MerchantOrderStatusCallbackURLOrderIDPostParams struct {
	OrderID uuid.UUID
}

func unpackMerchantOrderStatusCallbackURLOrderIDPostParams(packed middleware.Parameters) (params MerchantOrderStatusCallbackURLOrderIDPostParams) {
	{
		key := middleware.ParameterKey{
			Name: "order_id",
			In:   "path",
		}
		params.OrderID = packed[key].(uuid.UUID)
	}
	return params
}

func decodeMerchantOrderStatusCallbackURLOrderIDPostParams(args [1]string, argsEscaped bool, r *http.Request) (params MerchantOrderStatusCallbackURLOrderIDPostParams, _ error) {
	// Decode path: order_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "order_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.OrderID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "order_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams is parameters of merchant_schedule_get_public_v1_merchant__merchant_id__schedule_get operation.
type MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams struct {
	MerchantID uuid.UUID
}

func unpackMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams(packed middleware.Parameters) (params MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams) {
	{
		key := middleware.ParameterKey{
			Name: "merchant_id",
			In:   "path",
		}
		params.MerchantID = packed[key].(uuid.UUID)
	}
	return params
}

func decodeMerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams(args [1]string, argsEscaped bool, r *http.Request) (params MerchantScheduleGetPublicV1MerchantMerchantIDScheduleGetParams, _ error) {
	// Decode path: merchant_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "merchant_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.MerchantID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "merchant_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams is parameters of merchant_schedule_update_public_v1_merchant__merchant_id__schedule_put operation.
type MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams struct {
	MerchantID uuid.UUID
}

func unpackMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams(packed middleware.Parameters) (params MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams) {
	{
		key := middleware.ParameterKey{
			Name: "merchant_id",
			In:   "path",
		}
		params.MerchantID = packed[key].(uuid.UUID)
	}
	return params
}

func decodeMerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams(args [1]string, argsEscaped bool, r *http.Request) (params MerchantScheduleUpdatePublicV1MerchantMerchantIDSchedulePutParams, _ error) {
	// Decode path: merchant_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "merchant_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.MerchantID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "merchant_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams is parameters of place_schedule_get_public_v1_merchant_places__place_id__schedule_get operation.
type PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams struct {
	PlaceID uuid.UUID
}

func unpackPlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams(packed middleware.Parameters) (params PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams) {
	{
		key := middleware.ParameterKey{
			Name: "place_id",
			In:   "path",
		}
		params.PlaceID = packed[key].(uuid.UUID)
	}
	return params
}

func decodePlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams(args [1]string, argsEscaped bool, r *http.Request) (params PlaceScheduleGetPublicV1MerchantPlacesPlaceIDScheduleGetParams, _ error) {
	// Decode path: place_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "place_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.PlaceID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "place_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams is parameters of place_schedule_update_public_v1_merchant_places__place_id__schedule_put operation.
type PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams struct {
	PlaceID uuid.UUID
}

func unpackPlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams(packed middleware.Parameters) (params PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams) {
	{
		key := middleware.ParameterKey{
			Name: "place_id",
			In:   "path",
		}
		params.PlaceID = packed[key].(uuid.UUID)
	}
	return params
}

func decodePlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams(args [1]string, argsEscaped bool, r *http.Request) (params PlaceScheduleUpdatePublicV1MerchantPlacesPlaceIDSchedulePutParams, _ error) {
	// Decode path: place_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "place_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.PlaceID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "place_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// PlacesBlocksPublicV1MerchantPlacesBlocksGetParams is parameters of places_blocks_public_v1_merchant_places_blocks__get operation.
type PlacesBlocksPublicV1MerchantPlacesBlocksGetParams struct {
	PlaceIds []uuid.UUID
	Limit    OptInt
	Offset   OptInt
}

func unpackPlacesBlocksPublicV1MerchantPlacesBlocksGetParams(packed middleware.Parameters) (params PlacesBlocksPublicV1MerchantPlacesBlocksGetParams) {
	{
		key := middleware.ParameterKey{
			Name: "place_ids",
			In:   "query",
		}
		if v, ok := packed[key]; ok {
			params.PlaceIds = v.([]uuid.UUID)
		}
	}
	{
		key := middleware.ParameterKey{
			Name: "limit",
			In:   "query",
		}
		if v, ok := packed[key]; ok {
			params.Limit = v.(OptInt)
		}
	}
	{
		key := middleware.ParameterKey{
			Name: "offset",
			In:   "query",
		}
		if v, ok := packed[key]; ok {
			params.Offset = v.(OptInt)
		}
	}
	return params
}

func decodePlacesBlocksPublicV1MerchantPlacesBlocksGetParams(args [0]string, argsEscaped bool, r *http.Request) (params PlacesBlocksPublicV1MerchantPlacesBlocksGetParams, _ error) {
	q := uri.NewQueryDecoder(r.URL.Query())
	// Decode query: place_ids.
	if err := func() error {
		cfg := uri.QueryParameterDecodingConfig{
			Name:    "place_ids",
			Style:   uri.QueryStyleForm,
			Explode: true,
		}

		if err := q.HasParam(cfg); err == nil {
			if err := q.DecodeParam(cfg, func(d uri.Decoder) error {
				return d.DecodeArray(func(d uri.Decoder) error {
					var paramsDotPlaceIdsVal uuid.UUID
					if err := func() error {
						val, err := d.DecodeValue()
						if err != nil {
							return err
						}

						c, err := conv.ToUUID(val)
						if err != nil {
							return err
						}

						paramsDotPlaceIdsVal = c
						return nil
					}(); err != nil {
						return err
					}
					params.PlaceIds = append(params.PlaceIds, paramsDotPlaceIdsVal)
					return nil
				})
			}); err != nil {
				return err
			}
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "place_ids",
			In:   "query",
			Err:  err,
		}
	}
	// Set default value for query: limit.
	{
		val := int(500)
		params.Limit.SetTo(val)
	}
	// Decode query: limit.
	if err := func() error {
		cfg := uri.QueryParameterDecodingConfig{
			Name:    "limit",
			Style:   uri.QueryStyleForm,
			Explode: true,
		}

		if err := q.HasParam(cfg); err == nil {
			if err := q.DecodeParam(cfg, func(d uri.Decoder) error {
				var paramsDotLimitVal int
				if err := func() error {
					val, err := d.DecodeValue()
					if err != nil {
						return err
					}

					c, err := conv.ToInt(val)
					if err != nil {
						return err
					}

					paramsDotLimitVal = c
					return nil
				}(); err != nil {
					return err
				}
				params.Limit.SetTo(paramsDotLimitVal)
				return nil
			}); err != nil {
				return err
			}
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "limit",
			In:   "query",
			Err:  err,
		}
	}
	// Set default value for query: offset.
	{
		val := int(0)
		params.Offset.SetTo(val)
	}
	// Decode query: offset.
	if err := func() error {
		cfg := uri.QueryParameterDecodingConfig{
			Name:    "offset",
			Style:   uri.QueryStyleForm,
			Explode: true,
		}

		if err := q.HasParam(cfg); err == nil {
			if err := q.DecodeParam(cfg, func(d uri.Decoder) error {
				var paramsDotOffsetVal int
				if err := func() error {
					val, err := d.DecodeValue()
					if err != nil {
						return err
					}

					c, err := conv.ToInt(val)
					if err != nil {
						return err
					}

					paramsDotOffsetVal = c
					return nil
				}(); err != nil {
					return err
				}
				params.Offset.SetTo(paramsDotOffsetVal)
				return nil
			}); err != nil {
				return err
			}
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "offset",
			In:   "query",
			Err:  err,
		}
	}
	return params, nil
}

// PlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams is parameters of places_update_public_v1_merchant_places__place_id__put operation.
type PlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams struct {
	PlaceID uuid.UUID
}

func unpackPlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams(packed middleware.Parameters) (params PlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams) {
	{
		key := middleware.ParameterKey{
			Name: "place_id",
			In:   "path",
		}
		params.PlaceID = packed[key].(uuid.UUID)
	}
	return params
}

func decodePlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams(args [1]string, argsEscaped bool, r *http.Request) (params PlacesUpdatePublicV1MerchantPlacesPlaceIDPutParams, _ error) {
	// Decode path: place_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "place_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.PlaceID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "place_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// PublicCancelOrderPublicV1OrdersOrderIDDeleteParams is parameters of public_cancel_order_public_v1_orders__order_id___delete operation.
type PublicCancelOrderPublicV1OrdersOrderIDDeleteParams struct {
	OrderID uuid.UUID
}

func unpackPublicCancelOrderPublicV1OrdersOrderIDDeleteParams(packed middleware.Parameters) (params PublicCancelOrderPublicV1OrdersOrderIDDeleteParams) {
	{
		key := middleware.ParameterKey{
			Name: "order_id",
			In:   "path",
		}
		params.OrderID = packed[key].(uuid.UUID)
	}
	return params
}

func decodePublicCancelOrderPublicV1OrdersOrderIDDeleteParams(args [1]string, argsEscaped bool, r *http.Request) (params PublicCancelOrderPublicV1OrdersOrderIDDeleteParams, _ error) {
	// Decode path: order_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "order_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.OrderID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "order_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams is parameters of public_get_courier_position_public_v1_orders__order_id__tracking__get operation.
type PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams struct {
	OrderID uuid.UUID
}

func unpackPublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams(packed middleware.Parameters) (params PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams) {
	{
		key := middleware.ParameterKey{
			Name: "order_id",
			In:   "path",
		}
		params.OrderID = packed[key].(uuid.UUID)
	}
	return params
}

func decodePublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams(args [1]string, argsEscaped bool, r *http.Request) (params PublicGetCourierPositionPublicV1OrdersOrderIDTrackingGetParams, _ error) {
	// Decode path: order_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "order_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.OrderID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "order_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams is parameters of public_get_order_photos_public_v1_orders__order_id__photos__get operation.
type PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams struct {
	OrderID   uuid.UUID
	PhotoType []OrderPhotoType
}

func unpackPublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams(packed middleware.Parameters) (params PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams) {
	{
		key := middleware.ParameterKey{
			Name: "order_id",
			In:   "path",
		}
		params.OrderID = packed[key].(uuid.UUID)
	}
	{
		key := middleware.ParameterKey{
			Name: "photo_type",
			In:   "query",
		}
		if v, ok := packed[key]; ok {
			params.PhotoType = v.([]OrderPhotoType)
		}
	}
	return params
}

func decodePublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams(args [1]string, argsEscaped bool, r *http.Request) (params PublicGetOrderPhotosPublicV1OrdersOrderIDPhotosGetParams, _ error) {
	q := uri.NewQueryDecoder(r.URL.Query())
	// Decode path: order_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "order_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.OrderID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "order_id",
			In:   "path",
			Err:  err,
		}
	}
	// Decode query: photo_type.
	if err := func() error {
		cfg := uri.QueryParameterDecodingConfig{
			Name:    "photo_type",
			Style:   uri.QueryStyleForm,
			Explode: true,
		}

		if err := q.HasParam(cfg); err == nil {
			if err := q.DecodeParam(cfg, func(d uri.Decoder) error {
				return d.DecodeArray(func(d uri.Decoder) error {
					var paramsDotPhotoTypeVal OrderPhotoType
					if err := func() error {
						val, err := d.DecodeValue()
						if err != nil {
							return err
						}

						c, err := conv.ToInt(val)
						if err != nil {
							return err
						}

						paramsDotPhotoTypeVal = OrderPhotoType(c)
						return nil
					}(); err != nil {
						return err
					}
					params.PhotoType = append(params.PhotoType, paramsDotPhotoTypeVal)
					return nil
				})
			}); err != nil {
				return err
			}
			if err := func() error {
				var failures []validate.FieldError
				for i, elem := range params.PhotoType {
					if err := func() error {
						if err := elem.Validate(); err != nil {
							return err
						}
						return nil
					}(); err != nil {
						failures = append(failures, validate.FieldError{
							Name:  fmt.Sprintf("[%d]", i),
							Error: err,
						})
					}
				}
				if len(failures) > 0 {
					return &validate.Error{Fields: failures}
				}
				return nil
			}(); err != nil {
				return err
			}
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "photo_type",
			In:   "query",
			Err:  err,
		}
	}
	return params, nil
}

// PublicGetOrderPublicV1OrdersOrderIDGetParams is parameters of public_get_order_public_v1_orders__order_id___get operation.
type PublicGetOrderPublicV1OrdersOrderIDGetParams struct {
	OrderID uuid.UUID
}

func unpackPublicGetOrderPublicV1OrdersOrderIDGetParams(packed middleware.Parameters) (params PublicGetOrderPublicV1OrdersOrderIDGetParams) {
	{
		key := middleware.ParameterKey{
			Name: "order_id",
			In:   "path",
		}
		params.OrderID = packed[key].(uuid.UUID)
	}
	return params
}

func decodePublicGetOrderPublicV1OrdersOrderIDGetParams(args [1]string, argsEscaped bool, r *http.Request) (params PublicGetOrderPublicV1OrdersOrderIDGetParams, _ error) {
	// Decode path: order_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "order_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.OrderID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "order_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// PublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams is parameters of public_return_order_public_v1_orders__order_id__return__patch operation.
type PublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams struct {
	OrderID uuid.UUID
}

func unpackPublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams(packed middleware.Parameters) (params PublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams) {
	{
		key := middleware.ParameterKey{
			Name: "order_id",
			In:   "path",
		}
		params.OrderID = packed[key].(uuid.UUID)
	}
	return params
}

func decodePublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams(args [1]string, argsEscaped bool, r *http.Request) (params PublicReturnOrderPublicV1OrdersOrderIDReturnPatchParams, _ error) {
	// Decode path: order_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "order_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.OrderID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "order_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// PublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams is parameters of public_return_order_public_v2_orders__order_id__return__patch operation.
type PublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams struct {
	OrderID uuid.UUID
}

func unpackPublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams(packed middleware.Parameters) (params PublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams) {
	{
		key := middleware.ParameterKey{
			Name: "order_id",
			In:   "path",
		}
		params.OrderID = packed[key].(uuid.UUID)
	}
	return params
}

func decodePublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams(args [1]string, argsEscaped bool, r *http.Request) (params PublicReturnOrderPublicV2OrdersOrderIDReturnPatchParams, _ error) {
	// Decode path: order_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "order_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.OrderID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "order_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}

// PublicUpdateOrderPublicV1OrdersOrderIDPatchParams is parameters of public_update_order_public_v1_orders__order_id___patch operation.
type PublicUpdateOrderPublicV1OrdersOrderIDPatchParams struct {
	OrderID uuid.UUID
}

func unpackPublicUpdateOrderPublicV1OrdersOrderIDPatchParams(packed middleware.Parameters) (params PublicUpdateOrderPublicV1OrdersOrderIDPatchParams) {
	{
		key := middleware.ParameterKey{
			Name: "order_id",
			In:   "path",
		}
		params.OrderID = packed[key].(uuid.UUID)
	}
	return params
}

func decodePublicUpdateOrderPublicV1OrdersOrderIDPatchParams(args [1]string, argsEscaped bool, r *http.Request) (params PublicUpdateOrderPublicV1OrdersOrderIDPatchParams, _ error) {
	// Decode path: order_id.
	if err := func() error {
		param := args[0]
		if argsEscaped {
			unescaped, err := url.PathUnescape(args[0])
			if err != nil {
				return errors.Wrap(err, "unescape path")
			}
			param = unescaped
		}
		if len(param) > 0 {
			d := uri.NewPathDecoder(uri.PathDecoderConfig{
				Param:   "order_id",
				Value:   param,
				Style:   uri.PathStyleSimple,
				Explode: false,
			})

			if err := func() error {
				val, err := d.DecodeValue()
				if err != nil {
					return err
				}

				c, err := conv.ToUUID(val)
				if err != nil {
					return err
				}

				params.OrderID = c
				return nil
			}(); err != nil {
				return err
			}
		} else {
			return validate.ErrFieldRequired
		}
		return nil
	}(); err != nil {
		return params, &ogenerrors.DecodeParamError{
			Name: "order_id",
			In:   "path",
			Err:  err,
		}
	}
	return params, nil
}
